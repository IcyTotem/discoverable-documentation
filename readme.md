# Discoverable documentation

DISCLAIMER: This project has been **discontinued**.

This is a project I started long ago with the intent to make documentation written inside a repository more discoverable and up-to-date. The platform would run in a SaaS wrapper and notify developers when changes should be made to improve documentation on merge. What follows is a summary of the design document I wrote before starting. Currently, most of the basics are implemented but I lost commitment while writing the frontend.

## What problem are we trying to solve?
  - Documentation is sparse and not easily accessible nor discoverable
  - Developers often forget to update or add proper documentation documents (markdown) after they implemented a feature
  - Even when developers do update documentation, the change is usually “silent” and other developers will not notice it until something breaks

## How are we solving it?
  - Ease centralization by providing a nice interface to versioned documentation directly in the repository
  - Allow documentation to be discoverable by making it searchable with a powerful search tool (ideally semantic search)
  - Remind developers to document their changes before a merge
  - Notify team members of documentation changes on the master/develop branch

## Use cases
  - As the developer of a new feature, I want to be notified when I am about to merge my feature branch into master (or develop) and I forgot to write documentation about my changes
  - As a developer of a new branch, I want to be able to suppress the reminder if my current work does not require any documentation
  - As a developer, I want to be informed when other developers merge new pieces of documentation into master (but maybe only those I care about)
  - As a developer, I want to be able to search for knowledge in the existing documentation and see what is currently present (and maybe what was there in the past but has been changed or deleted)

## Requirements
  - (MUST) Allow developers from the same team to access a dashboard that contains all the documentation from the repo on its master branch, rendered from markdown to html for consultation
  - (MUST) Allow developers to perform semantic search across all documents to get relevant articles. For example: “how do I start the application on my local cluster?”
  - (MUST) Notification channels must be e-mails and platform-embedded elements. For reminders, pinpoint a list of “problems” for each project (this may be expanded in the future with other kinds of reminders). For new documentation, simply list a timeline of changes in the project dashboard
  - (MUST) Notify developers when a (big?) new piece of documentation is introduced
  - (MUST) Notify developers when a document has not been changed for a long time
  - (MUST) Remind developers to write their due documentation
  - (MUST) Allow developers to suppress notification they don’t want in a configurable way
  - (SHOULD) Include results from text search from previous versions of the documentation
  - (COULD) Make text search semantic using sentence embeddings
  - (COULD) Allow for other means of notification, like slack messages

## Won’t do

Identify when documentation points to files within the versioning system and notify the team when those files are no longer reachable, or when the documentation may need updating after big changes.

**Why?** It is a very niche use case. Researching other repositories showed that documentation files rarely contain full paths to other files in the same repo and it’s hard to spot them. Ultimately there is no big prize to be earned for the effort of implementing this

---

Establish a relationship among documents and source code that tend to change together and alert developers that a document may need to be updated based on the changes to the source code.
  
**Why?** While doable, the implementation is tricky. There is no well-defined heuristic to solve this problem. Analyzing other big repositories (like keras, mypy, the linux kernel and the mind foundry codebase) showed a lot of irregularities in how documentation is committed. Usually documents appear alone in a commit. Previous and successive commits are not necessary related to the documentation and may be from different authors. On the contrary, they may also be added much later in the cycle because they have been forgotten, and the change may be from a different author again

## Non-functional requirements
  - The system needs to be multi-tenant, allow multiple projects and multiple users per tenant
  - The “developers that must be notified” is not a clear concept. The only thing we know for sure is that it should be project-specific. Otherwise, it may be several things:
  - A list of e-mails manually added on configuration;
    - All the users that have been invited in the platform;
    - All the users that have ever committed some code in the repository;
    - Same as above, but in a time window (e.g. the last two months)
  - Since pricing should be per project per user per month, a change in the number of users must be detected efficiently and the set of target users should be well specified before proceeding.

## MVP architecture
The platform should be split into two macro components:
  - The agent will be live inside a docker container and will be spun up directly by clients inside their CI or from a development machine. It will receive an api key as environment variable and use it to communicate with the core server. It will collect documentation from their repository, as well as versioning information, and push them to the core;
  - The core will run on the platform’s server and will expose a RESTful api to allow the agent to push gathered data to it.
By leveraging the computation power of the clients’ build system, we will not incur excessive costs for running the platform. This is also necessary because we don’t have direct access to the client repository. Deciding how much responsibility to distribute among the two macro components is part of the trade-off we want to make. We don’t want the agent to be too heavy, otherwise clients will be discouraged from using it; nor do we want it to be too lightweight, or our margins will be lower.

## Agent
The agent will be distributed as a docker image. Clients should start the docker by mounting their repository into the image, which will contain all the needed tools. The agent will then work in two different modes:
  - When run on master, it will invoke git to analyze the difference with the last version of master pushed to the core (which may not exist if this is the first run). For every markdown document in the diff, it will push it to the core so that it can be indexed for full text search;
  - When run on a feature branch, it will still invoke git to get a diff from the last version of master available. If no documentation is present in the current branch, it will first send a message to the core (which will send a notification back to the developer if enabled) and will then terminate with an error message. This behaviour can be prevented if a specific string token is present in the commit message of any commit in the feature branch (for example, “[NoDocsNeeded]”).

## Core
The core will run as a set of micro services on our servers. The interface with the agent will be a RESTful api used for ingestion. Upon receiving a package of data from an instance of the agent, the core will:
  1. Convert all received markdown files into html using showdown;
  2. Save the html to persistent storage, indexed by relative path in the repo, for later retrieval;
  3. Split the html into chunks (p, li, h1, h2, code, ....), index each one into elastic search with full text analysis. Score tags by importance h1 > h2 > … p = li > code. The content should first be filtered for stopwords and stemmed. Code should also break down snake casing, camel casing and pascal casing. Store the project, tenant and file location along with the text content in each document;
  4. If no documentation was present for a feature branch, issue a reminder to the developer who pushed the last commit;
  5. If this is a master merge, issue a notification to all developers that a new document is available.

### File to documents dependence (won’t do)
The agent will collect relationships between files in the repo and affected documents. In general, for each affected document we will receive:
  - A list of files that were changed in the same commit, along with the total number of lines altered in each file;
  - A list of files that were changed in the n previous or successive commits, again with the number of altered lines per file.
  
Given this information, we will compute a numerical coefficient that somehow indicates how document D is related to file F:

cwlog(F(c))(1 + d(c, w)) (|c| - 1) 

Where:
  - c indicates a commit
  - w indicates the analyzed commit window, which should be from two commits behind to the next commit;
  - F(c) is the number of altered lines in file F in commit c (absolute difference between additions and insertions);
  - |c| is the total number of files changed in commit c;
  - d(c, w) is the distance of commit c from the commit under analysis (in which a document was changed), within the current window. It is 0 if it refers to the current commit.

When changes are detected that do not include any document, the platform will compute that score for each file in the total diff, sum up all the scores and if they exceed a given threshold (t.b.d.) raise a notification in the project dashboard.

## Stack
We aim to reduce the amount of custom code written. Therefore we are going to delegate tasks as much as possible to other libraries. Our chosen stack:
  - A kubernetes cluster will run the production instance. Each service will run in a pod;
  - Postgres for keeping track of users, projects, tenants, documentation file contents (not searchable), payment plans, etc…
  - ElasticSearch for full-text search within the documentation history;
  - Auth0 for authentication and authorization;
  - A dashboard template (tbd) for the frontend;
  - Stripe as a payment gateway.
  
### Searching documentation
Search will be performed by ElasticSearch full-text queries. Fuck ElasticSearch! It’s a pain to configure and a memory hog. We’ll use postgres full-text search, which has received recent support. We don’t need to support millions of requests per second. Ideally, there will be one index per tenant, and the only documents it contains should have the following fields:
  - Tenant id;
  - Project id;
  - Full path to the document in the project repo;
  - Whether the current document is extracted by a header (h1, h2, etc…), paragraph (p) or list item (li) in the markdown. This determines the corpus relevance, which is an ordinal integer score used to sort results;
  - The content of the chunk;
  - Last update time, commit and author.

### Main database
This would ideally be a Postgres database with schema-level multi-tenancy. However, we expect to have many small tenants and it seems premature to over-engineer things. Therefore we will choose the simplest quickest solution: shared tables for every tenant, with query filtering. The content of documentation files will be stored as a large blob field, for the following reasons:
  - Documentation is just text, which can easily be compressed to very small size;
  - Even in the largest code bases, the total amount of documentation is very manageable (few MBs at most);
  - Can retain history of all changes and migrations are easier.

### A note on date/times
All date/times handled in the core must be UTC. Ingestion will convert date/times from git (with time zones) to UTC before inserting them into the database. The frontend will display date/times appropriately according to the current browser’s time zone.
