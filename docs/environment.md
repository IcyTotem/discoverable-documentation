# Environment variables
All variables are required unless otherwise stated.

## Database
Postgres database used by the core:

  - `DB_HOST`
  - `DB_PORT`
  - `DB_NAME`
  - `DB_USER`
  - `DB_PASSWORD`

## Email
Nodemailer configuration used to send emails:

  - `NODEMAILER_HOST`
  - `NODEMAILER_PORT`
  - `NODEMAILER_USER`
  - `NODEMAILER_PASSWORD`
  - `DOC_NOTIFICATION_EMAIL`: email used as a sender address when sending notifications to developers
  - `DOC_NOTIFICATION_EMAIL_NAME`: display name paired with the above

## Ingestion api

  - `INGESTION_API_PORT`, *7112* by default
  - `INGESTION_API_KEY_HEADER`: the custom header used to transmit the api key. `X-Api-Key` by default
  - `INGESTION_UPLOAD_DIR`: the relative or absolute path to a directory used to temporarily store uploaded files. `./uploads` by default

## Frontend api

  - `FRONTEND_API_PORT`, *7113* by default