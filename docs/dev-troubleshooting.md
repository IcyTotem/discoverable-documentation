# Kubernetes

## Mounting files from host into a pod
It is possible to use the `volumes` configuration with `type: File` or 
`type: Directory` to create a volume with the file from the host available for read.
However, when talking about minikube, the host is the VirtualBox VM running the
minikube cluster. Therefore we have to mount the desired file/directory from the VM
host into the VM first:

```
minikube mount --ip 192.168.99.1 D:\\Windows\\HostPath:/vm/target/path &
```

The `--ip` option is there to explicitly tell minikube there to connect for the
mounting to work. To configure the VM properly for this to work, follow the
instructions described in [this github hissue](https://github.com/kubernetes/minikube/issues/2442). 
The modify the pod configuration with a new volume definition:

```
---
apiVersion: v1
kind: Pod
metadata:
  ...
spec:
  volumes:
    - name: volume-name
      hostPath:
        type: File
        path: /vm/target/path/file.txt
  containers:
  - name: container-name
    ...
    volumeMounts:
      - name: volume-name
        mountPath: /container/target/path/file.txt
```


# Typescript

## Paths and absolute imports

They are currently not support by react development environment. For server-side typescript, add the following to the `compilerOptions` of your `tsconfig.json`:

```
  "baseUrl": ".",
    "paths": {
      "@core/*": ["./src/*"],
      "@core-tests/*": ["./test/*"]
    }
```

This should make vscode tsc compiler able to resolve absolute imports starting with `@core` and `@core-tests`. For command-line scripts, remember to run:

`npm install --save-dev tsconfig-paths`

and add `--require tsconfig-paths/register` option to your node-enabled scripts. When using *Mocha Test Explorer*, also add the same to your `mochaExplorer.require` option
in the vscode `settings.json` file.


## TypeORM lacking features

  - Binding named parameters with raw queries? Nope.
  - Lazy loading for fields (like bytea)? Nope.
  - Only select some fields from an entity using find()? Nope. Well, actually yes.
  - Mapping results of a raw query to a custom class that is not an entity? Nope.
  - Transaction decorators without nasty optional parameters? Nope.
  - Easy upsert? Nope.
  - Undocumented behaviours only understandable by digging through github issues (https://github.com/typeorm/typeorm/issues/1239#issuecomment-366955628)