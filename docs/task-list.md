# Core

## Services

 - [x] Markdown <-> html converter
 - [x] Split html for full text search
 - [x] Upload a single markdown file for a project. This includes adding an archive entry, updating the latest snapshop, converting it to html, splitting the html and saving the full text search records
 - [x] Perform full text search on an entire project's documentation and retrieve the most meaningful documents
 - [x] Define all developers that take part into a project
 - [x] Issue a single notification to all developers of a project
 - [x] Create a new notification specific for a project and branch name reporting missing documentation on that branch on the latest commit
 - [x] Store diffs and commits and allow to reference them from other entities
 - [x] Get changes timeline for a project
 - [x] Associate a project/tenant to a unique api key

## Ingestion api
  - [ ] Authentication based on api key
  - [ ] Get which is the reference branch for the a project (i.e. which one is considered master)
  - [ ] Upload one or more markdown files to a single project (call off master)
  - [ ] Signal that a reminder is necessary (call off feature branch)

## Frontend api

  - [x] Creation date on all entities -> actually only Project needs it
  - [ ] Add project description and icon fields
  - [ ] CRUD on tenants
  - [ ] " users
  - [ ] " projects
  - [ ] Display documentation of a project
  - [ ] Full text search inside a project
  - [ ] Get/dismiss reminders
  - [ ] Buy license for flat icons: https://www.flaticon.com/packs/project-management-3, https://www.flaticon.com/free-icon/onigiri_774827

## Backlog

  - [ ] Split camel/snake/pascal casing in code
  - [ ] OR search

# Agent

  - [x] Find all commit authors in the repository, push them to the api
  - [x] Perform diff from the reference branch and fail if no documentation is included
  - [x] Call api for reminders on failure
  - [x] Avoid failure if commit diff contains a pre-defined ignore tag (which should be obtained via another api call)
  - [x] Perform diff from latest recorded commit (to be obtained from the api) and push all new/changed markdown files. Then invoke endpoint to notify developers
  - [ ] Dockerfile and main script to run the agent