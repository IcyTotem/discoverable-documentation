import React from 'react';
import { action } from '@storybook/addon-actions';
import { preview } from '../stories';
import { SearchBox } from './SearchBox';

export default { title: 'SearchBox' };

export const enabled = () => preview(<SearchBox />);

export const disabled = () => preview(<SearchBox disabled />);

export const withCallback = () => preview(<SearchBox onConfirmSearch={action('Confirm search')} />);
