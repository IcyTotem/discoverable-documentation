import React from 'react';
import { action } from '@storybook/addon-actions';
import { preview } from '../stories';
import { FileTree, Folder } from './FileTree';

export default { title: 'FileTree' };

const emptyRoot: Folder = { id: 0, name: 'Empty repo', children: [] };
const root: Folder = {
  id: 0,
  name: 'Repository',
  children: [
    {
      id: 1,
      name: 'docs',
      children: [{ name: 'how-to.md', id: 7 }],
    },
    {
      id: 2,
      name: 'examples',
      children: [
        {
          name: 'train.md',
          id: 10,
        },
        {
          name: 'test a file with kind of a long name.md',
          id: 41,
        },
      ],
    },
    {
      name: 'README.md',
      id: 22,
    },
  ],
};

export const defaultState = () => preview(<FileTree root={root} onFileSelected={action('File selected')} />);

export const withNoFiles = () => preview(<FileTree root={emptyRoot} onFileSelected={action('File selected')} />);
