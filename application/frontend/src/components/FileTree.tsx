/* eslint-disable react/prop-types */
import React from 'react';
import './FileTree.css';

export interface Node {
  id: number;
  name: string;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface File extends Node {}

export interface Folder extends Node {
  children: Array<File | Folder>;
}

export interface Props {
  root: Folder;
  onFileSelected?: (id: number) => void;
}

export const isFolder = (node: Node): node is Folder => 'children' in node;
export const isFile = (node: Node): node is File => !isFolder(node);
const byName = (a: Node, b: Node): number => a.name.localeCompare(b.name);

interface FileItemProps extends File {
  /**
   * Whether to render the current list item as selected. This correponds to the
   * "selected" css class, which is appended to the existing ones.
   */
  isSelected: boolean;

  /**
   * Whenever the user clicks on the list item, onFileClick will be invoked with the id of the current file.
   */
  onFileClick: (id: number) => void;
}

/**
 * Used to render a single file entry.
 */
const FileItem: React.SFC<FileItemProps> = (props: FileItemProps) => {
  const idBoundOnClick = React.useCallback(() => props.onFileClick(props.id), [props]);
  const liClass = `file ${props.isSelected ? 'selected' : ''}`;
  return (
    <li key={props.id} className={liClass} onClick={idBoundOnClick}>
      <span className="icon">
        <i className="fas fa-file" />
      </span>
      {props.name}
    </li>
  );
};

interface FolderItemProps extends Folder {
  /**
   * This single optional number is used to determine which file renders as "selected".
   * The value is propagated downwards to all descendant FileItem components.
   */
  selectedFileId?: number;

  /**
   * This array of numbers contains the IDs of all folders that are currently "expanded".
   * An expanded folder will render all its contained files as FileItems and all its
   * sub folders as FolderItems. A folder that is NOT expanded will have no children.
   */
  expandedFoldersIds: number[];

  /**
   * Whenever the user clicks on the current folder or any descendant sub folder, onFolderClick
   * will be invoked with the id of the clicked folder.
   */
  onFolderClick: (id: number) => void;

  /**
   * Whenever the user clicks on any child or descendant file, onFileClick will be invoked with
   * the id of the clicked file.
   */
  onFileClick: (id: number) => void;
}

/**
 * Used to render a folder entry. It has a name and icon, then it displays a list of
 * all sub folders and finally a list of all files that are direct children of the current
 * folder. Both sub folders and files are lexicographically sorted in ascending order.
 */
const FolderItem: React.SFC<FolderItemProps> = (props: FolderItemProps) => {
  const idBoundOnClick = React.useCallback(() => props.onFolderClick(props.id), [props]);
  const isExpanded = props.expandedFoldersIds.includes(props.id);
  const iconClass = `fas fa-folder${isExpanded ? '-open' : ''}`;

  const renderSubFolder = (subFolder: Folder) => <FolderItem key={subFolder.id} {...props} {...subFolder} />;
  const renderFile = (file: File) => (
    <FileItem key={file.id} {...file} isSelected={file.id === props.selectedFileId} onFileClick={props.onFileClick} />
  );

  // Notice we use key={iconClass} on the div that contains the folder name and icon.
  // By having a key dependent on the icon class, we force React to re-mount that div
  // whenever the icon changes. If that did not happen, the FontAwesome javascript library
  // would not be able to replace the <i/> element with an svg dynamically.
  return (
    <li key={props.id} className="folder">
      <div key={iconClass} onClick={idBoundOnClick}>
        <span className="icon has-text-primary">
          <i className={iconClass} />
        </span>
        {props.name}
      </div>
      {isExpanded && (
        <ul>
          {props.children
            .filter(isFolder)
            .sort(byName)
            .map(renderSubFolder)}
          {props.children
            .filter(isFile)
            .sort(byName)
            .map(renderFile)}
        </ul>
      )}
    </li>
  );
};

interface FileTreeState {
  selectedFileId?: number;
  expandedFoldersIds: number[];
}

export class FileTree extends React.Component<Props, FileTreeState> {
  public constructor(props: Props) {
    super(props);
    this.state = { expandedFoldersIds: [props.root.id] };
  }

  private onFolderClick = (folderId: number) => {
    const expandedIds = this.state.expandedFoldersIds;
    if (expandedIds.includes(folderId)) {
      this.setState({ expandedFoldersIds: expandedIds.filter(id => id !== folderId) });
    } else {
      this.setState({ expandedFoldersIds: [...expandedIds, folderId] });
    }
  };

  private onFileClick = (fileId: number) => {
    if (fileId !== this.state.selectedFileId) {
      this.setState({ selectedFileId: fileId });
      if (this.props.onFileSelected) {
        this.props.onFileSelected(fileId);
      }
    }
  };

  public render() {
    return (
      <ul className="file-tree">
        <FolderItem
          {...this.props.root}
          {...this.state}
          onFileClick={this.onFileClick}
          onFolderClick={this.onFolderClick}
        />
      </ul>
    );
  }
}
