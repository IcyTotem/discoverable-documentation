import React from 'react';
import { preview } from '../stories';
import { TimelineEntry } from './TimelineEntry';

export default { title: 'TimelineEntry' };

export const withUpdatedDocuments = () =>
  preview(
    <TimelineEntry
      authorName="Giulio Flavio"
      authorEmail="giulio.flavio@pna.it"
      branch={{ name: 'feature/oddballs', isMaster: false }}
      commitHash="cf1827ed71baa8172fdd891009"
      containsDocumentation={true}
      triggersReminder={false}
      creationDateTime="2020-01-17T13:19:55Z"
      documentPaths={[
        'docs/how-to.md',
        'user/start/beginner.md',
        'user/start/intermediate.md',
        'user/start/exprt.md',
        'tech/decisions/adr-001-states.md',
        'tech/decisions/adr-002-props.md',
      ]}
    />,
  );

export const withSuppressedNotification = () =>
  preview(
    <TimelineEntry
      authorName="Giulio Flavio"
      authorEmail="giulio.flavio@pna.it"
      branch={{ name: 'feature/oddballs', isMaster: false }}
      commitHash="cf1827ed71baa8172fdd891009"
      containsDocumentation={false}
      triggersReminder={false}
      creationDateTime="2020-01-17T13:19:55Z"
      documentPaths={[]}
    />,
  );

export const withNoDocumentation = () =>
  preview(
    <TimelineEntry
      authorName="Giulio Flavio"
      authorEmail="giulio.flavio@pna.it"
      branch={{ name: 'feature/oddballs', isMaster: false }}
      commitHash="cf1827ed71baa8172fdd891009"
      containsDocumentation={false}
      triggersReminder={true}
      creationDateTime="2020-01-17T13:19:55Z"
      documentPaths={[]}
    />,
  );

export const onMasterBranch = () =>
  preview(
    <TimelineEntry
      authorName="Giulio Flavio"
      authorEmail="giulio.flavio@pna.it"
      branch={{ name: 'master', isMaster: true }}
      commitHash="cf1827ed71baa8172fdd891009"
      containsDocumentation={false}
      triggersReminder={false}
      creationDateTime="2020-01-17T13:19:55Z"
      documentPaths={[]}
    />,
  );
