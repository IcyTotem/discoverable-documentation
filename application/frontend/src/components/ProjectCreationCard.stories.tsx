import React from 'react';
import { preview } from '../stories';
import { ProjectCreationCard } from './ProjectCreationCard';

export default { title: 'ProjectCreationCard' };

export const defaultState = () =>
  preview(
    <div className="columns">
      <div className="column is-one-third">
        <ProjectCreationCard />
      </div>
    </div>,
  );
