import React from 'react';
import BoxIcon from './box.svg';
import ChecklistIcon from './checklist.svg';
import ClipboardIcon from './clipboard.svg';
import ConnectionIcon from './connection.svg';
import DialogIcon from './dialog.svg';
import DocumentIcon from './document.svg';
import FolderIcon from './folder.svg';
import GearIcon from './gear.svg';
import StatisticsIcon from './statistics.svg';

export type IconName =
  | 'box'
  | 'checklist'
  | 'clipboard'
  | 'connection'
  | 'dialog'
  | 'document'
  | 'folder'
  | 'gear'
  | 'statistics';

const iconMap: { [key in IconName]: string } = {
  box: BoxIcon,
  checklist: ChecklistIcon,
  clipboard: ClipboardIcon,
  connection: ConnectionIcon,
  dialog: DialogIcon,
  document: DocumentIcon,
  folder: FolderIcon,
  gear: GearIcon,
  statistics: StatisticsIcon,
};

const allIconNames = Object.keys(iconMap) as IconName[];
const trivialHash = (text: string): number => text.split('').reduce((acc, c) => acc + c.charCodeAt(0), 0);
const modIconName = (n: number): IconName => allIconNames[n % allIconNames.length];
const seedIconName = (seed: number | string): IconName => {
  const n = typeof seed === 'number' ? seed : trivialHash(seed);
  return modIconName(n);
};

export const Icon = (props: { name: IconName }) => <img src={iconMap[props.name]} alt={props.name} />;

export const RandomIcon = (props: { seed: number | string }) => <Icon name={seedIconName(props.seed)} />;
