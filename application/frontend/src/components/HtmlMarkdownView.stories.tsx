import React from 'react';
import { preview } from '../stories';
import { HtmlMarkdownView } from './HtmlMarkdownView';

export default { title: 'HtmlMarkdownView' };

const exampleHtml = `
  <h1>Live demo</h1>
  <p>Changes are automatically rendered as you type.</p>
  <h2>Table of Contents</h2>
  <ul>
    <li><a href="#html-block-below">HTML block below</a></li>
    <li><a href="#how-about-some-code">How about some code?</a></li>
    <li><a href="#tables">Tables?</a></li><li><a href="#more-info">More info?</a></li>
  </ul>
  
  <h2>HTML block below</h2>
  <blockquote>
    This blockquote will change based on the HTML settings above.
  </blockquote>
  
  <h2>How about some code?</h2>
  <pre><code class="language-js hljs javascript">var React = require('react');
var Markdown = require('react-markdown');

React.render(
  &lt;Markdown source="# Your markdown here" />,
  document.getElementById('content')
);</code></pre>
  
  <p>Pretty neat, eh?</p>
  
  <h2>Tables?</h2>
  <table>
    <thead><tr><th>Feature</th><th>Support</th></tr></thead>
    <tbody>
      <tr><td>tables</td><td>✔</td></tr>
      <tr><td>alignment</td><td>✔</td></tr>
      <tr><td>wewt</td><td>✔</td></tr>
    </tbody>
  </table>
  
  <h2>More info?</h2>
  <p>Read usage information and more on <a href="//github.com/rexxars/react-markdown">GitHub</a></p>
  <hr>
`;

export const defaultState = () => preview(<HtmlMarkdownView html={exampleHtml} />);