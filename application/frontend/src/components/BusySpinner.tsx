import React from 'react';
import './BusySpinner.css';

export const BusySpinner = () => (
  <div className="level">
    <div className="level-item">
      <div className="lds-ellipsis">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    </div>
  </div>
);
