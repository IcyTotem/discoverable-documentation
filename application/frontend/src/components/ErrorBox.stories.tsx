import React from 'react';
import { preview } from '../stories';
import { ErrorBox } from './ErrorBox';

export default { title: 'ErrorBox' };

export const withoutTitle = () =>
  preview(<ErrorBox message="Something went wrong during the training of our monkeys" />);

export const withTitle = () =>
  preview(<ErrorBox title="Ooops!" message="Something went wrong during the training of our monkeys" />);
