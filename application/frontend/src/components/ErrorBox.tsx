import React from 'react';

export interface Props {
  title?: string;
  message: string;
}

export const ErrorBox = (props: Props) => (
  <div className="level">
    <div className="level-item">
      <article className="message is-danger is-small" style={{ maxWidth: 350 }}>
        <div className="message-header">
          <p>{props.title || 'Error'}</p>
        </div>
        <div className="message-body">{props.message}</div>
      </article>
    </div>
  </div>
);
