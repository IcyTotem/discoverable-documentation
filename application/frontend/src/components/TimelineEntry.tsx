import React from 'react';
import Gravatar from 'react-gravatar';
import { DateTime } from './DateTime';
import { DocumentsList } from './DocumentsList';

export interface Props {
  authorName: string;
  authorEmail: string;
  commitHash: string;
  creationDateTime: string;
  triggersReminder: boolean;
  containsDocumentation: boolean;
  documentPaths?: string[];
  branch: {
    name: string;
    isMaster: boolean;
  };
}

enum EntryAnnotation {
  POSITIVE,
  NEUTRAL,
  NEGATIVE,
}

const annotationIconClasses = {
  [EntryAnnotation.POSITIVE]: 'fa-check-circle',
  [EntryAnnotation.NEGATIVE]: 'fa-exclamation-circle',
  [EntryAnnotation.NEUTRAL]: 'fa-info-circle',
};

const annotationColorClasses = {
  [EntryAnnotation.POSITIVE]: 'has-text-success',
  [EntryAnnotation.NEGATIVE]: 'has-text-danger',
  [EntryAnnotation.NEUTRAL]: 'has-text-grey-light',
};

const getAnnotation = (props: Props) => {
  if (!props.containsDocumentation && props.triggersReminder) {
    return EntryAnnotation.NEGATIVE;
  }

  if (props.containsDocumentation && props.documentPaths && props.documentPaths.length > 0) {
    return EntryAnnotation.POSITIVE;
  }

  return EntryAnnotation.NEUTRAL;
};

const getIcon = (props: Props) => {
  const annotation = getAnnotation(props);
  return (
    <span className={`icon is-pulled-right is-size-4 ${annotationColorClasses[annotation]}`}>
      <i className={`fas ${annotationIconClasses[annotation]}`} />
    </span>
  );
};

const getComment = (props: Props) => {
  const annotation = getAnnotation(props);
  const colorClass = annotationColorClasses[annotation];
  const documentsCount = props.documentPaths ? props.documentPaths.length : 0;
  switch (annotation) {
    case EntryAnnotation.NEGATIVE:
      return <em className={colorClass}>without any documentation</em>;
    case EntryAnnotation.NEUTRAL:
      return <em className={colorClass}>without any documentation (notification suppressed)</em>;
    case EntryAnnotation.POSITIVE:
      return <em className={colorClass}>with {documentsCount} updated documents!</em>;
  }
};

export const TimelineEntry: React.SFC<Props> = (props: Props) => (
  <article className="media">
    <figure className="media-left">
      <p className="image is-64x64">
        <Gravatar email={props.authorEmail} size={64} default="identicon" />
      </p>
    </figure>
    <div className="media-content">
      <div className="content">
        <p>
          <strong>{props.authorName}</strong> <small>{props.authorEmail}</small>
          {getIcon(props)}
          <p className="is-clearfix" style={{ marginBottom: 0 }}>
            {props.branch.isMaster ? 'Merged' : 'Pushed'}
            &nbsp;
            <span className="has-text-info">{props.commitHash}</span>
            &nbsp;
            {props.branch.isMaster ? 'into branch' : 'to branch'}
            &nbsp;
            <span className="has-text-info">{props.branch.name}</span>
            &nbsp;
            {getComment(props)}
            {props.containsDocumentation && props.documentPaths && (
              <DocumentsList documentPaths={props.documentPaths} maxLength={5} />
            )}
          </p>
          <small>
            <DateTime format="smart" isoDateTimeString={props.creationDateTime} />
          </small>
        </p>
      </div>
    </div>
  </article>
);
