import React from 'react';
import { Folder, isFolder, FileTree } from './FileTree';

export interface Document {
  id: number;
  path: string;
}

export interface Props {
  documents: Array<Document>;
  rootName?: string;
  onDocumentSelected?: (id: number) => void;
}

/**
 * Create all the folder node in the tree with given root in order to represent the path
 * made up of pathChunks.
 * @param root Root node of the tree structure.
 * @param pathChunks Array of strings representing the single traversed subfolders.
 * @param idGenerator Function used to generate new valid IDs for created folder nodes.
 */
const createNodesOnPath = (root: Folder, pathChunks: string[], idGenerator: () => number): Folder => {
  let currentFolder = root;

  for (const pathChunk of pathChunks) {
    // Tries to find a child node of the current folder that is also a folder
    // and whose name is equal to the current pathChunk
    const matchingSubFolder = currentFolder.children.filter(isFolder).find(c => c.name === pathChunk);

    if (matchingSubFolder) {
      // If it is found, it becomes the new current folder and the loop continues
      currentFolder = matchingSubFolder;
    } else {
      // Otherwise, a new folder node is created
      const newNode = { id: idGenerator(), name: pathChunk, children: [] };
      currentFolder.children.push(newNode);
      currentFolder = newNode;
    }
  }

  return currentFolder;
};

/**
 * Given a list of document paths, build a tree structure to represent all the files
 * and folders in the list, in the format accepted by FileTree.
 */
const buildTreeStructure = (documents: Array<Document>, rootName: string): Folder => {
  const root: Folder = { id: 0, name: rootName, children: [] };

  // Id is only needed by the FileTree component to keep track of expanded folder
  // but has no real meaning for the backend. So we use negative numbers to avoid
  // clashing with document ids (actual database ids). This is a regrettable hack
  // but makes the tree structure easier to manage.
  let nextFolderId = -1;
  const idGenerator = () => nextFolderId--;

  for (const document of documents) {
    const pathChunks = document.path.split('/').filter(chunk => chunk.trim().length > 0);
    const fileName = pathChunks[pathChunks.length - 1];
    let referenceNode = root;

    if (pathChunks.length > 1) {
      const subFolderChunks = pathChunks.slice(0, pathChunks.length - 1);
      referenceNode = createNodesOnPath(root, subFolderChunks, idGenerator);
    }

    referenceNode.children.push({ id: document.id, name: fileName });
  }

  return root;
};

export const DocumentsTree: React.SFC<Props> = (props: Props) => {
  const { documents, rootName } = props;
  const treeStructure: Folder = React.useMemo(() => buildTreeStructure(documents, rootName || 'Repository'), [
    documents,
    rootName,
  ]);
  return <FileTree root={treeStructure} onFileSelected={props.onDocumentSelected} />;
};
