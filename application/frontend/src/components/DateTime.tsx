import React from 'react';
import Moment from 'react-moment';

export interface Props {
  format: 'smart';
  isoDateTimeString: string;
}

export const DateTime = (props: Props) => (
  <Moment interval={0} format="HH:mm, MMMM Do YYYY" local>
    {props.isoDateTimeString}
  </Moment>
);
