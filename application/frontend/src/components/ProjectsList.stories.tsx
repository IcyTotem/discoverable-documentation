import React from 'react';
import { preview } from '../stories';
import { ProjectsList } from './ProjectsList';

export default { title: 'ProjectsList' };

const projectExample = {
  id: 2,
  name: 'Big project',
  masterBranch: 'master',
  ownerName: 'Rob Estrovel',
  creationDateTime: '2020-01-14T11:00:21Z',
  commitsCount: 21,
  documentsCount: 14,
  problemsCount: 1,
};

const generateProjects = (length: number) => Array.from({ length }).map(() => projectExample);

export const withLessThanThreeItems = () => preview(<ProjectsList projects={generateProjects(2)} />);

export const withMoreThanThreeItems = () => preview(<ProjectsList projects={generateProjects(3)} />);
