import React from 'react';
import { RandomIcon } from './icons/Icon';
import { DateTime } from './DateTime';

export interface Props {
  id: number;
  name: string;
  description?: string;
  ownerName: string;
  masterBranch: string;
  problemsCount: number;
  documentsCount: number;
  commitsCount: number;
  creationDateTime: string;
}

const LevelItem = (props: { title: string; colorClass: string; iconClass: string; value: number }) => (
  <div className={`level-item has-text-centered ${props.colorClass}`} title={props.title}>
    <span className="icon">
      <i className={`fas ${props.iconClass}`} />
    </span>
    &nbsp;
    {props.value}
  </div>
);

export const ProjectCard = (props: Props) => (
  <div className="card">
    <div className="card-content">
      <div className="media" style={{ marginBottom: 15 }}>
        <div className="media-left">
          <figure className="image is-48x48">
            <RandomIcon seed={props.name} />
          </figure>
        </div>
        <div className="media-content">
          <p className="title is-4 has-text-info">{props.name}</p>
          <p className="subtitle is-6">
            on branch <strong>{props.masterBranch}</strong>
          </p>
        </div>
      </div>

      <div className="content">
        {props.description && <p>{props.description}</p>}
        <p className="is-size-7">
          Created by {props.ownerName}
          <br />
          At <DateTime isoDateTimeString={props.creationDateTime} format="smart" />
        </p>
        <nav className="level">
          <LevelItem
            title="Number of analyzed commits"
            colorClass="has-text-info"
            iconClass="fa-code"
            value={props.commitsCount}
          />
          <LevelItem
            title="Number of available documents"
            colorClass="has-text-success"
            iconClass="fa-file"
            value={props.documentsCount}
          />
          <LevelItem
            title="Number of detected problems"
            colorClass="has-text-danger"
            iconClass="fa-bug"
            value={props.problemsCount}
          />
        </nav>
      </div>
    </div>
  </div>
);
