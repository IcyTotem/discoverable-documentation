import React from 'react';

type KeyEvent = React.KeyboardEvent<HTMLInputElement>;
export type SearchCallback = (searchQuery: string) => void;

export interface Props {
  disabled?: boolean;
  onConfirmSearch?: SearchCallback;
}

export const SearchBox: React.SFC<Props> = (props: Props) => {
  const { disabled, onConfirmSearch } = props;
  const [input, setInput] = React.useState('');
  const boundOnKeyDown = React.useCallback((e: KeyEvent) => {
    if (e.key === 'Enter' && onConfirmSearch) {
      e.preventDefault();
      onConfirmSearch(input);
    }
  }, [input, onConfirmSearch])

  return (
    <div className="field">
      <div className="control has-icons-left">
        <input
          value={input}
          className="input is-medium"
          type="text"
          placeholder="Search..."
          disabled={!!disabled}
          onInput={e => setInput((e.target as HTMLInputElement).value)}
          onKeyDown={boundOnKeyDown}
        />
        <span className="icon is-small is-left">
          <i className="fas fa-search"></i>
        </span>
      </div>
    </div>
  );
};
