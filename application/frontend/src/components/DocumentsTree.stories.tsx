import React from 'react';
import { action } from '@storybook/addon-actions';
import { preview } from '../stories';
import { DocumentsTree } from './DocumentsTree';

export default { title: 'DocumentsTree' };

const exampleDocumentsList = [
  { id: 55, path: 'architecture/backend/tech.md' },
  { id: 1, path: 'architecture/frontend/tech.md' },
  { id: 3, path: 'architecture/frontend/api-docs.md' },
  { id: 3, path: '/decisions/faff.md' },
  { id: 8, path: 'decisions/queue.md' },
  { id: 9, path: 'readme.md' },
  { id: 11, path: 'license.md' },
];

export const defaultState = () =>
  preview(<DocumentsTree documents={exampleDocumentsList} onDocumentSelected={action('Document selected')} />);

export const withCustomRootName = () =>
  preview(
    <DocumentsTree
      documents={exampleDocumentsList}
      rootName="Project root"
      onDocumentSelected={action('Document selected')}
    />,
  );

export const withNoDocuments = () =>
  preview(<DocumentsTree documents={[]} onDocumentSelected={action('Document selected')} />);

export const withNoSelectedCallback = () => preview(<DocumentsTree documents={exampleDocumentsList} />);
