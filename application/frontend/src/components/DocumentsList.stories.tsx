import React from 'react';
import { preview } from '../stories';
import { DocumentsList } from './DocumentsList';

export default { title: 'DocumentList' };

export const withFewItems = () => preview(<DocumentsList documentPaths={['doc1', 'doc2']} maxLength={5} />);

export const withManyItems = () =>
  preview(<DocumentsList documentPaths={['doc1', 'doc2', 'doc3', 'doc4', 'doc5']} maxLength={3} />);
