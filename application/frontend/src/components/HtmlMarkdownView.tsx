import React from 'react';
import './HtmlMarkdownView.css';

export interface Props {
  html: string;
}

export const HtmlMarkdownView: React.SFC<Props> = (props: Props) => (
  <div className="html-markdown" dangerouslySetInnerHTML={{ __html: props.html }}></div>
);
