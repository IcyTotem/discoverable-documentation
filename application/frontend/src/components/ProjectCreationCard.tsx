import React from 'react';

export const ProjectCreationCard = () => (
  <div className="card" style={{ minHeight: 186 }}>
    <div className="card-content">
      <div className="has-text-centered" style={{ marginTop: 40 }}>
        <button className="button is-link">Create new project</button>
      </div>
    </div>
  </div>
);
