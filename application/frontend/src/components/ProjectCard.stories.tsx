import React from 'react';
import { preview } from '../stories';
import { ProjectCard } from './ProjectCard';

export default { title: 'ProjectCard' };

export const withDescription = () =>
  preview(
    <div className="columns">
      <div className="column is-one-third">
        <ProjectCard
          id={2}
          name="Big project"
          masterBranch="master"
          ownerName="Rob Estrovel"
          creationDateTime="2020-01-14T11:00:21Z"
          description="This is a very big project"
          commitsCount={21}
          documentsCount={14}
          problemsCount={1}
        />
      </div>
    </div>,
  );

export const withoutDescription = () =>
  preview(
    <div className="columns">
      <div className="column is-one-third">
        <ProjectCard
          id={2}
          name="Big project"
          masterBranch="master"
          ownerName="Rob Estrovel"
          creationDateTime="2020-01-14T11:00:21Z"
          commitsCount={21}
          documentsCount={14}
          problemsCount={1}
        />
      </div>
    </div>,
  );
