import React from 'react';

export interface Props {
  documentPaths: string[];
  maxLength?: number;
}

const noTopMargin = { marginTop: 0 };

export const DocumentsList: React.SFC<Props> = (props: Props) => {
  const { documentPaths, maxLength } = props;
  const displayedDocuments = documentPaths.sort().slice(0, maxLength || 5);
  const leftoverCount = documentPaths.length - displayedDocuments.length;
  return (
    <ul style={noTopMargin}>
      {displayedDocuments.map(documentPath => (
        <li key={documentPath} className="is-family-monospace" style={noTopMargin}>
          {documentPath}
        </li>
      ))}
      {leftoverCount > 0 && <li style={noTopMargin}>... and {leftoverCount} other documents</li>}
    </ul>
  );
};
