import React from 'react';
import { ProjectCreationCard } from './ProjectCreationCard';
import { ProjectCard, Props as ProjectCardProps } from './ProjectCard';

export interface Props {
  projects: Array<ProjectCardProps>;
}

/**
 * Split an array of arbitrary length into an array of chunks of fixed length.
 * @example For example, if array = [0, 2, 3, 1, 2, 6, 4] and chunkSize = 3, the function will
 *     return [[0, 2, 3], [1, 2, 6], [4]]. The last chunk contains at most chunkSize items.
 */
const splitIntoChunks = function<T>(array: T[], chunkSize: number): Array<T[]> {
  const allChunks: Array<T[]> = [];
  let currentChunk: T[] = [];

  for (const item of array) {
    currentChunk.push(item);

    if (currentChunk.length >= chunkSize) {
      allChunks.push(currentChunk);
      currentChunk = [];
    }
  }

  // Any leftovers should be pushed to the tail of the list
  allChunks.push(currentChunk);

  return allChunks;
};

const ThreeProjectsChunk = (props: { projects: Array<ProjectCardProps> }) => (
  <div className="columns">
    {props.projects.map(projectProps => (
      <div key={projectProps.id} className="column is-one-third">
        <ProjectCard {...projectProps} />
      </div>
    ))}

    {props.projects.length < 3 && (
      <div className="column is-one-third">
        <ProjectCreationCard />
      </div>
    )}
  </div>
);

export const ProjectsList = (props: Props) => (
  <React.Fragment>
    {splitIntoChunks(props.projects, 3).map((chunk, index) => (
      <ThreeProjectsChunk key={index} projects={chunk} />
    ))}
  </React.Fragment>
);
