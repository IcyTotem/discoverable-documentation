import React from 'react';

export interface CrumbProps {
  text: string;
}

export interface Props {
  crumbs: Array<CrumbProps>;
}

interface PositionalCrumbProps extends CrumbProps {
  isFirst: boolean;
  isLast: boolean;
}

const Crumb = (props: PositionalCrumbProps) => (
  <li className={props.isLast ? 'is-active' : undefined}>
    <a href="#">
      {props.isFirst && (
        <span className="icon is-small">
          <i className="fas fa-home" aria-hidden="true"></i>
        </span>
      )}
      <span>{props.text}</span>
    </a>
  </li>
);

export const Breadcrumbs = (props: Props) => (
  <nav className="breadcrumb" aria-label="breadcrumbs">
    <div className="container">
      <ul>
        {props.crumbs.map((crumbProps, index) => (
          <Crumb {...crumbProps} key={index} isFirst={index === 0} isLast={index === props.crumbs.length - 1} />
        ))}
      </ul>
    </div>
  </nav>
);
