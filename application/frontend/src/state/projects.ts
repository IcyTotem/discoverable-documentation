export interface ProjectItemState {
  id: number;
  name: string;
  description?: string;
  ownerName: string;
  masterBranch: string;
  problemsCount: number;
  documentsCount: number;
  commitsCount: number;
  creationDateTime: string;
}

export interface ProjectsListState {
  error?: string;
  items?: ProjectItemState[];
}
