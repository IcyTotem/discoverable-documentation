import { Dispatch } from 'redux';
import { BackendException, BackendResponse, isBackendException } from '../apis/base';

export enum ActionStatus {
  COMPLETED,
  PENDING,
  FAILED,
}

export interface ThunkAction {
  type: string;
}

export interface SuccessfulBackendAction<T> extends ThunkAction {
  status: ActionStatus.COMPLETED;
  payload: T;
}

export interface FailedBackendAction extends ThunkAction {
  status: ActionStatus.FAILED;
  exception: BackendException;
}

export interface PendingBackendAction extends ThunkAction {
  status: ActionStatus.PENDING;
}

export type BackendAction<T> = SuccessfulBackendAction<T> | FailedBackendAction | PendingBackendAction;

/**
 * Utility function to ease the dispatching of actions as a consequence of the resolution of a promise,
 * in particular asynchronous api requests. The function first immediately dispatches a PendingBackendAction
 * with the given type. Then it waits for the promise to resolve and depending on its outcome, dispatches
 * either a FailedBackendAction or a SuccessfulBackendAction.
 * @param request Promise obtained by an invocation to a method of ManagedClient. It is assumed that
 *     this promise will ALWAYS resolve successfully, as rejections are handled by the ManagedClient internally.
 * @param dispatch This comes from the clojure on the action dispatcher factory in which this function
 *     should be called.
 */
export async function handleBackendRequest<T>(
  actionType: string,
  request: Promise<BackendResponse<T>>,
  dispatch: Dispatch,
): Promise<BackendAction<T>> {
  dispatch<PendingBackendAction>({
    type: actionType,
    status: ActionStatus.PENDING,
  });

  const response = await request;

  if (isBackendException(response)) {
    return dispatch<FailedBackendAction>({
      type: actionType,
      status: ActionStatus.FAILED,
      exception: response,
    });
  } else {
    return dispatch<SuccessfulBackendAction<T>>({
      type: actionType,
      status: ActionStatus.COMPLETED,
      payload: response,
    });
  }
}
