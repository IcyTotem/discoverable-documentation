import { Dispatch } from 'redux';
import { fetchProjectsList, ProjectsListResponse } from '../apis/projects';
import { BackendAction, ThunkAction, handleBackendRequest } from './base';

export const LOAD_PROJECTS_LIST = 'LOAD_PROJECTS_LIST';
export const CLEAR_PROJECTS_LIST = 'CLEAR_PROJECTS_LIST';

export type LoadProjectsListAction = BackendAction<ProjectsListResponse>;
export type ClearProjectsListAction = ThunkAction;
export type AnyProjectsListAction = LoadProjectsListAction | ClearProjectsListAction;

export const loadProjectsList = (dispatch: Dispatch) => () =>
  handleBackendRequest(LOAD_PROJECTS_LIST, fetchProjectsList(), dispatch);
export const clearProjectsList = (dispatch: Dispatch) => () => dispatch<ThunkAction>({ type: CLEAR_PROJECTS_LIST });
