import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';

export interface BackendException {
  statusCode: number;
  failureReason: string;
}

export type BackendResponse<T> = T | BackendException;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const isBackendException = (object: any): object is BackendException =>
  'failureReason' in object && 'statusCode' in object;

export class ManagedClient {
  constructor(private readonly client: AxiosInstance) {}

  public async get<T>(url: string, config?: AxiosRequestConfig): Promise<BackendResponse<T>> {
    try {
      const response = await this.client.get<T>(url, config);
      return this.handleResponse(response);
    } catch (e) {
      return this.handleError(e);
    }
  }

  private handleResponse<T>(response: AxiosResponse<T>): BackendResponse<T> {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const rawResponseData = response.data as any;

    if (response.status >= 200 && response.status < 300) {
      return response.data;
    }

    if (rawResponseData.error) {
      return { statusCode: response.status, failureReason: rawResponseData.error };
    }

    return { statusCode: response.status, failureReason: response.statusText };
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private handleError(e: any): BackendException {
    return { statusCode: 0, failureReason: e ? e.toString() : 'unspecified' };
  }
}

const client = axios.create({ baseURL: 'http://localhost:7113/js' });
export const http = new ManagedClient(client);
