import { http } from './base';

export interface ProjectItemResponse {
  id: number;
  name: string;
  description?: string;
  ownerName: string;
  masterBranch: string;
  problemsCount: number;
  documentsCount: number;
  commitsCount: number;
  creationDateTime: string;
}

export interface ProjectsListResponse {
  projects: ProjectItemResponse[];
}

export const fetchProjectsList = () => http.get<ProjectsListResponse>('/projects');
