import React from 'react';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import { State } from '../reducers/root';
import { loadProjectsList, clearProjectsList } from '../actions/projects';
import { Props as ProjectCardProps } from '../components/ProjectCard';
import { BackendAction, ThunkAction } from '../actions/base';
import { ProjectsListResponse } from '../apis/projects';
import { BusySpinner } from '../components/BusySpinner';
import { ErrorBox } from '../components/ErrorBox';
import { Breadcrumbs } from '../components/Breadcrumbs';
import { ProjectsList } from '../components/ProjectsList';

export interface Props {
  error?: string;
  projects?: Array<ProjectCardProps>;
  actions: {
    loadProjectsList: () => Promise<BackendAction<ProjectsListResponse>>;
    clearProjectsList: () => ThunkAction;
  };
}

const mapStateToProps = (state: State) => ({
  error: state.projectsList.error,
  projects: state.projectsList.items,
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  actions: {
    loadProjectsList: loadProjectsList(dispatch),
    clearProjectsList: clearProjectsList(dispatch),
  },
});

export class ProjectsListContainer extends React.Component<Props> {
  componentDidMount() {
    this.props.actions.loadProjectsList();
  }

  componentWillUnmount() {
    this.props.actions.clearProjectsList();
  }

  render() {
    const { error, projects } = this.props;

    if (projects) {
      return (
        <React.Fragment>
          <Breadcrumbs crumbs={[{ text: 'All projects' }]} />
          <ProjectsList projects={projects} />
        </React.Fragment>
      );
    }

    if (error) {
      return <ErrorBox message={error} />;
    }

    return <BusySpinner />;
  }
}

export const ConnectedProjectsListContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProjectsListContainer);
