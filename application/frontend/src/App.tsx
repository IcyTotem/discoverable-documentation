import React from 'react';
import { ConnectedProjectsListContainer } from './pages/ProjectsListContainer';

const headerNavStyle = {
  boxShadow: '0 0.5em 1em -0.125em rgba(10,10,10,.1), 0 0 0 1px rgba(10,10,10,.02)',
};

const Header: React.SFC<{}> = () => {
  return (
    <nav className="navbar is-info" role="navigation" aria-label="main navigation" style={headerNavStyle}>
      <div className="container">
        <div className="navbar-brand">
          <a className="navbar-item" href="https://bulma.io">
            <img src="/title.png" width="140" height="28" />
          </a>

          <a
            role="button"
            className="navbar-burger burger"
            aria-label="menu"
            aria-expanded="false"
            data-target="navbarBasicExample"
          >
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </a>
        </div>

        <div id="navbarBasicExample" className="navbar-menu">
          <div className="navbar-start">
            <a className="navbar-item">Home</a>

            <a className="navbar-item">Documentation</a>
          </div>
        </div>
      </div>
    </nav>
  );
};

const App: React.SFC<{}> = () => (
  <React.Fragment>
    <Header />
    <section className="section">
      <div className="container">
        <ConnectedProjectsListContainer />
      </div>
    </section>
  </React.Fragment>
);

export default App;
