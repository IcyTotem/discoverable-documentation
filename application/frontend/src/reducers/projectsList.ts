import {
  AnyProjectsListAction,
  LOAD_PROJECTS_LIST,
  LoadProjectsListAction,
  CLEAR_PROJECTS_LIST,
} from '../actions/projects';
import { ProjectsListState } from '../state/projects';
import { ActionStatus } from '../actions/base';
import { createReducerFromMap, INITIAL_STATE } from './base';

export const reducer = createReducerFromMap({
  [INITIAL_STATE]: (): ProjectsListState => ({}),
  [CLEAR_PROJECTS_LIST]: (): ProjectsListState => ({ items: [] }),

  [LOAD_PROJECTS_LIST]: (state: ProjectsListState, action: AnyProjectsListAction): ProjectsListState => {
    const typedAction = action as LoadProjectsListAction;

    if (typedAction.status === ActionStatus.FAILED) {
      return { error: typedAction.exception.failureReason };
    }

    if (typedAction.status === ActionStatus.COMPLETED) {
      return { items: typedAction.payload.projects };
    }

    return {};
  },
});
