import { combineReducers } from 'redux';
import { ProjectsListState } from '../state/projects';
import { reducer as projectsListReducer } from './projectsList';

export interface State {
  projectsList: ProjectsListState;
}

export const reducer = combineReducers({
  projectsList: projectsListReducer,
});
