import { ThunkAction } from '../actions/base';

// Used in the map of createReducerFromMap as a safe fallback
export const INITIAL_STATE = '__init__';

export type Reducer<S, A extends ThunkAction> = (state: S, action: A) => S;

export function createReducerFromMap<S, A extends ThunkAction>(actionHandlersMap: {
  [key: string]: Reducer<S, A>;
}): Reducer<S, A> {
  return (state: S, action: A): S => {
    const targetReducer = actionHandlersMap[action.type] || actionHandlersMap[INITIAL_STATE];
    if (targetReducer) {
      return targetReducer(state, action);
    } else {
      console.error('There was no reducer for action type', action.type);
      return state;
    }
  };
}
