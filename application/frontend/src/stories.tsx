import React from 'react';

export const preview = (element: React.ReactElement) => (
  <section className="section">
    <div className="container">{element}</div>
  </section>
);
