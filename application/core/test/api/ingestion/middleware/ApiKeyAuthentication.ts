import * as faker from 'faker';
import { Request, Response } from 'express';
import { mock, when, instance, verify, anything } from 'ts-mockito';
import { describe, it, beforeEach } from 'mocha';
import { assert } from 'chai';
import { ServiceContainer } from '@core/services/Container';
import { ProjectService } from '@core/services/ProjectService';
import { ApiKeyAuthentication } from '@core/api/ingestion/middleware/ApiKeyAuthentication';
import { Project } from '@core/entities/Project';

describe('ApiKeyAuthentication', () => {
    let MockServiceContainer: ServiceContainer;
    let MockProjectService: ProjectService;
    let MockRequest: Request;
    let MockResponse: Response;

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let nextCallArgument: any;
    let apiKeyAuthentication: ApiKeyAuthentication;
    let randomHeaderName: string;
    let nextInvoked: boolean;
    let req: Request;
    let res: Response;

    async function invokeMiddleware(apiKeyHeaderName: string): Promise<Response> {
        const middlewareFunction = apiKeyAuthentication.middleware(apiKeyHeaderName);
        nextInvoked = false;

        // NextFunction is mockable but its invocation is not verifiable, at least
        // not with the current ts-mockito capabilities
        return middlewareFunction(req, res, e => {
            nextCallArgument = e;
            nextInvoked = true;
        });
    }

    beforeEach(() => {
        MockServiceContainer = mock(ServiceContainer);
        MockProjectService = mock(ProjectService);

        // Yay, support for interfaces in mockito!
        MockRequest = mock<Request>();
        MockResponse = mock<Response>();

        const container = instance(MockServiceContainer);
        const projectService = instance(MockProjectService);
        req = instance(MockRequest);
        res = instance(MockResponse);

        apiKeyAuthentication = new ApiKeyAuthentication(container);
        randomHeaderName = faker.random.word();

        when(MockServiceContainer.projectService()).thenResolve(projectService);
    });

    it('should return Forbidden when no api key is specified', async () => {
        await invokeMiddleware(randomHeaderName);

        verify(MockResponse.sendStatus(403)).once();
        verify(MockProjectService.findByApiKey(anything())).never();

        assert.isFalse(nextInvoked);
        assert.isUndefined(nextCallArgument);
    });

    it('should return Bad Request when multiple api keys are specified', async () => {
        // Return two different values for the same header
        when(MockRequest.headers).thenReturn({
            [randomHeaderName]: [faker.random.uuid(), faker.random.uuid()]
        });

        await invokeMiddleware(randomHeaderName);

        verify(MockResponse.sendStatus(400)).once();
        verify(MockProjectService.findByApiKey(anything())).never();

        assert.isFalse(nextInvoked);
        assert.isUndefined(nextCallArgument);
    });

    it('should pass an error to next when services throw an exception', async () => {
        const notFoundError = new Error('Project not found');
        const apiKey = faker.random.uuid();

        when(MockProjectService.findByApiKey(apiKey)).thenReject(notFoundError);
        when(MockRequest.headers).thenReturn({ [randomHeaderName]: apiKey });

        await invokeMiddleware(randomHeaderName);

        verify(MockResponse.sendStatus(anything())).never();

        assert.isTrue(nextInvoked);
        assert.equal(nextCallArgument, notFoundError);
    });

    it('should populate response.project and call next on success', async () => {
        const project = new Project();
        const apiKey = faker.random.uuid();

        when(MockProjectService.findByApiKey(apiKey)).thenResolve(project);
        when(MockRequest.headers).thenReturn({ [randomHeaderName]: apiKey });

        await invokeMiddleware(randomHeaderName);

        verify(MockResponse.sendStatus(anything())).never();

        assert.isTrue(nextInvoked);
        assert.isUndefined(nextCallArgument);
        assert.equal(req.project, project);
    });
});
