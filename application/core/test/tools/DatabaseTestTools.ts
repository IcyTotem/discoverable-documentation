import * as faker from 'faker';
import { Database } from '@core/services/Database';
import { User } from '@core/entities/User';
import { Tenant } from '@core/entities/Tenant';
import { Project } from '@core/entities/Project';
import { DocumentationRecord } from '@core/entities/DocumentationRecord';
import { FullTextSearchRecord } from '@core/entities/FullTextSearchRecord';
import { Commit } from '@core/entities/Commit';
import { Developer } from '@core/entities/Developer';

const entities = [Developer, FullTextSearchRecord, DocumentationRecord, Commit, Project, User, Tenant];

interface TenantUserProject {
    tenant: Tenant;
    user: User;
    project: Project;
}

export class DatabaseTestTools {
    /**
     * Delete all data from the database. Useful to clean up stuff after a test.
     */
    public static async clear(): Promise<void> {
        const connection = await Database.connection();
        const repositories = entities.map(entity => connection.getRepository(entity));

        for (const repo of repositories) {
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            const entities: any[] = await repo.find();
            await repo.remove(entities);
        }
    }

    public static async createFakeTenantUserProject(): Promise<TenantUserProject> {
        const connection = await Database.connection();
        const tenantRepository = connection.getRepository(Tenant);
        const userRepository = connection.getRepository(User);
        const projectRepository = connection.getRepository(Project);

        let tenant = new Tenant();
        tenant.name = faker.random.word();
        tenant = await tenantRepository.save(tenant);

        let user = new User();
        user.name = `${faker.name.firstName()} ${faker.name.lastName()}`;
        user.email = faker.internet.email();
        user.tenantId = tenant.id;
        user = await userRepository.save(user);

        let project = new Project();
        project.name = faker.random.word();
        project.description = faker.random.word();
        project.ownerId = user.id;
        project.tenantId = tenant.id;
        project.apiKey = faker.random.uuid();
        project.creationTime = new Date();
        project = await projectRepository.save(project);

        return { tenant, user, project };
    }
}
