import * as zlib from 'zlib';
import * as faker from 'faker';
import { DocumentationRecord } from '@core/entities/DocumentationRecord';
import { Project } from '@core/entities/Project';
import { Developer } from '@core/entities/Developer';
import { Commit } from '@core/entities/Commit';

export class RandomEntities {
    public static get project(): Project {
        return Object.assign(new Project(), {
            id: faker.random.number({ min: 0 }),
            tenantId: faker.random.number({ min: 0 }),
            ownerId: faker.random.number({ min: 0 }),
            name: faker.random.word(),
            areEmailNotificationsEnabled: Math.random() < 0.5
        });
    }

    public static get developer(): Developer {
        return Object.assign(new Developer(), {
            id: faker.random.number({ min: 0 }),
            projectId: faker.random.number({ min: 0 }),
            name: faker.random.word(),
            email: faker.internet.email()
        });
    }

    public static get document(): DocumentationRecord {
        return Object.assign(new DocumentationRecord(), {
            id: faker.random.number({ min: 0 }),
            projectId: faker.random.number({ min: 0 }),
            commitTime: faker.date.past(),
            commitHash: faker.random.uuid(),
            repositoryPath: faker.internet.url(),
            isLatest: Math.random() < 0.5,
            compressedContent: zlib.deflateSync(Buffer.from(faker.lorem.paragraph(), 'utf-8'))
        });
    }

    public static get commit(): Commit {
        return Object.assign(new Commit(), {
            id: faker.random.number({ min: 0 }),
            projectId: faker.random.number({ min: 0 }),
            branchName: faker.random.word(),
            authorName: faker.name.lastName(),
            authorEmail: faker.internet.email(),
            hash: faker.random.uuid(),
            creationTime: faker.date.past(),
            compressedDiffFromMaster: zlib.deflateSync(Buffer.from(faker.lorem.paragraph(), 'utf-8')),
            containsDocumentation: Math.random() < 0.5,
            triggersReminder: Math.random() < 0.5
        });
    }

    public static documents(count: number, override?: Partial<DocumentationRecord>): DocumentationRecord[] {
        return Array.from({ length: count }).map(() => {
            const result = this.document;
            override && Object.assign(result, override);
            return result;
        });
    }

    public static developers(count: number, override?: Partial<Developer>): Developer[] {
        return Array.from({ length: count }).map(() => {
            const result = this.developer;
            override && Object.assign(result, override);
            return result;
        });
    }

    public static commits(count: number, override?: Partial<Commit>): Commit[] {
        return Array.from({ length: count }).map(() => {
            const result = this.commit;
            override && Object.assign(result, override);
            return result;
        });
    }
}
