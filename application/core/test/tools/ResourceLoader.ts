import * as fs from 'fs';
import * as path from 'path';

export class ResourceLoader {
    public static asString(resourceName: string): string {
        return fs.readFileSync(ResourceLoader.absolutePathOf(resourceName), { encoding: 'utf8' });
    }

    public static absolutePathOf(resourceName: string): string {
        return path.resolve(__dirname, `../resources/${resourceName}`);
    }
}
