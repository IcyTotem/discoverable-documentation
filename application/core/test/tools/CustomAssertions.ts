import { assert } from 'chai';

class CustomAssertions {
    sorted<T>(items: T[], comparer: (item0: T, item1: T) => number): void {
        if (items.length > 1) {
            for (let i = 1; i < items.length; ++i) {
                const item0 = items[i - 1];
                const item1 = items[i];
                assert.isTrue(
                    comparer(item0, item1) <= 0,
                    `Expected item at position ${i - 1} (${item0}) to preceed its successor's value (${item1})`
                );
            }
        }
    }
}

class AsyncAssertions {
    async promiseRejected<T>(promise: Promise<T>, expectedMessage?: string): Promise<void> {
        try {
            await promise;
            assert.fail('No exception', 'Database exception');
        } catch (e) {
            assert.isOk(e);

            if (expectedMessage) {
                assert.equal(e.message || e.toString(), expectedMessage);
            }
        }
    }
}

export const customAssert = new CustomAssertions();
export const asyncAssert = new AsyncAssertions();
