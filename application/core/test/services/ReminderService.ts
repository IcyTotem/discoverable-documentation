import * as chai from 'chai';
import * as faker from 'faker';
import chaiExclude from 'chai-exclude';
import { describe, it, beforeEach, afterEach } from 'mocha';
import { Repository } from 'typeorm';
import { ReminderService } from '@core/services/ReminderService';
import { DatabaseTestTools } from '@core-tests/tools/DatabaseTestTools';
import { RandomEntities } from '@core-tests/tools/RandomEntities';
import { Database } from '@core/services/Database';
import { Commit } from '@core/entities/Commit';

const assert = chai.assert;
chai.use(chaiExclude);

describe('ReminderService', () => {
    let reminderService: ReminderService;
    let commitRepository: Repository<Commit>;
    let existingProjectId = 0;

    beforeEach(async () => {
        const { project } = await DatabaseTestTools.createFakeTenantUserProject();
        existingProjectId = project.id;

        commitRepository = await Database.repository(Commit);
        reminderService = new ReminderService(commitRepository);

        // Make sure all commits have valid project id
        const allCommits = RandomEntities.commits(faker.random.number({ min: 5, max: 50 }), {
            projectId: existingProjectId
        });

        // Make sure at least some commits match the criteria for the following tests
        allCommits[0].triggersReminder = true;
        allCommits[0].containsDocumentation = false;
        allCommits[1].triggersReminder = false;
        allCommits[1].containsDocumentation = false;
        allCommits[2].triggersReminder = false;
        allCommits[2].containsDocumentation = true;

        await commitRepository.save(allCommits);
    });

    afterEach(async () => DatabaseTestTools.clear());

    describe('#remindMissingDocumentation', () => {
        it('should mark a single commit as reminder for missing documentation', async () => {
            const unmarkedCommit = await commitRepository.findOne({
                where: {
                    projectId: existingProjectId,
                    containsDocumentation: false,
                    triggersReminder: false
                }
            });

            assert.isOk(unmarkedCommit);
            await reminderService.remindMissingDocumentation(existingProjectId, unmarkedCommit.hash);

            const sameCommit = await commitRepository.findOne({
                where: {
                    projectId: existingProjectId,
                    hash: unmarkedCommit.hash
                }
            });

            assert.isOk(sameCommit);
            assert.deepEqualExcluding(sameCommit, unmarkedCommit, ['triggersReminder']);
            assert.isTrue(sameCommit.triggersReminder);
        });
    });

    describe('#dismiss', () => {
        it('should dismiss a single commit that was marked as reminder', async () => {
            const markedCommit = await commitRepository.findOne({
                where: {
                    projectId: existingProjectId,
                    containsDocumentation: false,
                    triggersReminder: true
                }
            });

            assert.isOk(markedCommit);
            await reminderService.dismiss(existingProjectId, markedCommit.hash);

            const sameCommit = await commitRepository.findOne({
                where: {
                    projectId: existingProjectId,
                    hash: markedCommit.hash
                }
            });

            assert.isOk(sameCommit);
            assert.deepEqualExcluding(sameCommit, markedCommit, ['triggersReminder']);
            assert.isFalse(sameCommit.triggersReminder);
        });
    });

    describe('#dismissAll', () => {
        it('should dismiss all commits marked as reminders in the same project', async () => {
            const markedCommits = await commitRepository.find({
                where: {
                    projectId: existingProjectId,
                    containsDocumentation: false,
                    triggersReminder: true
                }
            });

            assert.isNotEmpty(markedCommits);
            await reminderService.dismissAll(existingProjectId);

            const allCommitsFromProject = await commitRepository.find({ where: { projectId: existingProjectId } });
            assert.isNotEmpty(allCommitsFromProject);
            assert.isTrue(allCommitsFromProject.every(c => !c.triggersReminder));

            // Make sure the commits that were previously marked are still there
            assert.includeDeepMembers(allCommitsFromProject.map(c => c.hash), markedCommits.map(c => c.hash));

            const reminderCommits = await reminderService.getActiveReminderCommits(existingProjectId);
            assert.isEmpty(reminderCommits);
        });
    });

    describe('#getActiveReminderCommits', () => {
        it('should only retrieve commits marked as reminders', async () => {
            const markedCommits = await commitRepository.find({
                where: {
                    projectId: existingProjectId,
                    containsDocumentation: false,
                    triggersReminder: true
                }
            });

            const retrievedCommits = await reminderService.getActiveReminderCommits(existingProjectId);
            assert.deepEqual(
                markedCommits
                    .map(c => ({
                        hash: c.hash,
                        authorName: c.authorName,
                        authorEmail: c.authorEmail,
                        branchName: c.branchName,
                        creationTime: c.creationTime
                    }))
                    .sort(),
                retrievedCommits.sort()
            );
        });

        it('should return an empty list for a project that does not exist', async () => {
            const retrievedCommits = await reminderService.getActiveReminderCommits(-1);
            assert.isEmpty(retrievedCommits);
        });
    });

    describe('#acknowledgeMergedBranches', () => {
        it('should mark reminder commits whose branch matches the given list as dismissed', async () => {
            const markedCommits = await commitRepository.find({
                where: {
                    projectId: existingProjectId,
                    containsDocumentation: false,
                    triggersReminder: true
                }
            });

            assert.isNotEmpty(markedCommits);

            const branchNames = markedCommits.map(c => c.branchName);
            await reminderService.acknowledgeMergedBranches(existingProjectId, branchNames);

            const allCommits = await commitRepository.find({ where: { projectId: existingProjectId } });
            const commitsMatchingBranches = allCommits.filter(c => branchNames.includes(c.branchName));
            assert.isTrue(commitsMatchingBranches.every(c => !c.triggersReminder));
        });
    });
});
