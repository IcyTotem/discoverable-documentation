import * as os from 'os';
import * as path from 'path';
import { assert } from 'chai';
import * as faker from 'faker';
import { describe, it, before } from 'mocha';
import { FileService } from '@core/services/FileService';
import { asyncAssert } from '@core-tests/tools/CustomAssertions';
import { ResourceLoader } from '@core-tests/tools/ResourceLoader';

describe('FileService', () => {
    let tempDirectory: string;
    let fileService: FileService;

    async function createEmptyFile(parentDirectory: string): Promise<string> {
        const filePath = path.join(parentDirectory, faker.random.uuid());
        await fileService.fsp.writeFile(filePath, '', { encoding: 'utf-8' });
        return filePath;
    }

    before(() => {
        tempDirectory = os.tmpdir();
        fileService = new FileService();
    });

    describe('#createDirectoryForExtraction', () => {
        it('should create a new directory at the same level of the archive', async () => {
            const fakeArchive = await createEmptyFile(tempDirectory);
            const newDirectory = await fileService.createDirectoryForExtraction(fakeArchive);
            const newDirectoryStat = await fileService.fsp.stat(newDirectory);

            assert.isTrue(newDirectoryStat.isDirectory());

            const fakeArchiveName = path.basename(fakeArchive);
            const newDirectoryName = path.basename(newDirectory);

            assert.equal(newDirectoryName, `${fakeArchiveName}-extracted`);
        });

        it('should throw an exception if the archive path is empty', async () => {
            asyncAssert.promiseRejected(fileService.createDirectoryForExtraction(''), 'archivePath must not be empty');
        });
    });

    describe('#enumerateFilesRecursively', () => {
        it('should return a list of relative paths to the given directory', async () => {
            // Create some random directories first
            const containerDirectory = path.join(tempDirectory, faker.random.uuid());
            await fileService.fsp.mkdir(containerDirectory);
            await fileService.fsp.mkdir(path.join(containerDirectory, 'one', 'two'), { recursive: true });
            await fileService.fsp.mkdir(path.join(containerDirectory, 'edge'));
            await fileService.fsp.writeFile(path.join(containerDirectory, 'one', 'two', 'leaf.txt'), 'leaf data');
            await fileService.fsp.writeFile(path.join(containerDirectory, 'edge', 'burkel.md'), 'burkel data');

            // Resulting structure:
            // containerDirectory/one/two/leaf (file)
            // containerDirectory/one/two (dir)
            // containerDirectory/one (dir)
            // containerDirectory/edge/burkel (file)
            // containerDirectory/edge (dir)

            // Notice we use path.join everywhere to abstract the OS pathing structure
            const relativePaths = await fileService.enumerateFilesRecursively(containerDirectory);
            assert.deepEqual(relativePaths, [path.join('edge', 'burkel.md'), path.join('one', 'two', 'leaf.txt')]);
        });

        it('should use the current working directory when directory path is empty', async () => {
            const relativePaths = await fileService.enumerateFilesRecursively('');
            assert.isNotEmpty(relativePaths);
        });
    });

    describe('extractArchive', () => {
        it('should extract the given archive to the specified directory maintaining the internal tree structure', async () => {
            const containerDirectory = path.join(tempDirectory, faker.random.uuid());
            await fileService.fsp.mkdir(containerDirectory);

            await fileService.extractArchive(ResourceLoader.absolutePathOf('mock-archive.zip'), containerDirectory);

            // Use the same enumeration method of the service to check the resulting extracted content
            const relativePaths = await fileService.enumerateFilesRecursively(containerDirectory);
            assert.deepEqual(relativePaths, [
                'recipe.md',
                path.join('fruits', 'apple-baum.md'),
                path.join('fruits', 'banana.md')
            ]);

            const fileContents = await Promise.all(
                relativePaths
                    .map(relativePath => path.join(containerDirectory, relativePath))
                    .map(absolutePath => fileService.fsp.readFile(absolutePath, { encoding: 'utf-8' }))
            );

            assert.deepEqual(fileContents, [
                'Two spoonful of sugar and a pinch of fabulous',
                "It's not what you think it is",
                "It's yellow and full of potassium"
            ]);
        });

        it('should throw an exception if the archive path is empty', async () => {
            asyncAssert.promiseRejected(fileService.extractArchive('', 'existing'), 'archivePath must not be empty');
        });
    });
});
