import * as mockery from 'mockery';
import * as nodemailerMock from 'nodemailer-mock';
import * as faker from 'faker';

// This is quite awful, but mocks for node modules must be done here because the imports
// in the following lines will implicitly load them as dependencies
mockery.enable({ warnOnUnregistered: false });
mockery.registerMock('nodemailer', nodemailerMock);

import { assert } from 'chai';
import { describe, it, after, afterEach } from 'mocha';
import { EmailService } from '@core/services/EmailService';
import { HtmlDocument } from '@core/models/text/HtmlDocument';

describe('EmailService', () => {
    const service = new EmailService();
    const from = `"${EmailService.senderDisplayName}" <${EmailService.senderEmail}>`;

    afterEach(() => nodemailerMock.mock.reset());

    after(() => {
        mockery.deregisterAll();
        mockery.disable();
    });

    describe('#sendEmail', () => {
        it('should send an email to all recipients', async () => {
            const validEmails = Array.from({ length: 10 }).map(() => faker.internet.email());

            await service.sendEmail(validEmails, {
                subject: 'This is a test subject',
                body: 'This is a plaintext body'
            });

            const sentMail = nodemailerMock.mock.getSentMail();
            assert.deepEqual(sentMail, [
                {
                    from,
                    subject: 'This is a test subject',
                    text: 'This is a plaintext body',
                    html: undefined,
                    to: validEmails.join(', ')
                }
            ]);
        });

        it('should send both plaintext and html emails', async () => {
            const recipient = faker.internet.email();

            await service.sendEmail([recipient], {
                subject: 'This is a test subject',
                body: 'This is a plaintext body'
            });

            await service.sendEmail([recipient], {
                subject: 'This is a test subject',
                body: new HtmlDocument('This is an <b>html</b> body')
            });

            const sentMail = nodemailerMock.mock.getSentMail();
            assert.deepEqual(sentMail, [
                {
                    from,
                    subject: 'This is a test subject',
                    text: 'This is a plaintext body',
                    html: undefined,
                    to: recipient
                },
                {
                    from,
                    subject: 'This is a test subject',
                    text: undefined,
                    html: 'This is an <b>html</b> body',
                    to: recipient
                }
            ]);
        });

        it('should ignore all non-valid email addresses', async () => {
            const validEmails = Array.from({ length: 10 }).map(() => faker.internet.email());
            const invalidEmails = [
                faker.internet.password(),
                faker.random.uuid(),
                faker.lorem.paragraph(),
                faker.date.future().toString()
            ];

            await service.sendEmail(validEmails.concat(invalidEmails), {
                subject: 'This is a test subject',
                body: 'This is a plaintext body'
            });

            const sentMail = nodemailerMock.mock.getSentMail();
            assert.equal(sentMail[0].to, validEmails.join(', '));
        });
    });
});
