import * as faker from 'faker';
import { assert } from 'chai';
import { describe, it, beforeEach, afterEach } from 'mocha';
import { Repository } from 'typeorm';
import { DatabaseTestTools } from '@core-tests/tools/DatabaseTestTools';
import { RandomEntities } from '@core-tests/tools/RandomEntities';
import { Database } from '@core/services/Database';
import { Commit } from '@core/entities/Commit';
import { TimelineService } from '@core/services/TimelineService';
import { DocumentationRecord } from '@core/entities/DocumentationRecord';
import { customAssert } from '@core-tests/tools/CustomAssertions';

describe('TimelineService', () => {
    let timelineService: TimelineService;
    let commitRepository: Repository<Commit>;
    let documentRepository: Repository<DocumentationRecord>;
    let existingProjectId = 0;

    async function createTestCommits(): Promise<Commit[]> {
        const allCommits = RandomEntities.commits(faker.random.number({ min: 10, max: 15 }), {
            projectId: existingProjectId
        });
        await commitRepository.save(allCommits);
        return allCommits;
    }

    async function createTestDocumentsForCommits(commits: Commit[]): Promise<DocumentationRecord[]> {
        const allDocuments: DocumentationRecord[] = [];

        for (const commit of commits) {
            const documentsForCommit = RandomEntities.documents(faker.random.number({ min: 1, max: 3 }), {
                projectId: existingProjectId,
                commitHash: commit.hash
            });
            documentsForCommit.forEach(d => allDocuments.push(d));
        }

        await documentRepository.save(allDocuments);
        return allDocuments;
    }

    beforeEach(async () => {
        const { project } = await DatabaseTestTools.createFakeTenantUserProject();
        existingProjectId = project.id;

        commitRepository = await Database.repository(Commit);
        documentRepository = await Database.repository(DocumentationRecord);
        timelineService = new TimelineService(commitRepository, documentRepository);

        const allCommits = await createTestCommits();
        await createTestDocumentsForCommits(allCommits);
    });

    afterEach(async () => DatabaseTestTools.clear());

    describe('#fetchTimeline', () => {
        it('should load the n most recent commits and their associated documents', async () => {
            const entries = await timelineService.fetchTimeline(existingProjectId, 8);

            // We created at least 10 commits during initialization, hence we expect exactly 8 results
            assert.lengthOf(entries, 8);

            // All entries must be sorted in descending order of commit date
            customAssert.sorted(
                entries,
                (e0, e1) => e1.commit.creationTime.getTime() - e0.commit.creationTime.getTime()
            );

            // There should be no empty entry
            assert.isTrue(entries.every(e => e.documents.length > 0), 'All entries must have at least one document');

            // Check that all returned objects actually exist in the database
            for (const entry of entries) {
                const dbCommit = await commitRepository.findOne({
                    where: { projectId: existingProjectId, hash: entry.commit.hash }
                });
                assert.isOk(
                    dbCommit,
                    `Entry of commit ${entry.commit.hash} should refer to an existing commit in the database`
                );

                for (const document of entry.documents) {
                    const dbDocument = await documentRepository.findOne({
                        where: { projectId: existingProjectId, id: document.id }
                    });
                    assert.isOk(
                        dbDocument,
                        `Document ${document.id} of commit ${entry.commit.hash} should refer to an existing document in the database`
                    );
                }
            }
        });

        it('should return an empty array if commitCount <= 0', async () => {
            let entries = await timelineService.fetchTimeline(existingProjectId, 0);
            assert.lengthOf(entries, 0);

            entries = await timelineService.fetchTimeline(existingProjectId, -78);
            assert.lengthOf(entries, 0);
        });

        it('should return an empty array for projects with no commits', async () => {
            const entries = await timelineService.fetchTimeline(-1);
            assert.lengthOf(entries, 0);
        });
    });
});
