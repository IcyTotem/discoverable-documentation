import * as faker from 'faker';
import { assert } from 'chai';
import { Repository } from 'typeorm';
import { describe, it, beforeEach, afterEach } from 'mocha';
import { DatabaseTestTools } from '@core-tests/tools/DatabaseTestTools';
import { Database } from '@core/services/Database';
import { Commit } from '@core/entities/Commit';
import { CommitService } from '@core/services/CommitService';
import { RandomEntities } from '@core-tests/tools/RandomEntities';

describe('CommitService', () => {
    let commitRepository: Repository<Commit>;
    let commitService: CommitService;
    let existingProjectId: number;

    beforeEach(async () => {
        const { project } = await DatabaseTestTools.createFakeTenantUserProject();
        existingProjectId = project.id;

        commitRepository = await Database.repository(Commit);
        commitService = new CommitService(commitRepository);
    });

    afterEach(async () => DatabaseTestTools.clear());

    describe('#getLatestHash', () => {
        it('should return the latest commit hash for given project and branch', async () => {
            const oldCommitRightBranch = RandomEntities.commit;
            const newCommitRightBranch = RandomEntities.commit;
            const newCommitWrongBranch = RandomEntities.commit;

            // Force the properties to be consistent with the constants names
            oldCommitRightBranch.branchName = newCommitRightBranch.branchName;
            oldCommitRightBranch.creationTime = faker.date.past(1, newCommitRightBranch.creationTime);

            // Make sure the properties have been arrange correctly
            assert.equal(oldCommitRightBranch.branchName, newCommitRightBranch.branchName);
            assert.notEqual(newCommitWrongBranch.branchName, newCommitRightBranch.branchName);

            await commitService.create(existingProjectId, { ...oldCommitRightBranch, diffFromMaster: null });
            await commitService.create(existingProjectId, { ...newCommitRightBranch, diffFromMaster: null });
            await commitService.create(existingProjectId, { ...newCommitWrongBranch, diffFromMaster: null });

            const latestCommitHash = await commitService.getLatestHash(
                existingProjectId,
                newCommitRightBranch.branchName
            );
            assert.equal(latestCommitHash, newCommitRightBranch.hash);
        });

        it('should return null when there is no latest commit', async () => {
            const latestCommitHash = await commitService.getLatestHash(existingProjectId, '');
            assert.isNull(latestCommitHash);
        });
    });

    describe('#exists', () => {
        it('returns true for an existing commit', async () => {
            const meta = RandomEntities.commit;
            await commitService.create(existingProjectId, { ...meta, diffFromMaster: null });

            const result = await commitService.exists(existingProjectId, meta.hash);
            assert.isTrue(result);
        });

        it('returns false for a commit that does not exist', async () => {
            assert.isFalse(await commitService.exists(existingProjectId, 'asd'));
        });

        it('returns false if hash is empty', async () => {
            assert.isFalse(await commitService.exists(existingProjectId, ''));
        });
    });
});
