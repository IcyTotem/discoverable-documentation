import { assert } from 'chai';
import { describe, it, before } from 'mocha';
import { HtmlDocument } from '@core/models/text/HtmlDocument';
import { ResourceLoader } from '@core-tests/tools/ResourceLoader';
import { HypertextSplitter } from '@core/services/HypertextSplitter';

const shrink = (text: string): string => text.replace(/\s+/gi, ' ').trim();

describe('HypertextSplitter', () => {
    describe('#searchRecords', () => {
        const archiveId = Math.floor(Math.random() * 100);
        const projectId = Math.floor(Math.random() * 100);
        let records = [];

        before(() => {
            const htmlDocument = new HtmlDocument(ResourceLoader.asString('mock-html.html'));
            const analyzer = new HypertextSplitter(htmlDocument);
            records = Array.from(analyzer.searchRecords(projectId, archiveId));
        });

        it('have the correct archive and project ids', () => {
            for (const record of records) {
                assert.equal(record.projectId, projectId);
                assert.equal(record.archiveId, archiveId);
            }
        });

        it('do not contain empty elements', () => {
            const emptyRecords = records.filter(record => record.textContent.match(/^\s*$/));
            assert.isEmpty(emptyRecords);
        });

        it('do not contain other elements except relevant ones', () => {
            assert.isEmpty(records.filter(record => !HypertextSplitter.relevantElements.includes(record.element)));
        });

        it('contain proper paragraphs', () => {
            const pContents = records.filter(record => record.element === 'p').map(p => shrink(p.textContent));

            assert.deepEqual(pContents, [
                'dolor sit amet, consectetur adipiscing elit. Donec vitae elit vitae nisl pellentesque facilisis. ' +
                    'Praesent viverra hendrerit mollis. Nam tincidunt volutpat vestibulum. Morbi et leo ipsum. ' +
                    'In non aliquam libero. Maecenas at dolor placerat, placerat sem posuere, vestibulum est.',

                'elit faucibus, scelerisque orci sollicitudin, fermentum lorem. ' +
                    'Nam consequat tellus eget auctor pharetra. Phasellus ac ullamcorper lacus.',

                'Morbi id pretium dolor, eu porttitor augue. ' +
                    'Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. ' +
                    'Integer iaculis porta justo, in sagittis augue euismod id. ' +
                    'Cras eget volutpat diam, quis dictum massa.'
            ]);
        });

        it('contain proper list items', () => {
            const liContents = records.filter(record => record.element === 'li').map(li => shrink(li.textContent));

            assert.deepEqual(liContents, [
                'id lobortis risus. Fusce blandit efficitur commodo',
                'Quisque convallis odio ac accumsan iaculis',
                'Quisque vitae purus felis',
                'Praesent blandit magna sit amet molestie fringilla'
            ]);
        });

        it('contain proper headers', () => {
            const headerSummary = records
                .filter(record => record.element.match(/^h\d$/))
                .map(hn => ({ name: hn.element, content: shrink(hn.textContent) }));

            assert.deepEqual(headerSummary, [
                { name: 'h1', content: 'Lorem ipsum' },
                { name: 'h2', content: 'Mauris ut sagittis libero' },
                { name: 'h4', content: 'Maecenas sit amet' },
                { name: 'h5', content: 'vitae condimentum leo efficitur' }
            ]);
        });

        it('contain proper code', () => {
            const code = records.filter(record => record.element === 'code')[0];
            assert.equal(
                shrink(code.textContent),
                'const nulla = venenatisElit(ac, auctorDictum.fusce.sit && amet); ' +
                    'sem(libero => { phasellus.tristique(massa); ac += tellusVestibulum; });'
            );
        });
    });
});
