import { assert } from 'chai';
import { describe, it, beforeEach, afterEach } from 'mocha';
import { Database } from '@core/services/Database';
import { DocumentationUploadService } from '@core/services/DocumentationUploadService';
import { SearchService, SearchResultEntry, SearchResultHeadline } from '@core/services/SearchService';
import { DocumentationRecord } from '@core/entities/DocumentationRecord';
import { MarkdownDocument } from '@core/models/text/MarkdownDocument';
import { DatabaseTestTools } from '@core-tests/tools/DatabaseTestTools';
import { ResourceLoader } from '@core-tests/tools/ResourceLoader';
import { HtmlDocument } from '@core/models/text/HtmlDocument';
import { Commit } from '@core/entities/Commit';
import { RandomEntities } from '@core-tests/tools/RandomEntities';
import { FileService } from '@core/services/FileService';
import { CommitService } from '@core/services/CommitService';

describe('SearchService', () => {
    describe('#search', async () => {
        let documentationUploadService: DocumentationUploadService;
        let searchService: SearchService;
        let existingProjectId = 0;
        let existingCommitHash = '';

        async function quickUpload(repositoryPath: string, markdownText: string): Promise<DocumentationRecord> {
            const meta = {
                commitHash: existingCommitHash,
                repositoryPath
            };
            return documentationUploadService.upload(existingProjectId, new MarkdownDocument(markdownText), meta);
        }

        function headline(element: string, html: string): SearchResultHeadline {
            return { element, html: new HtmlDocument(html) };
        }

        function assertResultMatch(
            actualResult: SearchResultEntry,
            expectedDocument: DocumentationRecord,
            headlines: SearchResultHeadline | SearchResultHeadline[]
        ): void {
            assert.deepEqual(actualResult.document, expectedDocument);

            if (headlines instanceof Array) {
                assert.deepEqual(actualResult.headlines, headlines);
            } else {
                assert.deepEqual(actualResult.headlines, [headlines]);
            }
        }

        beforeEach(async () => {
            const { project } = await DatabaseTestTools.createFakeTenantUserProject();
            existingProjectId = project.id;

            const commitRepository = await Database.repository(Commit);
            const commit = RandomEntities.commit;
            commit.projectId = existingProjectId;
            existingCommitHash = commit.hash;
            await commitRepository.save(commit);

            documentationUploadService = new DocumentationUploadService(
                await Database.entityManager(),
                new FileService(),
                new CommitService(commitRepository)
            );
            searchService = new SearchService();
        });

        afterEach(async () => DatabaseTestTools.clear());

        it('finds correct very specific result', async () => {
            const text = ResourceLoader.asString('searchable-markdown.md');
            const document = await quickUpload('docs/tech/illus.md', text);
            const results = await searchService.search(existingProjectId, 'a process to define and create', {
                onlyIncludeLatest: false,
                resultsPerPage: 100,
                pageIndex: 0
            });

            assert.lengthOf(results, 1);
            assertResultMatch(
                results[0],
                document,
                headline(
                    'li',
                    '<b>process</b> of <b>defining</b>, <b>creating</b> and delivering information products for the safe, efficient and effective use of products'
                )
            );
        });

        it('finds all occurrences of the query in a single document', async () => {
            const text = ResourceLoader.asString('searchable-markdown.md');
            const document = await quickUpload('docs/tech/marcovoglio.md', text);
            const results = await searchService.search(existingProjectId, 'communicate', {
                onlyIncludeLatest: false,
                resultsPerPage: 100,
                pageIndex: 0
            });

            assert.lengthOf(results, 1);
            assertResultMatch(results[0], document, [
                headline('h1', 'Technical <b>communication</b>'),
                headline('h2', 'Technical <b>communicators</b>'),
                headline(
                    'li',
                    '<b>communication</b>. Some individuals are designated as technical <b>communicators</b> or technical writers. These individuals use a set of methods'
                ),
                headline(
                    'li',
                    'Scientific and Technical <b>Communicators</b> defines technical <b>communication</b> as factual <b>communication</b>, usually about products and services.'
                ),
                headline(
                    'p',
                    'Association for Technical <b>Communication</b> published a competence framework for the professional field of technical <b>communication</b>.'
                ),
                headline(
                    'p',
                    '<b>communicators</b> generally tailor information to a specific audience, which may be subject matter experts, consumers'
                ),
                headline(
                    'li',
                    '<b>Communication</b> briefly defines technical <b>communication</b> as "the process of defining, creating and delivering information products'
                ),
                headline('p', 'Technical <b>communication</b> jobs include the following:'),
                headline(
                    'p',
                    'Technical <b>communication</b> is a means to convey scientific, engineering, or other technical information.'
                ),
                headline(
                    'p',
                    'Other jobs available to technical <b>communicators</b> include digital strategist, marketing specialist, and content manager.'
                ),
                headline(
                    'p',
                    '<b>communication</b>, the overarching goal of the practice is to create easily accessible information for a specific'
                )
            ]);
        });

        it('finds occurrences of the query across many documents with paged results', async () => {
            const documents = await Promise.all([
                quickUpload('docs/tired.md', 'I am so very tired, I could sleep for three days'),
                quickUpload(
                    'readme.md',
                    '## How to change a tire\n - Step 1: cry\n - Step 2: call a mechanic\n - Step 3: sleep'
                ),
                quickUpload(
                    'docs/other/misc/fluff.md',
                    'He kept sleeping for a long time\n\n# Awakening\nThen he decided to call someone'
                ),
                quickUpload(
                    'docs/mirth.md',
                    'There is no doubt pancakes are the best call you can make for a nice sleeping morning'
                )
            ]);

            const firstPageResults = await searchService.search(existingProjectId, 'sleep', {
                onlyIncludeLatest: true,
                pageIndex: 0,
                resultsPerPage: 3
            });

            assert.lengthOf(firstPageResults, 3);
            assert.isAtLeast(firstPageResults[0].relevance, firstPageResults[1].relevance);
            assert.isAtLeast(firstPageResults[1].relevance, firstPageResults[2].relevance);

            assertResultMatch(firstPageResults[0], documents[1], headline('li', 'Step 3: <b>sleep</b>'));
            assertResultMatch(
                firstPageResults[1],
                documents[2],
                headline('p', 'He kept <b>sleeping</b> for a long time')
            );
            assertResultMatch(
                firstPageResults[2],
                documents[0],
                headline('p', 'I am so very tired, I could <b>sleep</b> for three days')
            );

            const secondPageResults = await searchService.search(existingProjectId, 'sleep', {
                onlyIncludeLatest: true,
                pageIndex: 1,
                resultsPerPage: 3
            });

            assert.lengthOf(secondPageResults, 1);
            assert.isAtLeast(firstPageResults[2].relevance, secondPageResults[0].relevance);
            assertResultMatch(
                secondPageResults[0],
                documents[3],
                headline(
                    'p',
                    'There is no doubt pancakes are the best call you can make for a nice <b>sleeping</b> morning'
                )
            );
        });

        it('correctly discriminates between latest and historic results', async () => {
            const doc0 = await quickUpload('docs/tired.md', 'I am so very tired, I could sleep for three days');
            const doc1 = await quickUpload(
                'docs/tired.md',
                '## The problems of being tired\nI am so very tired, I could sleep for three days'
            );
            const doc2 = await quickUpload(
                'docs/tired.md',
                '## The problems of being tired\nI was tired, then I realized I did not need to be, so I just stopped'
            );

            const latestSearchResults = await searchService.search(existingProjectId, 'tired', {
                onlyIncludeLatest: true,
                pageIndex: 0,
                resultsPerPage: 10
            });

            assert.lengthOf(latestSearchResults, 1);
            assertResultMatch(latestSearchResults[0], doc2, [
                headline('h2', 'The problems of being <b>tired</b>'),
                headline('p', 'I was <b>tired</b>, then I realized I did not need to be, so I just stopped')
            ]);

            const historicSearchResults = await searchService.search(existingProjectId, 'tired', {
                onlyIncludeLatest: false,
                pageIndex: 0,
                resultsPerPage: 10
            });

            assert.lengthOf(historicSearchResults, 3);

            // When objects are returned from quickUpload they all have isLatest = true, but retroactively
            // it becomes incorrect, so we have to update the fields manually (or reload them from db)
            doc1.isLatest = false;
            doc0.isLatest = false;
            assert.deepEqual(historicSearchResults.map(r => r.document), [doc2, doc1, doc0]);
        });

        it('returns empty results when the query is empty', async () => {
            assert.isEmpty(
                await searchService.search(existingProjectId, '   ', {
                    onlyIncludeLatest: false,
                    resultsPerPage: 100,
                    pageIndex: 0
                })
            );
        });

        it('returns empty results when the number of results per page is zero or less', async () => {
            await quickUpload('docs/legit.md', 'I am a legit document');
            assert.isEmpty(
                await searchService.search(existingProjectId, 'legit', {
                    onlyIncludeLatest: false,
                    resultsPerPage: 0,
                    pageIndex: 0
                })
            );
        });

        it('returns empty results when the project does not exist', async () => {
            await quickUpload('docs/phantom.md', 'I am a legit document');
            assert.isEmpty(
                await searchService.search(-1, 'legit', {
                    onlyIncludeLatest: false,
                    resultsPerPage: 10,
                    pageIndex: 0
                })
            );
        });
    });
});
