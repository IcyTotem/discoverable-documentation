import { assert } from 'chai';
import { describe, it, beforeEach, afterEach } from 'mocha';
import { mock, verify, when, anything, capture, instance } from 'ts-mockito';
import { Repository } from 'typeorm';
import { ChangedDocumentationSummary, NotificationService } from '@core/services/NotificationService';
import { Project } from '@core/entities/Project';
import { RandomEntities } from '@core-tests/tools/RandomEntities';
import { Developer } from '@core/entities/Developer';
import { EmailService } from '@core/services/EmailService';
import { Commit } from '@core/entities/Commit';
import { DocumentationRecord } from '@core/entities/DocumentationRecord';
import { DatabaseTestTools } from '@core-tests/tools/DatabaseTestTools';
import { Database } from '@core/services/Database';

describe('ChangedDocumentationSummary', () => {
    it('should create html for documentation', () => {
        const project = RandomEntities.project;
        const doc0 = RandomEntities.document;
        const doc1 = RandomEntities.document;

        const summary = new ChangedDocumentationSummary(project, new Date('2015-10-10T23:00:45'), [doc0, doc1]);
        const html = summary.getHtml().content;
        assert.include(html, `You are receiving this e-mail because you are a developer of ${project.name}.`);
        assert.include(
            html,
            'The following pieces of documentation have changed on Saturday October 10th 2015, 23:00 (UTC):'
        );
        assert.include(html, `<li><pre>${doc0.repositoryPath}</pre></li>`);
        assert.include(html, `<li><pre>${doc1.repositoryPath}</pre></li>`);
        assert.match(html, /^<html>[\w\W]+<\/html>$/gi);
    });
});

describe('NotificationService', () => {
    describe('#notifyDocumentationChanged', () => {
        let project: Project;
        let commit: Commit;
        let projectRepository: Repository<Project>;
        let MockedEmailService: EmailService;
        let developerRepository: Repository<Developer>;
        let commitRepository: Repository<Commit>;
        let documentRepository: Repository<DocumentationRecord>;
        let service: NotificationService;

        beforeEach(async () => {
            const { project: existingProject } = await DatabaseTestTools.createFakeTenantUserProject();

            projectRepository = await Database.repository(Project);
            project = existingProject;
            project.areEmailNotificationsEnabled = true;
            await projectRepository.save(project);

            commitRepository = await Database.repository(Commit);
            commit = RandomEntities.commit;
            commit.projectId = project.id;
            commit.containsDocumentation = true;
            await commitRepository.save(commit);

            MockedEmailService = mock(EmailService);
            when(MockedEmailService.sendEmail(anything(), anything())).thenResolve();

            developerRepository = await Database.repository(Developer);
            documentRepository = await Database.repository(DocumentationRecord);

            service = new NotificationService(
                instance(MockedEmailService),
                projectRepository,
                developerRepository,
                commitRepository,
                documentRepository
            );
        });

        afterEach(async () => DatabaseTestTools.clear());

        it('sends an email to all project developers if email notifications are enabled for the project', async () => {
            assert.isTrue(project.areEmailNotificationsEnabled);

            const developers = RandomEntities.developers(21, { projectId: project.id });
            await developerRepository.save(developers);

            const documents = RandomEntities.documents(6, { projectId: project.id, commitHash: commit.hash });
            await documentRepository.save(documents);

            await service.notifyDocumentationChanged(project.id, commit.hash);

            verify(MockedEmailService.sendEmail(anything(), anything())).called();

            const [actualEmails, actualEmailOptions] = capture(MockedEmailService.sendEmail).last();
            const summary = new ChangedDocumentationSummary(project, commit.creationTime, documents);

            // We have to capture the argument and then compare them rather than verifying the call
            // of the method direcly using deepEqual because the email arrays may be in different order
            assert.deepEqual(actualEmails.sort(), developers.map(d => d.email).sort());
            assert.deepEqual(actualEmailOptions, {
                subject: `New documentation as added to ${project.name}`,
                body: summary.getHtml()
            });
        });

        it('does not send any email if email notifications are disabled for the project', async () => {
            project.areEmailNotificationsEnabled = false;
            await projectRepository.save(project);

            const developers = RandomEntities.developers(21, { projectId: project.id });
            await developerRepository.save(developers);

            const documents = RandomEntities.documents(6, { projectId: project.id, commitHash: commit.hash });
            await documentRepository.save(documents);

            await service.notifyDocumentationChanged(project.id, commit.hash);
            verify(MockedEmailService.sendEmail(anything(), anything())).never();
        });

        it('does not send any email if there are no developers for the project', async () => {
            assert.isTrue(project.areEmailNotificationsEnabled);

            const documents = RandomEntities.documents(6, { projectId: project.id, commitHash: commit.hash });
            await documentRepository.save(documents);

            await service.notifyDocumentationChanged(project.id, commit.hash);
            verify(MockedEmailService.sendEmail(anything(), anything())).never();
        });

        it('does nothing if no documents are given', async () => {
            assert.isTrue(project.areEmailNotificationsEnabled);
            await service.notifyDocumentationChanged(project.id, commit.hash);
            verify(MockedEmailService.sendEmail(anything(), anything())).never();
        });

        it('does nothing if the commit does not exist', async () => {
            await service.notifyDocumentationChanged(project.id, 'this commit does not exist');
            verify(MockedEmailService.sendEmail(anything(), anything())).never();
        });

        it('does nothing if the project does not exist', async () => {
            await service.notifyDocumentationChanged(-1, commit.hash);
            verify(MockedEmailService.sendEmail(anything(), anything())).never();
        });
    });
});
