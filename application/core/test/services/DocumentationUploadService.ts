import * as path from 'path';
import * as faker from 'faker';
import { assert } from 'chai';
import { describe, it, before, after } from 'mocha';
import { mock, when, anything, instance, anyString, spy, capture } from 'ts-mockito';
import { Database } from '@core/services/Database';
import { DocumentationUploadService } from '@core/services/DocumentationUploadService';
import { FullTextSearchRecord } from '@core/entities/FullTextSearchRecord';
import { DocumentationRecord } from '@core/entities/DocumentationRecord';
import { MarkdownDocument } from '@core/models/text/MarkdownDocument';
import { CompressedHtmlDocument } from '@core/models/text/CompressedHtmlDocument';
import { DatabaseTestTools } from '@core-tests/tools/DatabaseTestTools';
import { ResourceLoader } from '@core-tests/tools/ResourceLoader';
import { Commit } from '@core/entities/Commit';
import { RandomEntities } from '@core-tests/tools/RandomEntities';
import { FileService } from '@core/services/FileService';
import { asyncAssert } from '@core-tests/tools/CustomAssertions';
import { CommitService } from '@core/services/CommitService';
import { EntityManager } from 'typeorm';

describe('DocumentationUploadService', () => {
    let existingProjectId = 0;
    let existingCommitHash = '';
    let commitService: CommitService;
    let service: DocumentationUploadService;

    before(async () => {
        const { project } = await DatabaseTestTools.createFakeTenantUserProject();
        existingProjectId = project.id;

        const commitRepository = await Database.repository(Commit);
        const commit = RandomEntities.commit;
        commit.projectId = existingProjectId;
        existingCommitHash = commit.hash;
        await commitRepository.save(commit);

        commitService = new CommitService(commitRepository);
        service = new DocumentationUploadService(await Database.entityManager(), new FileService(), commitService);
    });

    after(async () => DatabaseTestTools.clear());

    describe('#upload', () => {
        it('should correctly upload one markdown file', async () => {
            const markdown = ResourceLoader.asString('mock-markdown.md');
            const markdownDocument = new MarkdownDocument(markdown);
            const meta = {
                commitHash: existingCommitHash,
                repositoryPath: 'docs/important/document.md'
            };

            await service.upload(existingProjectId, markdownDocument, meta);

            const connection = await Database.connection();
            const fullTextSearchRecords = await connection.getRepository(FullTextSearchRecord).find();
            const documentationRecords = await connection.getRepository(DocumentationRecord).find();

            assert.lengthOf(documentationRecords, 1);
            assert.lengthOf(fullTextSearchRecords, 7);

            const documentationRecord = documentationRecords[0];
            assert.deepInclude(documentationRecord, meta);
            assert.equal(documentationRecord.projectId, existingProjectId);
            assert.isTrue(documentationRecord.isLatest);
            assert.isFalse(documentationRecord.isDeleted);

            const compressedHtmlDocument = new CompressedHtmlDocument(documentationRecord.compressedContent);
            assert.equal(compressedHtmlDocument.inflate().content, markdownDocument.toHtml().content);

            assert.deepInclude(fullTextSearchRecords[0], {
                archiveId: documentationRecord.id,
                corpusRelevance: 1,
                element: 'h1',
                textContent: 'Big title'
            });

            assert.deepInclude(fullTextSearchRecords[1], {
                archiveId: documentationRecord.id,
                corpusRelevance: 2,
                element: 'h2',
                textContent: 'Code example'
            });

            assert.deepInclude(fullTextSearchRecords[2], {
                archiveId: documentationRecord.id,
                corpusRelevance: 5,
                element: 'p',
                textContent: 'Paragraph description and long insightful brooding'
            });

            assert.deepInclude(fullTextSearchRecords[3], {
                archiveId: documentationRecord.id,
                corpusRelevance: 5,
                element: 'p',
                textContent: 'The end of the show!'
            });

            assert.deepInclude(fullTextSearchRecords[4], {
                archiveId: documentationRecord.id,
                corpusRelevance: 5,
                element: 'li',
                textContent: 'first scary motivation;'
            });

            assert.deepInclude(fullTextSearchRecords[5], {
                archiveId: documentationRecord.id,
                corpusRelevance: 5,
                element: 'li',
                textContent: 'second less scary motivation of doom.'
            });

            assert.deepInclude(fullTextSearchRecords[6], {
                archiveId: documentationRecord.id,
                corpusRelevance: 6,
                element: 'code',
                textContent: 'let example = new Example();\nexample.brood = false;\n'
            });
        });

        it('throws an exception if repositoryPath is empty', async () => {
            asyncAssert.promiseRejected(
                service.upload(existingProjectId, new MarkdownDocument(''), { commitHash: 'asd', repositoryPath: '' }),
                'repositoryPath must not be empty'
            );
        });

        it('throws an exception if commit does not exist', async () => {
            asyncAssert.promiseRejected(
                service.upload(existingProjectId, new MarkdownDocument(''), {
                    commitHash: 'asd',
                    repositoryPath: 'doc.md'
                }),
                'Cannot upload document because target commit asd was not found'
            );
        });
    });

    describe('#uploadArchive', () => {
        let MockedFileService: FileService;
        let MockedCommitService: CommitService;
        let service: DocumentationUploadService;
        let spiedService: DocumentationUploadService;

        // This will be spawned by the mocked FileService
        let mockTempDir: string;
        // And these will be the fake extracted contents from the archivePath
        let mockExtractedPaths: string[];
        let archivePath: string;

        before(() => {
            MockedFileService = mock(FileService);
            MockedCommitService = mock(CommitService);

            archivePath = faker.internet.url();
            mockTempDir = faker.random.word();
            mockExtractedPaths = [
                faker.random.word() + '.md',
                faker.random.word() + '.md',
                faker.random.word() + '.md',
                faker.random.word() // Just to check only documents are processed
            ];

            when(MockedCommitService.exists(existingProjectId, existingCommitHash)).thenResolve(true);
            when(MockedFileService.createDirectoryForExtraction(anyString())).thenResolve(mockTempDir);
            when(MockedFileService.extractArchive(archivePath, mockTempDir)).thenResolve();
            when(MockedFileService.enumerateFilesRecursively(mockTempDir)).thenResolve(mockExtractedPaths);
            when(MockedFileService.fsp).thenReturn({
                mkdir: null,
                readdir: null,
                stat: null,
                writeFile: null,
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                readFile: (path: any) => Promise.resolve(path)
            });

            service = new DocumentationUploadService(
                instance(mock(EntityManager)),
                instance(MockedFileService),
                instance(MockedCommitService)
            );
            spiedService = spy(service);
        });

        it('should upload one document entry for each file in the archive', async () => {
            await service.uploadArchive(existingProjectId, existingCommitHash, archivePath);

            const uploadInvocations = capture(spiedService.upload);

            for (let i = 0; i < 3; ++i) {
                const ithInvocation = uploadInvocations.byCallIndex(i);
                const [projectId, markdownDocument, meta] = ithInvocation;
                assert.equal(projectId, existingProjectId);
                assert.equal(markdownDocument.content, path.join(mockTempDir, mockExtractedPaths[i]));
                assert.deepEqual(meta, {
                    commitHash: existingCommitHash,
                    repositoryPath: mockExtractedPaths[i]
                });
            }
        });

        it('should keep uploading documents even if one fails', async () => {
            // Fail on the second upload
            when(spiedService.upload(anything(), anything(), anything())).thenReject(new Error('Arbitrary error'));

            await service.uploadArchive(existingProjectId, existingCommitHash, archivePath);

            const uploadInvocations = capture(spiedService.upload);
            assert.isOk(uploadInvocations.first());
            assert.isOk(uploadInvocations.second());
            assert.isOk(uploadInvocations.third());
        });

        it('should throw an exception if archivePath is empty', async () => {
            asyncAssert.promiseRejected(
                service.uploadArchive(existingProjectId, existingCommitHash, ''),
                'archivePath must not be empty'
            );
        });

        it('should throw an exception if commit does not exist', async () => {
            const invalidCommitHash = faker.random.uuid();
            when(MockedCommitService.exists(existingProjectId, invalidCommitHash)).thenResolve(false);
            asyncAssert.promiseRejected(
                service.uploadArchive(existingProjectId, invalidCommitHash, archivePath),
                `Cannot upload document because target commit ${invalidCommitHash} was not found`
            );
        });
    });

    describe('#markAsDeleted', () => {
        it('should create a new empty DocumentationRecord with isDeleted = true and mark all previous ones with isLatest = false', async () => {
            const markdown = ResourceLoader.asString('mock-markdown.md');
            const markdownDocument = new MarkdownDocument(markdown);
            const meta = {
                commitHash: existingCommitHash,
                repositoryPath: 'docs/semi-relevant/document.md'
            };

            const firstVersion = await service.upload(existingProjectId, markdownDocument, meta);
            await service.markAsDeleted(existingProjectId, meta.commitHash, meta.repositoryPath);

            const connection = await Database.connection();
            const documentationRecords = await connection.getRepository(DocumentationRecord).find();
            const fullTextSearchRecords = await connection.getRepository(FullTextSearchRecord).find();

            assert.isNotEmpty(fullTextSearchRecords);
            assert.isTrue(
                fullTextSearchRecords.filter(sr => sr.archiveId === firstVersion.id).every(sr => !sr.isLatest),
                'Expected all full text search records to not be latest any more'
            );

            const updatedFirstVersion = documentationRecords.find(
                dr => dr.repositoryPath === meta.repositoryPath && dr.id === firstVersion.id
            );
            assert.isFalse(updatedFirstVersion.isLatest);
            assert.isFalse(updatedFirstVersion.isDeleted);

            const newDeletedVersion = documentationRecords.find(
                dr => dr.repositoryPath === meta.repositoryPath && dr.id !== firstVersion.id
            );
            assert.isTrue(newDeletedVersion.isLatest);
            assert.isTrue(newDeletedVersion.isDeleted);
        });

        it('throws an exception if repositoryPath is empty', async () => {
            asyncAssert.promiseRejected(
                service.markAsDeleted(existingProjectId, 'asd', ''),
                'repositoryPath must not be empty'
            );
        });
    });
});
