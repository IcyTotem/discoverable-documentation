import * as chai from 'chai';
import chaiExclude from 'chai-exclude';
import { describe, it, before, after } from 'mocha';
import { DeveloperService } from '@core/services/DeveloperService';
import { DatabaseTestTools } from '@core-tests/tools/DatabaseTestTools';
import { RandomEntities } from '@core-tests/tools/RandomEntities';
import { Developer } from '@core/entities/Developer';
import { Database } from '@core/services/Database';

const assert = chai.assert;
chai.use(chaiExclude);

describe('DeveloperService', () => {
    let developerService;
    let existingProjectId = 0;

    before(async () => {
        const { project } = await DatabaseTestTools.createFakeTenantUserProject();
        existingProjectId = project.id;
        developerService = new DeveloperService(await Database.repository(Developer));
    });

    after(async () => DatabaseTestTools.clear());

    it('should allow to add, list and remove developers from a project', async () => {
        const people = RandomEntities.developers(10);
        await developerService.add(existingProjectId, people);

        const developers = await developerService.list(existingProjectId);
        assert.deepEqualExcluding(developers.sort(), people.sort() as Developer[], ['id', 'projectId']);
        assert.isTrue(developers.every(d => d.projectId === existingProjectId));

        const slackers = [developers[0], developers[2], developers[7]];
        await developerService.remove(existingProjectId, [
            slackers[0],
            { email: slackers[1].email },
            slackers[2].email
        ]);

        const survivors = await developerService.list(existingProjectId);
        assert.includeDeepMembers(developers, survivors);

        // They probably forgot to include the definition of notIncludeDeepMembers in @types/chai,
        // but it actually exists as reported in the documentation
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        (assert as any).notIncludeDeepMembers(survivors, slackers);
    });

    describe('#list', () => {
        it('should return an empty array if project does not exist', async () => {
            assert.isEmpty(await developerService.list(-1));
        });
    });

    describe('#remove', () => {
        it('should completely silently if project or email do not exist', async () => {
            await developerService.remove(-1, ['fake@email.com']);
        });
    });
});
