import * as faker from 'faker';
import { assert } from 'chai';
import { Repository } from 'typeorm';
import { describe, it, beforeEach, afterEach } from 'mocha';
import { DatabaseTestTools } from '@core-tests/tools/DatabaseTestTools';
import { Database } from '@core/services/Database';
import { asyncAssert } from '@core-tests/tools/CustomAssertions';
import { ProjectService, ProjectMeta } from '@core/services/ProjectService';
import { Project } from '@core/entities/Project';

describe('ProjectService', () => {
    let projectRepository: Repository<Project>;
    let projectService: ProjectService;
    let existingTenantId: number;
    let existingUserId: number;
    let existingProjectId: number;

    beforeEach(async () => {
        const { tenant, user, project } = await DatabaseTestTools.createFakeTenantUserProject();
        existingTenantId = tenant.id;
        existingUserId = user.id;
        existingProjectId = project.id;

        projectRepository = await Database.repository(Project);
        projectService = new ProjectService(projectRepository);
    });

    afterEach(async () => DatabaseTestTools.clear());

    describe('#list', () => {
        it('should return an empty list for invalid tenant', async () => {
            const projects = await projectService.list(-1);
            assert.isEmpty(projects);
        });

        it('should return all projects associated with a given tenant', async () => {
            const projects = await projectService.list(existingTenantId);
            const expectedProject = await projectRepository.findOne({
                where: { id: existingProjectId, tenantId: existingTenantId }
            });
            assert.deepEqual(projects, [expectedProject]);
        });
    });

    describe('#create', () => {
        it('should throw an exception if the project name is blank', async () => {
            const attemptCreation = (): Promise<Project> =>
                projectService.create(existingTenantId, {
                    name: '',
                    masterBranch: null,
                    ownerId: null,
                    areEmailNotificationsEnabled: true
                });
            await asyncAssert.promiseRejected(attemptCreation(), 'Project name cannot be blank');
        });

        it('should create a project when parameters are valid', async () => {
            const meta: ProjectMeta = {
                name: faker.random.word(),
                description: faker.random.word(),
                masterBranch: null,
                ownerId: existingUserId,
                areEmailNotificationsEnabled: true
            };

            const project = await projectService.create(existingTenantId, meta);
            assert.deepInclude(project, { ...meta, masterBranch: 'master' });
            assert.isOk(project.apiKey);
            assert.isAtLeast(project.apiKey.length, 32);
            assert.instanceOf(project.creationTime, Date);
            assert.equal(project.tenantId, existingTenantId);

            const allProjects = await projectService.list(existingTenantId);
            assert.deepInclude(allProjects, project);
        });
    });

    describe('#findByApiKey', () => {
        it('should throw an exception if the api key does not exist', async () => {
            await asyncAssert.promiseRejected(projectService.findByApiKey(''));
        });

        it('should return a project with the given api key', async () => {
            const project = await projectRepository.findOne(existingProjectId);
            const foundByApiKey = await projectService.findByApiKey(project.apiKey);
            assert.deepEqual(foundByApiKey, project);
        });
    });
});
