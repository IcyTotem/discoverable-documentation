# Technical communication

Technical communication is a means to convey scientific, engineering, or other technical information.

  1. Individuals in a variety of contexts and with varied professional credentials engage in technical communication. Some individuals are designated as technical communicators or technical writers. These individuals use a set of methods to research, document, and present technical processes or products. Technical communicators may put the information they capture into paper documents, web pages, computer-based training, digitally stored text, audio, video, and other media. The Society for Technical Communication defines the field as any form of communication that focuses on technical or specialized topics, communicates specifically by using _technology_ or provides instructions on how to do something.
  2. More succinctly, the Institute of Scientific and Technical Communicators defines technical communication as factual communication, usually about products and services.
  3. The European Association for Technical Communication briefly defines technical communication as "the process of defining, creating and delivering information products for the safe, efficient and effective use of products (technical systems, software, services)".

Whatever the definition of technical communication, the overarching goal of the practice is to create easily accessible information for a specific audience.[6]

## Technical communicators

Technical communicators generally *tailor* information to a specific audience, which may be subject matter experts, consumers, end users, etc. Technical communicators often work collaboratively to create deliverables that include online help, user manuals, classroom training guides, computer-based training, white papers, specifications, industrial videos, reference cards, data sheets, journal articles, and patents. Technical domains can be of any kind, including the soft and hard sciences, high technology including computers and software and consumer electronics. Technical communicators often work with a range of specific Subject-matter experts (SMEs) on these educational projects.

| title              | not long ago    | in a far-off land       | a kind         | was born                |
|--------------------|-----------------|-------------------------|----------------|-------------------------|
| he was             | totally awesome | and everybody loved him | however        | in the shadows          |
| his pale brother   | schemed.        | an ordeal was coming    | for him        | but he was              |
| blissfully unaware | of the danger.  | so time passed          | and eventually | the scheme was enacted. |

Technical communication jobs include the following:
  - API writer, 
  - e-learning author, 
  - information architect, 
  - technical content developer, 
  - technical editor, 
  - technical illustrator, 
  - technical trainer, 
  - technical translator, 
  - technical writer, 
  - usability expert, 
  - user experience designer, 
  - and user interface designer. 
  
Other jobs available to technical communicators include digital strategist, marketing specialist, and content manager.

In 2015, the European Association for Technical Communication published a competence framework for the professional field of technical communication.