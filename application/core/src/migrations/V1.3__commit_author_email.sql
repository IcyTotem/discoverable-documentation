alter table commits
    drop column author,
    add column author_name text not null,
    add column author_email text not null;