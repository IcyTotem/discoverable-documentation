-- This is used for full-text search. Every document will be split into headers, paragraphs, list items
-- and code. Each chunk will be processed and memorized separately. Processing includes removing stop words,
-- lemmatizing, stemming, lower casing, etc... (notice Postgres already has native support for many of
-- these operations).
create table full_text_search_archive(
    id bigserial primary key,
    project_id bigint not null,
    archive_id bigint not null,
    element text not null,
    corpus_relevance smallint not null default 1,
    content_vector tsvector,
    text_content text not null,
    index_timestamp timestamp without time zone,

    -- This is a denormalized copy of documentation_archive.is_latest to avoid
    -- joins during the full text search query. Consistency is maintained in the trigger
    -- mark_latest_archive_record()
    is_latest boolean not null default true
);

create index full_text_search_archive_content_vector_index on full_text_search_archive using gin(content_vector);
create index full_text_search_archive_project_id on full_text_search_archive(project_id);
alter table full_text_search_archive
    alter column index_timestamp set default now();
alter table full_text_search_archive
    add constraint full_text_search_archive_archives_fk foreign key (archive_id) references documentation_archive(id) on delete cascade,
    add constraint full_text_search_archive_projects_fk foreign key (project_id) references projects(id) on delete cascade;

create or replace function compute_content_vector() returns trigger as
$body$
begin
    new.content_vector = to_tsvector(new.text_content);
    return new;
end;
$body$ language 'plpgsql';

create trigger full_text_search_archive_trigger
    before insert on full_text_search_archive
    for each row
    execute procedure compute_content_vector();
