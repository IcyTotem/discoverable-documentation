create table commits(
    hash text not null,
    project_id bigint not null,
    branch_name text not null,
    author text not null,
    creation_time timestamp without time zone not null,
    compressed_master_diff bytea default null,
    contains_documentation boolean default true,
    triggers_reminder boolean default false
);

alter table commits
    add primary key (hash, project_id),
    add constraint commits_projects_fk foreign key (project_id) references projects(id) on delete cascade;
