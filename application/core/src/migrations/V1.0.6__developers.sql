-- Developers are slightly different from users in that they are associated to a specific
-- project by the agent script that inspects a local git repository. Developers may not be
-- user of the platform directly but they will still receive notifications by e-mail when
-- new documentation is added
create table developers(
    id bigserial primary key,
    project_id bigint not null,
    name text not null,
    email text not null
);

create index developers_project_id_index on developers(project_id);
alter table developers
    add constraint developers_projects_fk foreign key (project_id) references projects(id) on delete cascade,
    add constraint developers_project_email_unique unique (project_id, email);