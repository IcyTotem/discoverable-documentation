alter table projects 
    add column api_key text not null,
    add column master_branch text not null default 'master';

alter table projects
    add constraint project_api_key_unique unique (api_key);