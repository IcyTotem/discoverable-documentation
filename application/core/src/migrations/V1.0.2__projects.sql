create table projects(
    id bigserial primary key,
    tenant_id bigint not null,
    owner_id bigint not null,
    name text not null,
    email_notifications_enabled boolean default true
);

create index projects_tenant_id_index on projects(tenant_id);
create index projects_owner_id_index on projects(owner_id);
alter table projects
    add constraint projects_tenants_fk foreign key (tenant_id) references tenants(id) on delete cascade,
    add constraint projects_owners_fk foreign key (owner_id) references users(id) on delete no action;

create table projects_authorizations(
    project_id bigint not null,
    user_id bigint not null,
    can_view boolean default false
);

create index projects_authorizations_project_id_index on projects_authorizations(project_id);
create index projects_authorizations_user_id_index on projects_authorizations(user_id);
alter table projects_authorizations
    add constraint projects_authorizations_projects_fk foreign key (project_id) references projects(id) on delete cascade,
    add constraint projects_authorizations_users_fk foreign key (user_id) references users(id) on delete cascade;
