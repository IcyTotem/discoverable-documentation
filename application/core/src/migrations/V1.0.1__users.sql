create table users(
    id bigserial primary key,
    tenant_id bigint not null,
    name text not null,
    email text unique not null
);

create index users_tenant_id_index on users(tenant_id);
alter table users
    add constraint users_tenants_fk foreign key (tenant_id) references tenants(id) on delete cascade;