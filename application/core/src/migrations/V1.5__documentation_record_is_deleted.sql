alter table documentation_archive add column is_deleted boolean not null default false;
alter table documentation_archive alter column compressed_content drop not null;