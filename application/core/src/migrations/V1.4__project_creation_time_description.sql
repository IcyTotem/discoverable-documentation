alter table projects
    add column description text default null,
    add column creation_time timestamp without time zone not null;