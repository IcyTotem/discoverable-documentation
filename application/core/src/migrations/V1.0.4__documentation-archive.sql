-- This table contains all the historic documentation files that were present in the
-- repository at some point in time. It's essentially a snapshot of every file after each change.
-- Contents are gzipped html strings
create table documentation_archive(
    id bigserial primary key,
    project_id bigint not null,
    repository_path text not null,
    commit_hash text not null,
    compressed_content bytea not null,
    is_latest boolean not null default true
);

-- This virtual table ensures a nice view on the most up-to-date documents for each project
create view latest_documentation_snapshot as
    select * from documentation_archive
    where is_latest = true;

create index documentation_archive_project_id_index on documentation_archive(project_id);
alter table documentation_archive
    add constraint documentation_archive_projects_fk foreign key (project_id) references projects(id) on delete cascade,
    add constraint documentation_archive_commits_fk foreign key (project_id, commit_hash) references commits(project_id, hash) on delete cascade;

create or replace function mark_latest_archive_record() returns trigger as
$body$
begin
    -- Mark all entries that share the same project and repository path as historic,
    -- i.e. all previous versions of the same document
    update documentation_archive
        set is_latest = false
        where project_id = new.project_id and repository_path = new.repository_path;
    
    -- Synchronize all full text records associated with their associated archive record
    update full_text_search_archive as f
        set is_latest = false
        from documentation_archive as a
        where f.is_latest = true and a.is_latest = false and f.archive_id = a.id;

    -- Ensure the document being added is actually marked as latest
    new.is_latest = true;
    return new;
end;
$body$ language 'plpgsql';

create trigger documentation_archive_trigger
    before insert on documentation_archive
    for each row
    execute procedure mark_latest_archive_record();