import * as zlib from 'zlib';
import { Repository } from 'typeorm';
import { Commit } from '@core/entities/Commit';

export interface CommitMeta {
    hash: string;
    authorName: string;
    authorEmail: string;
    branchName: string;
    creationTime: Date;
    diffFromMaster: string | null;
    containsDocumentation: boolean;
    triggersReminder: boolean;
}

export class CommitService {
    public constructor(private readonly commitRepository: Repository<Commit>) {}

    public async create(projectId: number, meta: CommitMeta): Promise<Commit> {
        const {
            hash,
            authorName,
            authorEmail,
            branchName,
            creationTime,
            diffFromMaster,
            containsDocumentation,
            triggersReminder
        } = meta;
        const compressedDiffFromMaster = diffFromMaster ? zlib.deflateSync(Buffer.from(diffFromMaster, 'utf8')) : null;

        const commit = new Commit();
        Object.assign(commit, {
            projectId,
            hash,
            authorName,
            authorEmail,
            branchName,
            creationTime,
            compressedDiffFromMaster,
            containsDocumentation,
            triggersReminder
        });

        return this.commitRepository.save(commit);
    }

    public async getLatestHash(projectId: number, branchName: string): Promise<string | null> {
        const commitCandidates = await this.commitRepository.find({
            select: ['hash'],
            where: { projectId, branchName },
            order: { creationTime: 'DESC' },
            take: 1
        });
        return commitCandidates.length === 1 ? commitCandidates[0].hash : null;
    }

    public async exists(projectId: number, hash: string): Promise<boolean> {
        if (hash.length === 0) {
            return false;
        }

        const commit = await this.commitRepository.findOne({
            select: ['hash'],
            where: { projectId, hash }
        });

        return !!commit;
    }
}
