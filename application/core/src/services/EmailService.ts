import * as nodemailer from 'nodemailer';
import { HtmlDocument } from '@core/models/text/HtmlDocument';

export interface EmailOptions {
    subject: string;
    body: HtmlDocument | string;
}

export class EmailService {
    // The type is defined in @types/nodemailer but all the imports I tried seem not to be able to find it
    private readonly transporter;
    private readonly senderDisplayName: string;
    private readonly senderEmail: string;
    private readonly emailValidationRegex: RegExp;

    constructor() {
        this.senderDisplayName = EmailService.senderDisplayName;
        this.senderEmail = EmailService.senderEmail;
        this.emailValidationRegex = EmailService.emailValidationRegex;
        this.transporter = nodemailer.createTransport({
            host: EmailService.host,
            port: EmailService.port,
            secure: true,
            auth: {
                user: EmailService.user,
                pass: EmailService.password
            }
        });
    }

    /**
     * Send an email with the given subject and body to all valid addresses in recipientEmails using nodemailer.
     * @param recipientEmails Array of strings representing (potentially invalid) email addresses.
     * @param options Subject and body of the message to send.
     */
    public async sendEmail(recipientEmails: string[], options: EmailOptions): Promise<void> {
        if (recipientEmails.length > 0 && options.subject && options.body) {
            await this.transporter.sendMail({
                from: `"${this.senderDisplayName}" <${this.senderEmail}>`,
                to: recipientEmails.filter(email => email.match(this.emailValidationRegex)).join(', '),
                subject: options.subject,
                text: typeof options.body === 'string' ? options.body : undefined,
                html: typeof options.body !== 'string' ? options.body.content : undefined
            });
        }
    }

    private static get host(): string {
        return process.env['NODEMAILER_HOST'];
    }

    private static get port(): number {
        return Number(process.env['NODEMAILER_PORT']);
    }

    private static get user(): string {
        return process.env['NODEMAILER_USER'];
    }

    private static get password(): string {
        return process.env['NODEMAILER_PASSWORD'];
    }

    private static get emailValidationRegex(): RegExp {
        // Taken from https://emailregex.com/
        // Should work in most cases
        return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    }

    static get senderEmail(): string {
        return process.env['DOC_NOTIFICATION_EMAIL'];
    }

    static get senderDisplayName(): string {
        return process.env['DOC_NOTIFICATION_EMAIL_NAME'];
    }
}
