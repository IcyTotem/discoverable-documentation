import * as crypto from 'crypto';
import { Repository } from 'typeorm';
import { Project } from '@core/entities/Project';
import { notBlank } from '@core/services/validation/ValidationHelper';

export interface ProjectMeta {
    ownerId: number;
    name: string;
    description?: string;
    masterBranch: string;
    areEmailNotificationsEnabled: boolean;
}

function generateNewApiKey(): string {
    const currentTime = new Date().getTime();
    const randomNumber = Math.random();
    return crypto
        .createHash('sha512')
        .update(currentTime.toString())
        .update(randomNumber.toString())
        .digest('base64');
}

function utcTime(): Date {
    const now = new Date();
    const timestampUtc = Date.UTC(
        now.getUTCFullYear(),
        now.getUTCMonth(),
        now.getUTCDate(),
        now.getUTCHours(),
        now.getUTCMinutes(),
        now.getUTCSeconds()
    );
    return new Date(timestampUtc);
}

export class ProjectService {
    constructor(private readonly projectRepository: Repository<Project>) {}

    public async list(tenantId: number): Promise<Project[]> {
        return this.projectRepository.find({ where: { tenantId } });
    }

    public async create(tenantId: number, meta: ProjectMeta): Promise<Project> {
        notBlank(meta.name, 'Project name cannot be blank');

        // If project creation fail because of a constraint violation, save will throw
        // an exception and that will be propagated upwards
        const project = new Project();
        Object.assign(project, {
            tenantId,
            ownerId: meta.ownerId,
            name: meta.name,
            description: meta.description,
            creationTime: utcTime(),
            apiKey: generateNewApiKey(),
            masterBranch: meta.masterBranch || 'master',
            areEmailNotificationsEnabled: !!meta.areEmailNotificationsEnabled
        });

        return this.projectRepository.save(project);
    }

    public async findByApiKey(apiKey: string): Promise<Project> {
        return this.projectRepository.findOneOrFail({ where: { apiKey } });
    }
}
