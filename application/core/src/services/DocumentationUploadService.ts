import * as path from 'path';
import { EntityManager } from 'typeorm';
import { DocumentationRecord } from '@core/entities/DocumentationRecord';
import { MarkdownDocument } from '@core/models/text/MarkdownDocument';
import { HtmlDocument } from '@core/models/text/HtmlDocument';
import { HypertextSplitter } from '@core/services/HypertextSplitter';
import { FileService } from '@core/services/FileService';
import { CommitService } from '@core/services/CommitService';
import { NotFoundException } from '@core/services/exceptions/NotFoundException';
import { notBlank, assignValues } from './validation/ValidationHelper';

export interface DocumentMeta {
    repositoryPath: string;
    commitHash: string;
}

export class DocumentationUploadService {
    public constructor(
        private readonly entityManager: EntityManager,
        private readonly fileService: FileService,
        private readonly commitService: CommitService
    ) {}

    public async upload(
        projectId: number,
        markdownDocument: MarkdownDocument,
        meta: DocumentMeta
    ): Promise<DocumentationRecord> {
        notBlank(meta.repositoryPath, 'repositoryPath must not be blank');
        await this.checkCommitExists(projectId, meta.commitHash);

        const htmlDocument = markdownDocument.toHtml();
        const documentationRecord = this.makeDocumentationRecord(projectId, htmlDocument, meta);
        const htmlAnalyzer = new HypertextSplitter(htmlDocument);
        let savedDocumentationRecord: DocumentationRecord;

        await this.entityManager.transaction(async em => {
            savedDocumentationRecord = await em.save(documentationRecord);

            const pid = savedDocumentationRecord.projectId;
            const aid = savedDocumentationRecord.id;
            const promises = [];

            for (const searchRecord of htmlAnalyzer.searchRecords(pid, aid)) {
                promises.push(em.save(searchRecord));
            }

            await Promise.all(promises);
        });

        return savedDocumentationRecord;
    }

    public async markAsDeleted(projectId: number, commitHash: string, repositoryPath: string): Promise<void> {
        notBlank(repositoryPath, 'repositoryPath must not be blank');

        // The sql trigger on insert on the documentation record table will automatically flag
        // all previous records with the same path with is_latest = false
        const record = assignValues(new DocumentationRecord(), {
            projectId,
            repositoryPath,
            commitHash,
            isLatest: true,
            isDeleted: true
        });

        await this.entityManager.save(record);
    }

    public async uploadArchive(projectId: number, commitHash: string, archivePath: string): Promise<void> {
        notBlank(archivePath, 'archivePath must not be blank');
        await this.checkCommitExists(projectId, commitHash);

        const tempDir = await this.fileService.createDirectoryForExtraction(archivePath);
        await this.fileService.extractArchive(archivePath, tempDir);

        const extractedPaths = await this.fileService.enumerateFilesRecursively(tempDir);
        const extractedDocumentsPaths = extractedPaths.filter(ep => ep.endsWith('.md'));

        for (const extractedDocumentPath of extractedDocumentsPaths) {
            const absoluteDocumentPath = path.join(tempDir, extractedDocumentPath);
            const content = await this.fileService.fsp.readFile(absoluteDocumentPath, { encoding: 'utf-8' });
            try {
                await this.upload(projectId, new MarkdownDocument(content), {
                    commitHash,
                    repositoryPath: extractedDocumentPath
                });
            } catch (e) {
                console.error(
                    `Upload of ${extractedDocumentPath} for commit ${commitHash} in project ${projectId} failed:`,
                    e
                );
            }
        }
    }

    private makeDocumentationRecord(
        projectId: number,
        htmlDocument: HtmlDocument,
        meta: DocumentMeta
    ): DocumentationRecord {
        return assignValues(new DocumentationRecord(), {
            projectId,
            isDeleted: false,
            compressedContent: htmlDocument.deflate().content,
            repositoryPath: meta.repositoryPath,
            commitHash: meta.commitHash
        });
    }

    private async checkCommitExists(projectId: number, commitHash: string): Promise<void> {
        const isValidCommit = await this.commitService.exists(projectId, commitHash);
        if (!isValidCommit) {
            throw new NotFoundException(`Cannot upload document because target commit ${commitHash} was not found`);
        }
    }
}
