import * as fs from 'fs';
import * as path from 'path';
import { promisify } from 'util';
import * as unzipper from 'unzipper';
import { IllegalParameterException } from '@core/services/exceptions/IllegalParameterException';

type ErrorCallback = (e: NodeJS.ErrnoException) => void;

// Mini promisification of some fs methods
const fsp = {
    mkdir: promisify(fs.mkdir),
    readdir: promisify(fs.readdir),
    stat: promisify(fs.stat),
    readFile: promisify(fs.readFile),
    writeFile: promisify(fs.writeFile)
};

export class FileService {
    /**
     * Extract all the content of an archive to the destination directory, keeping the same file structure
     * of files from the archive.
     */
    async extractArchive(archivePath: string, destinationDirectory: string): Promise<void> {
        if (archivePath.length === 0) {
            throw new IllegalParameterException('archivePath must not be empty');
        }

        // For some reason, unzipper does not behave correctly when it is piped through
        // stream.pipeline (promisified pipeline returns before completion). Therefore
        // we have to manually handle events. Dirty, but it works
        // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
        return new Promise((resolve, reject) =>
            fs
                .createReadStream(archivePath)
                .pipe(unzipper.Extract({ path: destinationDirectory }))
                .on('error', reject)
                .on('close', resolve)
        );
    }

    /**
     * Return a list of all files contained in the given directory.
     * @param directory Relative or absolute path.
     * @returns An array of string, each of which is the relative path to a found file, assuming
     *     as a base path the directory given as parameter.
     */
    async enumerateFilesRecursively(directory: string): Promise<string[]> {
        const absoluteDirectoryPath = path.resolve(directory);
        const nodeNames = await fsp.readdir(absoluteDirectoryPath);
        const results = [];
        const subDirectoriesNodeNames = [];

        for (const nodeName of nodeNames) {
            const nodeAbsolutePath = path.join(absoluteDirectoryPath, nodeName);
            const info = await fsp.stat(nodeAbsolutePath);
            (info.isDirectory() ? subDirectoriesNodeNames : results).push(nodeName);
        }

        for (const nodeName of subDirectoriesNodeNames) {
            const subDirectoryAbsolutePath = path.join(absoluteDirectoryPath, nodeName);
            const subResults = await this.enumerateFilesRecursively(subDirectoryAbsolutePath);
            subResults.map(subResult => path.join(nodeName, subResult)).forEach(subResult => results.push(subResult));
        }

        return results;
    }

    /**
     * Create a new directory with the same name as the given archive in the same parent folder.
     */
    async createDirectoryForExtraction(archivePath: string): Promise<string> {
        if (archivePath.length === 0) {
            throw new IllegalParameterException('archivePath must not be empty');
        }

        const archiveBasename = path.basename(archivePath);
        const archiveDirectory = path.dirname(archivePath);
        const newPath = path.join(archiveDirectory, `${archiveBasename}-extracted`);
        await fsp.mkdir(newPath);
        return newPath;
    }

    /**
     * Return a promisification of the native fs module.
     */
    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    public get fsp() {
        return fsp;
    }
}
