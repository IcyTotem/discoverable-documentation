import { DocumentationRecord } from '@core/entities/DocumentationRecord';
import { HtmlDocument } from '@core/models/text/HtmlDocument';
import { Database } from '@core/services/Database';

export interface SearchOptions {
    pageIndex: number;
    resultsPerPage: number;
    onlyIncludeLatest: boolean;
}

export interface SearchResultHeadline {
    element: string;
    html: HtmlDocument;
}

export interface SearchResultEntry {
    document: DocumentationRecord;
    relevance: number;
    headlines: SearchResultHeadline[];
}

interface RawSearchResultEntry {
    archive_id: number;
    headline: string;
    element: string;
    id: number;
    index_timestamp: Date;
    is_latest: boolean;
    rank: number;
}

class DocumentMap {
    private readonly map: { [id: number]: DocumentationRecord } = {};

    constructor(documents: DocumentationRecord[]) {
        documents.forEach(document => (this.map[document.id] = document));
    }

    getById(id: number): DocumentationRecord {
        return this.map[id];
    }
}

class SearchResultCollection {
    private readonly map: { [documentId: number]: SearchResultEntry } = {};

    add(document: DocumentationRecord): SearchResultEntry {
        if (document.id in this.map) {
            return this.map[document.id];
        }

        const entry: SearchResultEntry = {
            document,
            headlines: [],
            relevance: 0
        };

        return (this.map[document.id] = entry);
    }

    sortedByRelevance(): SearchResultEntry[] {
        // Sorted in descending order of relevance
        return Object.values(this.map).sort((a, b) => b.relevance - a.relevance);
    }
}

export class SearchService {
    public async search(projectId: number, query: string, options: SearchOptions): Promise<SearchResultEntry[]> {
        if (query.match(/^\s*$/g) || options.resultsPerPage <= 0) {
            return [];
        }

        const offset = options.pageIndex * options.resultsPerPage;
        const rawResultEntries = options.onlyIncludeLatest
            ? await this.searchLatest(projectId, query, options.resultsPerPage, offset)
            : await this.searchHistory(projectId, query, options.resultsPerPage, offset);
        return this.processRawSearchResults(rawResultEntries);
    }

    private async processRawSearchResults(entries: RawSearchResultEntry[]): Promise<SearchResultEntry[]> {
        const archiveIdSet = new Set<number>(entries.map(entry => entry.archive_id));
        const uniqueArchiveIds = Array.from(archiveIdSet.values());

        const repo = await Database.repository(DocumentationRecord);
        const documents = await repo.findByIds(uniqueArchiveIds);

        const resultsCollection = new SearchResultCollection();
        const documentMap = new DocumentMap(documents);

        for (const entry of entries) {
            const document = documentMap.getById(entry.archive_id);
            const result = resultsCollection.add(document);
            result.headlines.push({
                element: entry.element,
                html: new HtmlDocument(entry.headline)
            });
            result.relevance += entry.rank;
        }

        return resultsCollection.sortedByRelevance();
    }

    private async searchHistory(
        projectId: number,
        query: string,
        resultsCount: number,
        offset: number
    ): Promise<RawSearchResultEntry[]> {
        const connection = await Database.connection();
        const params = [query, projectId, resultsCount, offset];

        return connection.query(
            `SELECT 
                a.id,
                a.index_timestamp,
                a.archive_id,
                a.is_latest,
                a.element,
                ts_rank_cd(a.content_vector, query, 1) as rank,
                ts_headline(a.text_content, query) as headline
            FROM 
                full_text_search_archive as a, 
                plainto_tsquery($1) as query
            WHERE 
                a.content_vector @@ query AND
                a.project_id = $2
            ORDER BY
                is_latest DESC,
                corpus_relevance ASC,
                rank DESC,
                index_timestamp DESC
            LIMIT $3
            OFFSET $4;`,
            params
        );
    }

    private async searchLatest(
        projectId: number,
        query: string,
        resultsCount: number,
        offset: number
    ): Promise<RawSearchResultEntry[]> {
        const connection = await Database.connection();
        const params = [query, projectId, resultsCount, offset];

        return connection.query(
            `SELECT 
                a.id,
                a.index_timestamp,
                a.archive_id,
                a.is_latest,
                a.element,
                ts_rank_cd(a.content_vector, query, 1) as rank,
                ts_headline(a.text_content, query) as headline
            FROM 
                full_text_search_archive as a, 
                plainto_tsquery($1) as query
            WHERE 
                a.is_latest IS TRUE AND
                a.content_vector @@ query AND
                a.project_id = $2
            ORDER BY
                corpus_relevance ASC,
                rank DESC,
                index_timestamp DESC
            LIMIT $3
            OFFSET $4;`,
            params
        );
    }
}
