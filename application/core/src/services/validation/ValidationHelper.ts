import { IllegalParameterException } from '@core/services/exceptions/IllegalParameterException';

const BLANK_REGEX = /^\s*$/gi;

export function notBlank(value: string, message: string): void {
    if (value.match(BLANK_REGEX)) {
        throw new IllegalParameterException(message);
    }
}

export function assignValues<T>(destination: T, source: Partial<T>): T {
    return Object.assign(destination, source);
}
