import { Developer } from '@core/entities/Developer';
import { Repository } from 'typeorm';

export interface DeveloperMeta {
    name: string;
    email: string;
}

export class DeveloperService {
    public constructor(private readonly repository: Repository<Developer>) {}

    public async add(projectId: number, developers: DeveloperMeta[]): Promise<void> {
        await this.repository
            .createQueryBuilder()
            .insert()
            .into(Developer)
            .values(developers.map(d => ({ projectId, name: d.name, email: d.email })))
            .onConflict('ON CONSTRAINT developers_project_email_unique DO NOTHING')
            .execute();
    }

    public async list(projectId: number): Promise<Developer[]> {
        return this.repository.find({ projectId });
    }

    public async remove(projectId: number, developersOrEmails: Array<{ email: string } | string>): Promise<void> {
        const emails = developersOrEmails.map(doe => (typeof doe === 'string' ? doe : doe.email));

        // Undocumented behaviour of typeorm with arrays used in IN clauses:
        // https://github.com/typeorm/typeorm/issues/1239#issuecomment-366955628
        await this.repository
            .createQueryBuilder()
            .delete()
            .from(Developer)
            .where('project_id = :projectId', { projectId })
            .andWhere('email in (:...emails)', { emails })
            .execute();
    }
}
