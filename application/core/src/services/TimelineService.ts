import { Repository } from 'typeorm';
import { Commit } from '@core/entities/Commit';
import { DocumentationRecord } from '@core/entities/DocumentationRecord';

const DEFAULT_COMMIT_COUNT = 10;

/**
 * Shorter version of DocumentationRecord.
 */
export interface TimelineDocument {
    id: number;
    repositoryPath: string;
}

/**
 * Shorter version of Commit.
 */
export interface TimelineCommit {
    hash: string;
    projectId: number;
    authorName: string;
    authorEmail: string;
    branchName: string;
    creationTime: Date;
}

export interface TimelineEntry {
    commit: TimelineCommit;
    documents: TimelineDocument[];
}

export class TimelineService {
    constructor(
        private readonly commitRepository: Repository<Commit>,
        private readonly documentRepository: Repository<DocumentationRecord>
    ) {}

    /**
     * For the given project, returns the most recent documentation changes as an array of TimelineEntry.
     * @param commitCount Maximum number of commits to retrieve in the past. Since there is a 1-to-1
     *     correspondence between commits and timeline entries, this effectively limits the number
     *     of returned entries. If unspecified, it falls back to the default (currently 10). If less than 1,
     *     no results will be returned.
     */
    public async fetchTimeline(projectId: number, commitCount?: number): Promise<TimelineEntry[]> {
        if (commitCount === undefined) {
            commitCount = DEFAULT_COMMIT_COUNT;
        }

        if (commitCount < 1) {
            return [];
        }

        const recentCommits = await this.collectRecentCommits(projectId, commitCount);
        const entriesPromises = recentCommits.map(commit => this.createEntryForCommit(commit));
        return Promise.all(entriesPromises);
    }

    private async collectRecentCommits(projectId: number, commitCount: number): Promise<TimelineCommit[]> {
        return this.commitRepository.find({
            select: ['hash', 'projectId', 'authorName', 'authorEmail', 'branchName', 'creationTime'],
            where: { projectId },
            order: { creationTime: 'DESC' },
            take: commitCount
        });
    }

    private async collectDocumentsForCommit(commit: TimelineCommit): Promise<TimelineDocument[]> {
        return this.documentRepository.find({
            select: ['id', 'repositoryPath'],
            where: { projectId: commit.projectId, commitHash: commit.hash }
        });
    }

    private async createEntryForCommit(commit: TimelineCommit): Promise<TimelineEntry> {
        const documents = await this.collectDocumentsForCommit(commit);
        return { commit, documents };
    }
}
