import * as moment from 'moment';
import { Repository } from 'typeorm';
import { HtmlDocument } from '@core/models/text/HtmlDocument';
import { DocumentationRecord } from '@core/entities/DocumentationRecord';
import { Developer } from '@core/entities/Developer';
import { Project } from '@core/entities/Project';
import { Commit } from '@core/entities/Commit';
import { EmailService } from '@core/services/EmailService';

export class ChangedDocumentationSummary {
    constructor(private project: Project, private date: Date, private documents: DocumentationRecord[]) {}

    getHtml(): HtmlDocument {
        const format = 'dddd MMMM Do YYYY, HH:mm';
        return new HtmlDocument(`<html>
          You are receiving this e-mail because you are a developer of ${this.project.name}.
          The following pieces of documentation have changed on ${moment(this.date).format(format)} (UTC):
          <ul>
          ${this.documents.map(document => this.getListItem(document)).join('')}
          </ul>
        </html>`);
    }

    private getListItem(document: DocumentationRecord): string {
        return `<li><pre>${document.repositoryPath}</pre></li>`;
    }
}

export class NotificationService {
    constructor(
        private readonly emailService: EmailService,
        private readonly projectRepository: Repository<Project>,
        private readonly developerRepository: Repository<Developer>,
        private readonly commitRepository: Repository<Commit>,
        private readonly documentRepository: Repository<DocumentationRecord>
    ) {}

    public async notifyDocumentationChanged(projectId: number, commitHash: string): Promise<void> {
        const project = await this.projectRepository.findOne(projectId);
        if (!project) {
            return;
        }

        const commit = await this.commitRepository.findOne({
            where: {
                projectId,
                hash: commitHash,
                containsDocumentation: true
            }
        });
        if (!commit) {
            return;
        }

        const documents = await this.documentRepository.find({ where: { projectId, commitHash } });
        if (documents.length === 0) {
            return;
        }

        if (project.areEmailNotificationsEnabled) {
            const developers = await this.developerRepository.find({ projectId });

            if (developers.length > 0) {
                const summary = new ChangedDocumentationSummary(project, commit.creationTime, documents);
                await this.emailService.sendEmail(developers.map(dev => dev.email), {
                    subject: `New documentation as added to ${project.name}`,
                    body: summary.getHtml()
                });
            }
        }
    }
}
