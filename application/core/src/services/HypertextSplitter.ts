import * as cheerio from 'cheerio';
import { HtmlDocument } from '@core/models/text/HtmlDocument';
import { FullTextSearchRecord } from '@core/entities/FullTextSearchRecord';

export class HypertextSplitter {
    // Lower values are more relevant
    private static elementsRelevance: { [key: string]: number } = {
        h1: 1,
        h2: 2,
        h3: 3,
        h4: 4,
        h5: 4,
        h6: 4,
        p: 5,
        li: 5,
        code: 6
    };

    // This is the cheerio selector
    private readonly $: Function;

    public constructor(htmlDocument: HtmlDocument) {
        this.$ = cheerio.load(htmlDocument.content);
    }

    private extractElementsText(tagName: string): string[] {
        const result: string[] = [];
        const $ = this.$;
        $(tagName).each(function() {
            result.push($(this).text());
        });
        return result;
    }

    public *searchRecords(projectId: number, archiveId: number): Iterable<FullTextSearchRecord> {
        for (const tagName in HypertextSplitter.elementsRelevance) {
            const textChunks = this.extractElementsText(tagName);
            const nonEmptyTextChunks = textChunks.filter(chunk => !chunk.match(/^\s*$/));

            for (const textChunk of nonEmptyTextChunks) {
                const record = new FullTextSearchRecord();
                record.projectId = projectId;
                record.archiveId = archiveId;
                record.corpusRelevance = HypertextSplitter.elementsRelevance[tagName];
                record.element = tagName;
                record.textContent = textChunk;
                yield record;
            }
        }
    }

    public static get relevantElements(): string[] {
        return Object.keys(HypertextSplitter.elementsRelevance);
    }
}
