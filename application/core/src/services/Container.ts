import { Repository } from 'typeorm';
import { Project } from '@core/entities/Project';
import { Commit } from '@core/entities/Commit';
import { Developer } from '@core/entities/Developer';
import { Database } from '@core/services/Database';
import { ProjectService } from '@core/services/ProjectService';
import { CommitService } from '@core/services/CommitService';
import { DeveloperService } from '@core/services/DeveloperService';
import { DocumentationUploadService } from '@core/services/DocumentationUploadService';
import { FileService } from '@core/services/FileService';

/**
 * Implement basic dependency injection. Database repositories are instantiate through the
 * Database helper class, which in turn works by calling typeorm connection pool manager.
 * Obtained repositories are connection-specific and connections to the database should be
 * used and released as soon as possible, ideally on a per-request basis. For this reason,
 * the container dynamically creates new objects on demand.
 */
export class ServiceContainer {
    public async projectRepository(): Promise<Repository<Project>> {
        return Database.repository(Project);
    }

    public async commitRepository(): Promise<Repository<Commit>> {
        return Database.repository(Commit);
    }

    public async developerRepository(): Promise<Repository<Developer>> {
        return Database.repository(Developer);
    }

    public async fileService(): Promise<FileService> {
        return new FileService();
    }

    public async projectService(): Promise<ProjectService> {
        return new ProjectService(await this.projectRepository());
    }

    public async commitService(): Promise<CommitService> {
        return new CommitService(await this.commitRepository());
    }

    public async developerService(): Promise<DeveloperService> {
        return new DeveloperService(await this.developerRepository());
    }

    public async documentationUploadService(): Promise<DocumentationUploadService> {
        return new DocumentationUploadService(
            await Database.entityManager(),
            await this.fileService(),
            await this.commitService()
        );
    }
}

// The container itself is stateless, hence it's safe to cache at the module level
export const mainContainer = new ServiceContainer();
