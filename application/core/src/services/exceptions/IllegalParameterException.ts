export class IllegalParameterException extends Error {
    public constructor(public readonly message: string) {
        super(message);
    }
}
