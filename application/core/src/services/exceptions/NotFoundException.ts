export class NotFoundException extends Error {
    public constructor(public readonly message: string) {
        super(message);
    }
}
