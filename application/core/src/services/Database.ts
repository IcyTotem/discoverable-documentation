import * as path from 'path';
import { createConnection, Connection, getConnection, EntityManager, getManager, Repository } from 'typeorm';

export class Database {
    public static get host(): string {
        return process.env['DB_HOST'];
    }

    public static get port(): number {
        return Number(process.env['DB_PORT']);
    }

    public static get dbName(): string {
        return process.env['DB_NAME'];
    }

    public static get user(): string {
        return process.env['DB_USER'];
    }

    private static get password(): string {
        return process.env['DB_PASSWORD'];
    }

    static async connection(): Promise<Connection> {
        try {
            return getConnection();
        } catch (e) {
            return createConnection({
                type: 'postgres',
                host: Database.host,
                port: Database.port,
                username: Database.user,
                password: Database.password,
                database: Database.dbName,
                entities: [path.resolve(__dirname, '../entities/*.ts')],
                // It is of PARAMOUNT importance that this is set to false, otherwise the postgres
                // schema will be WIPED on startup, including all custom constraints and triggers!
                synchronize: false,
                logging: false
            });
        }
    }

    static async transaction(callback: (em: EntityManager) => Promise<void>): Promise<void> {
        await getManager().transaction(callback);
    }

    static async repository<Entity>(target: { new (): Entity }): Promise<Repository<Entity>> {
        const connection = await Database.connection();
        return connection.getRepository(target);
    }

    static async entityManager(): Promise<EntityManager> {
        return getManager();
    }
}
