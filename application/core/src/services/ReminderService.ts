import { Repository } from 'typeorm';
import { Commit } from '@core/entities/Commit';

export interface ReminderCommitReference {
    hash: string;
    authorName: string;
    authorEmail: string;
    branchName: string;
    creationTime: Date;
}

export class ReminderService {
    public constructor(private readonly commitRepository: Repository<Commit>) {}

    public async remindMissingDocumentation(projectId: number, commitHash: string): Promise<void> {
        await this.commitRepository.update(
            { projectId, hash: commitHash, containsDocumentation: false },
            { triggersReminder: true }
        );
    }

    public async dismiss(projectId: number, commitHash: string): Promise<void> {
        await this.commitRepository.update(
            { projectId, hash: commitHash, triggersReminder: true },
            { triggersReminder: false }
        );
    }

    public async dismissAll(projectId: number): Promise<void> {
        await this.commitRepository.update({ projectId, triggersReminder: true }, { triggersReminder: false });
    }

    public async getActiveReminderCommits(projectId: number): Promise<ReminderCommitReference[]> {
        return this.commitRepository.find({
            select: ['hash', 'authorName', 'authorEmail', 'branchName', 'creationTime'],
            where: {
                projectId,
                containsDocumentation: false,
                triggersReminder: true
            }
        });
    }

    public async acknowledgeMergedBranches(projectId: number, mergedBranches: string[]): Promise<void> {
        if (mergedBranches.length > 0) {
            await this.commitRepository
                .createQueryBuilder()
                .update(Commit)
                .set({ triggersReminder: false })
                .where('project_id = :projectId', { projectId })
                .andWhere('triggers_reminder is true')
                .andWhere('branch_name in (:...mergedBranches)', { mergedBranches })
                .execute();
        }
    }
}
