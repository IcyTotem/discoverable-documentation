// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { Project } from '@core/entities/Project';

declare global {
    namespace Express {
        export interface Request {
            /**
             * Injected by our custom authentication middleware. It refers to the project whose
             * api key is being declared in the api key header for the current request.
             */
            project?: Project;
        }
    }
}
