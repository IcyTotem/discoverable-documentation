import { RequestHandler } from 'express';
import { ServiceContainer } from '@core/services/Container';

export class ApiKeyAuthentication {
    constructor(private readonly container: ServiceContainer) {}

    public middleware(apiKeyHeaderName: string): RequestHandler {
        // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
        return async (req, res, next) => {
            const apiKeyContent = req.headers[apiKeyHeaderName];

            if (!apiKeyContent) {
                return res.sendStatus(403);
            }

            // Accessing "headers" may either return a string or a string array (in case the
            // same header is repeated multiple times). In case we receive multiple headers,
            // the most conservative choice is to drop the request alltogether
            if (apiKeyContent instanceof Array) {
                return res.sendStatus(400);
            }

            try {
                // Notice apiKeyContent must now be a string because the array case was excluded
                // on the previous lines
                const apiKey = apiKeyContent as string;
                const projectService = await this.container.projectService();
                const project = await projectService.findByApiKey(apiKey);
                req.project = project;
                next();
            } catch (e) {
                // Errors raised asynchronously must be passed to the next function
                next(e);
            }
        };
    }
}
