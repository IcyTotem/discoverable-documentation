import * as path from 'path';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as multer from 'multer';
import { mainContainer } from '@core/services/Container';
import { ApiKeyAuthentication } from '@core/api/ingestion/middleware/ApiKeyAuthentication';
import { CommitMeta } from '@core/services/CommitService';
import { DeveloperMeta } from '@core/services/DeveloperService';

const PORT = process.env['INGESTION_API_PORT'] || 7112;
const API_KEY_HEADER = process.env['INGESTION_API_KEY_HEADER'] || 'X-Api-Key';
const INGESTION_UPLOAD_DIR = path.resolve(process.env['INGESTION_UPLOAD_DIR'] || './uploads/');

const upload = multer({ dest: INGESTION_UPLOAD_DIR });
const app = express();
app.use(bodyParser.json());

const authentication = new ApiKeyAuthentication(mainContainer);
app.use(authentication.middleware(API_KEY_HEADER));

app.get('/in/project', async (req, res) => {
    const commitService = await mainContainer.commitService();
    const latestMasterCommitHash = await commitService.getLatestHash(req.project.id, req.project.masterBranch);
    res.json({
        id: req.project.id,
        masterBranch: req.project.masterBranch,
        latestCommit: latestMasterCommitHash,
        ignoreTriggerPhrase: req.project.ignoreTriggerPhrase
    });
});

app.post('/in/developers', async (req, res) => {
    const developerService = await mainContainer.developerService();
    const developerMetas: DeveloperMeta[] = req.body;
    await developerService.add(req.project.id, developerMetas);
    res.status(201).end();
});

app.post('/in/commit', async (req, res) => {
    const commitService = await mainContainer.commitService();
    const commitMeta: CommitMeta = req.body;

    commitMeta.creationTime = new Date(req.body.creationTime); // Dates come in as strings
    commitMeta.diffFromMaster = null; // TODO

    await commitService.create(req.project.id, commitMeta);
    res.status(201).end();
});

app.put('/in/commit/:commitHash/docs', upload.single('data'), async (req, res) => {
    const commitHash = req.params.commitHash;
    const documentationUploadService = await mainContainer.documentationUploadService();
    documentationUploadService.uploadArchive(req.project.id, commitHash, req.file.path);
    res.status(200).end();
});

app.post('/in/commit/:commitHash/docs/deleted', async (req, res) => {
    const commitHash = req.params.commitHash;
    const documentationUploadService = await mainContainer.documentationUploadService();
    const repositoryPaths = req.body.paths;
    for (const repositoryPath of repositoryPaths) {
        documentationUploadService.markAsDeleted(req.project.id, commitHash, repositoryPath);
    }
    res.status(200).end();
});

app.listen(PORT, () => console.log(`Ingestion server listening on port ${PORT}!`));
