import * as showdown from 'showdown';
import { HtmlDocument } from './HtmlDocument';

const converter = new showdown.Converter();

export class MarkdownDocument {
    public constructor(public readonly content: string) {}

    public toHtml(): HtmlDocument {
        return new HtmlDocument(converter.makeHtml(this.content));
    }
}
