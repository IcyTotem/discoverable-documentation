import * as zlib from 'zlib';
import { HtmlDocument } from './HtmlDocument';

export class CompressedHtmlDocument {
    public constructor(public readonly content: Buffer) {}

    public inflate(): HtmlDocument {
        return new HtmlDocument(zlib.inflateSync(this.content).toString());
    }
}
