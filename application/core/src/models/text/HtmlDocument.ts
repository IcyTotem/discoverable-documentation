import * as zlib from 'zlib';
import { CompressedHtmlDocument } from './CompressedHtmlDocument';

export class HtmlDocument {
    public constructor(public readonly content: string) {}

    public deflate(): CompressedHtmlDocument {
        return new CompressedHtmlDocument(zlib.deflateSync(Buffer.from(this.content, 'utf8')));
    }
}
