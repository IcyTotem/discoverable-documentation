import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('projects')
export class Project {
    @PrimaryGeneratedColumn({ type: 'bigint' })
    id: number;

    @Column({ type: 'bigint', name: 'tenant_id', nullable: false })
    tenantId: number;

    @Column({ type: 'bigint', name: 'owner_id', nullable: false })
    ownerId: number;

    @Column({ type: 'text', name: 'name', nullable: false })
    name: string;

    @Column({ type: 'text', name: 'api_key', nullable: false })
    apiKey: string;

    @Column({ type: 'text', name: 'master_branch', nullable: false, default: 'master' })
    masterBranch: string;

    @Column({ type: 'boolean', name: 'email_notifications_enabled', nullable: false, default: true })
    areEmailNotificationsEnabled: boolean;

    @Column({ type: 'text', name: 'ignore_trigger_phrase', nullable: true })
    ignoreTriggerPhrase: string;

    @Column({ type: 'timestamp without time zone', name: 'creation_time', nullable: false })
    creationTime: Date;

    @Column({ type: 'text', name: 'description', nullable: true })
    description?: string;
}
