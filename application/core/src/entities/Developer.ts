import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('developers')
export class Developer {
    @PrimaryGeneratedColumn({ type: 'bigint' })
    id: number;

    @Column({ type: 'bigint', name: 'project_id', nullable: false })
    projectId: number;

    @Column({ type: 'text', name: 'name', nullable: false })
    name: string;

    @Column({ type: 'text', name: 'email', nullable: false })
    email: string;
}
