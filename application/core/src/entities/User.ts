import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('users')
export class User {
    @PrimaryGeneratedColumn({ type: 'bigint' })
    id: number;

    @Column({ type: 'bigint', name: 'tenant_id', nullable: false })
    tenantId: number;

    @Column({ type: 'text', name: 'name', nullable: false })
    name: string;

    @Column({ type: 'text', name: 'email', nullable: false })
    email: string;
}
