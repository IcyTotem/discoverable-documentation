import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('full_text_search_archive')
export class FullTextSearchRecord {
    @PrimaryGeneratedColumn({ type: 'bigint' })
    id: number;

    @Column({ type: 'bigint', name: 'archive_id', nullable: false })
    archiveId: number;

    @Column({ type: 'bigint', name: 'project_id', nullable: false })
    projectId: number;

    @Column({ type: 'text', name: 'element', nullable: false })
    element: string;

    @Column({ type: 'smallint', name: 'corpus_relevance', nullable: false, default: 1 })
    corpusRelevance: number;

    @Column({ type: 'text', name: 'text_content', nullable: false })
    textContent: string;

    @Column({ type: 'timestamp without time zone', name: 'index_timestamp' })
    indexTimestamp: Date;

    @Column({ type: 'boolean', name: 'is_latest', nullable: false, default: true })
    isLatest: boolean;
}
