import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('documentation_archive')
export class DocumentationRecord {
    @PrimaryGeneratedColumn({ type: 'bigint' })
    id: number;

    @Column({ type: 'bigint', name: 'project_id', nullable: false })
    projectId: number;

    @Column({ type: 'text', name: 'repository_path', nullable: false })
    repositoryPath: string;

    @Column({ type: 'text', name: 'commit_hash', nullable: false })
    commitHash: string;

    @Column({ type: 'bytea', name: 'compressed_content', nullable: true })
    compressedContent?: Buffer;

    @Column({ type: 'boolean', name: 'is_latest', nullable: false, default: true })
    isLatest: boolean;

    @Column({ type: 'boolean', name: 'is_deleted', nullable: false, default: false })
    isDeleted: boolean;
}
