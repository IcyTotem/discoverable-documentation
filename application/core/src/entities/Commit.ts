import { Entity, PrimaryColumn, Column } from 'typeorm';

@Entity('commits')
export class Commit {
    @PrimaryColumn({ type: 'text' })
    hash: string;

    @PrimaryColumn({ type: 'bigint', name: 'project_id', nullable: false })
    projectId: number;

    @Column({ type: 'text', name: 'author_name', nullable: false })
    authorName: string;

    @Column({ type: 'text', name: 'author_email', nullable: false })
    authorEmail: string;

    @Column({ type: 'text', name: 'branch_name', nullable: false })
    branchName: string;

    @Column({ type: 'timestamp without time zone', name: 'creation_time', nullable: false })
    creationTime: Date;

    @Column({ type: 'bytea', name: 'compressed_master_diff', nullable: true })
    compressedDiffFromMaster: Buffer | null;

    @Column({ type: 'boolean', name: 'contains_documentation', nullable: false, default: true })
    containsDocumentation: boolean;

    @Column({ type: 'boolean', name: 'triggers_reminder', nullable: false, default: false })
    triggersReminder: boolean;
}
