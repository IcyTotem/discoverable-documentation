mkdir /flyway && cd /flyway
export FLYWAY_VERSION=6.0.1

curl -L https://repo1.maven.org/maven2/org/flywaydb/flyway-commandline/${FLYWAY_VERSION}/flyway-commandline-${FLYWAY_VERSION}.tar.gz -o flyway-commandline-${FLYWAY_VERSION}.tar.gz \
  && tar -xzf flyway-commandline-${FLYWAY_VERSION}.tar.gz --strip-components=1 \
  && rm flyway-commandline-${FLYWAY_VERSION}.tar.gz

export FLYWAY_EDITION=community
export FLYWAY_URL=jdbc:postgresql://localhost:5432/pipelines_test_userdata
export FLYWAY_USER=test_coreactor
export FLYWAY_PASSWORD="$CI_POSTGRES_PASSWORD"
