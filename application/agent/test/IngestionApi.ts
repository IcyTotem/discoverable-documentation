import * as path from 'path';
import * as fs from 'fs';
import { Server } from 'http';
// How to have * as express and single type/object imports at the same time?
// eslint-disable-next-line import/no-duplicates
import * as express from 'express';
import * as multer from 'multer';
import * as bodyParser from 'body-parser';
import * as faker from 'faker';
// eslint-disable-next-line import/no-duplicates
import { Express, Request, Response } from 'express';
import { assert } from 'chai';
import { describe, it, before, after } from 'mocha';
import { IngestionApi, ProjectManifest, CommitCreationMeta, DeveloperMeta } from '../src/IngestionApi';
import { repeat, randomFile } from './tools/utils';

// This should be Request<ParamsDictionary>, where ParamsDictionary is a type defined in
// express-serve-static-core. However, if I import that module, eslint complains it does not
// find the type definition, even after installing @types/express-serve-static-core.
// So have to resort to any
// eslint-disable-next-line @typescript-eslint/no-explicit-any
type EndpointCallback = (req: Request<any>, res: Response) => void;

const UPLOAD_DIR = path.resolve('./uploads');
const API_KEY_HEADER = 'X-Api-Key';
const PORT = 3033;
const TEST_SERVER_URL = `http://localhost:${PORT}`;

describe('IngestionApi', () => {
    let apiKey: string;
    let ingestionApi: IngestionApi;
    let app: Express;
    let server: Server;
    let handleGetProject: EndpointCallback;
    let handlePostCommit: EndpointCallback;
    let handlePostDevelopers: EndpointCallback;
    let handlePutArchive: EndpointCallback;
    let handleDeleteDocuments: EndpointCallback;

    before(async () => {
        app = express();
        app.use(bodyParser.json());
        app.use(bodyParser.raw({ type: 'application/octet-stream' }));

        const upload = multer({ dest: UPLOAD_DIR });

        app.get('/in/project', (req, res) => handleGetProject(req, res));
        app.post('/in/commit', (req, res) => handlePostCommit(req, res));
        app.post('/in/developers', (req, res) => handlePostDevelopers(req, res));
        app.put('/in/commit/:commit/docs', upload.single('data'), (req, res) => handlePutArchive(req, res));
        app.post('/in/commit/:commit/docs/deleted', (req, res) => handleDeleteDocuments(req, res));

        server = app.listen(PORT, () => {});
        apiKey = faker.random.uuid();
        ingestionApi = new IngestionApi(TEST_SERVER_URL, apiKey);
    });

    after(() => {
        server.close();
        fs.readdirSync(UPLOAD_DIR).forEach(file => fs.unlinkSync(path.join(UPLOAD_DIR, file)));
        fs.rmdirSync(UPLOAD_DIR);
    });

    describe('#fetchProjectManifest', () => {
        it('correctly fetches the project manifest from the core server', async () => {
            let actualApiKey: string;
            let expectedProjectManifest: ProjectManifest;

            // Set up the behaviour of the mocked test server
            handleGetProject = (req, res): void => {
                actualApiKey = req.header(API_KEY_HEADER);
                expectedProjectManifest = {
                    id: faker.random.uuid(),
                    masterBranch: faker.random.word(),
                    latestCommit: faker.random.uuid(),
                    ignoreTriggerPhrase: faker.lorem.sentence()
                };
                res.json(expectedProjectManifest);
            };

            // Call the api, ending up in the previously defined handler
            const projectManifest = await ingestionApi.fetchProjectManifest();

            assert.equal(actualApiKey, apiKey);
            assert.deepEqual(projectManifest, expectedProjectManifest);
        });
    });

    describe('#createCommit', () => {
        it('correctly requests the creation of a new commit', async () => {
            let actualApiKey: string;
            let actualCommit: CommitCreationMeta;
            const expectedCommit: CommitCreationMeta = {
                hash: faker.random.uuid(),
                authorName: faker.name.firstName(),
                authorEmail: faker.internet.email(),
                branchName: faker.random.word(),
                creationTime: faker.date.past(),
                containsDocumentation: faker.random.boolean(),
                triggersReminder: faker.random.boolean()
            };

            handlePostCommit = (req, res): void => {
                actualApiKey = req.header(API_KEY_HEADER);
                actualCommit = req.body;
                res.status(201).send();
            };

            await ingestionApi.createCommit(expectedCommit);

            // Before comparing, make sure that both creationDate fields contain the
            // same data type so the deepEqual won't fail. This is due to json serialization
            actualCommit.creationTime = new Date(actualCommit.creationTime);

            assert.equal(actualApiKey, apiKey);
            assert.deepEqual(actualCommit, expectedCommit);
        });
    });

    describe('#registerDevelopers', () => {
        it('correctly requests the registration of some (potentially) new developers', async () => {
            let actualApiKey: string;
            let actualDevelopers: DeveloperMeta[];
            const expectedDevelopers: DeveloperMeta[] = repeat(faker.random.number({ min: 0, max: 10 }), () => ({
                email: faker.internet.email(),
                name: faker.name.firstName()
            }));

            handlePostDevelopers = (req, res): void => {
                actualApiKey = req.header(API_KEY_HEADER);
                actualDevelopers = req.body.developers;
                res.status(201).send();
            };

            await ingestionApi.registerDevelopers(expectedDevelopers);

            assert.equal(actualApiKey, apiKey);
            assert.deepEqual(actualDevelopers, expectedDevelopers);
        });

        it('does nothing if the list of provided developers is empty', async () => {
            let endpointCalled = false;

            handlePostDevelopers = (req, res): void => {
                endpointCalled = true;
                res.status(201).send();
            };

            await ingestionApi.registerDevelopers([]);
            assert.isFalse(endpointCalled);

            await ingestionApi.registerDevelopers(null);
            assert.isFalse(endpointCalled);
        });
    });

    describe('#uploadDocumentsArchive', () => {
        it('correctly uploads a document archive', async () => {
            let actualApiKey: string;
            let actualCommitHash: string;
            let actualFilePath: string;

            handlePutArchive = (req, res): void => {
                // Multer automatically handles file upload and saves the result in this location
                actualFilePath = req.file.path;
                actualCommitHash = req.params.commit;
                actualApiKey = req.header(API_KEY_HEADER);
                res.status(200).send();
            };

            // Create a random lorem ipsum file. It does not matter what is in the file or
            // its extension. We only want to check the contents are correctly uploaded
            const filePath = randomFile();
            const commitHash = faker.random.uuid();

            await ingestionApi.uploadDocumentsArchive(commitHash, filePath);

            assert.equal(actualApiKey, apiKey);
            assert.equal(actualCommitHash, commitHash);
            assert.equal(
                fs.readFileSync(actualFilePath, { encoding: 'utf-8' }),
                fs.readFileSync(filePath, { encoding: 'utf-8' })
            );
        });

        it('does nothing if the commit hash is missing or the target archive does not exist', async () => {
            let endpointCalled = false;

            handlePutArchive = (req, res): void => {
                endpointCalled = true;
                res.status(200).send();
            };

            const filePath = randomFile();

            // File exists but commit is empty
            await ingestionApi.uploadDocumentsArchive('', filePath);
            assert.isFalse(endpointCalled);

            // Commit is not empty but the file does not exist
            const missingFilePath = path.resolve(UPLOAD_DIR, faker.random.uuid());
            await ingestionApi.uploadDocumentsArchive(faker.random.uuid(), missingFilePath);
            assert.isFalse(endpointCalled);
        });
    });

    describe('#markDocumentsAsDeleted', () => {
        it('correctly requests that documents are marked as deleted', async () => {
            let actualApiKey: string;
            let actualDocumentsPaths: string[];
            const commitHash = faker.random.uuid();
            const expectedDocumentsPaths = repeat(faker.random.number({ min: 0, max: 10 }), () => faker.random.word());

            handleDeleteDocuments = (req, res): void => {
                actualApiKey = req.header(API_KEY_HEADER);
                actualDocumentsPaths = req.body.paths;
                res.status(200).send();
            };

            await ingestionApi.markDocumentsAsDeleted(commitHash, expectedDocumentsPaths);

            assert.equal(actualApiKey, apiKey);
            assert.deepEqual(actualDocumentsPaths, expectedDocumentsPaths);
        });

        it('does nothing if the commit hash is missing or the list of document paths is empty', async () => {
            let endpointCalled = false;

            handleDeleteDocuments = (req, res): void => {
                endpointCalled = true;
                res.status(200).send();
            };

            await ingestionApi.markDocumentsAsDeleted('', ['okpath']);
            assert.isFalse(endpointCalled);

            await ingestionApi.markDocumentsAsDeleted('okcommit', []);
            assert.isFalse(endpointCalled);
        });
    });
});
