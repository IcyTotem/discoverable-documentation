import * as faker from 'faker';
import { mock, verify, when, instance } from 'ts-mockito';
import { describe, it, beforeEach } from 'mocha';
import { assert } from 'chai';
import { ShellWrapper, GitWrapper } from '../src/GitWrapper';
import { randomFilePaths, repeat } from './tools/utils';

// ts-mockito cannot mock interfaces, so we have to mock a mock object that
// implements the interface we want to mock. Intuitive, uh?
class EmptyShellWrapper {
    exec(): Promise<string> {
        return Promise.resolve('');
    }
}

describe('GitWrapper', () => {
    let MockShellWrapper: ShellWrapper;
    let gitWrapper: GitWrapper;

    beforeEach(() => {
        MockShellWrapper = mock(EmptyShellWrapper);
        gitWrapper = new GitWrapper(instance(MockShellWrapper));
    });

    describe('#show', () => {
        it('should invoke git show witht the given format', async () => {
            when(MockShellWrapper.exec("git show -s --format='%anyformat' HEAD")).thenResolve('result');

            const output = await gitWrapper.show('%anyformat');
            assert.equal(output, 'result');

            verify(MockShellWrapper.exec("git show -s --format='%anyformat' HEAD")).once();
        });
    });

    describe('#findChangedFiles', () => {
        it('should call git diff and return a list of all files included in the diff', async () => {
            const referenceCommit = faker.random.uuid();
            const gitDiffOutput = `
                M    modified/file.md
                A    added/file.md
                R80  former/file/name.md  new/file name with spaces.md
                C100 copied/file_1.md     copied/file2.md
                D    deleted/former-file.md`;

            when(MockShellWrapper.exec(`git diff --name-only ${referenceCommit} HEAD`)).thenResolve(gitDiffOutput);

            const actualFiles = await gitWrapper.findChangedFiles(referenceCommit);
            assert.deepEqual(actualFiles, [
                { path: 'modified/file.md', deleted: false },
                { path: 'added/file.md', deleted: false },
                { path: 'former/file/name.md', deleted: true },
                { path: 'new/file name with spaces.md', deleted: false },
                { path: 'copied/file2.md', deleted: false },
                { path: 'deleted/former-file.md', deleted: true }
            ]);

            verify(MockShellWrapper.exec(`git diff --name-only ${referenceCommit} HEAD`)).once();
        });

        it('should return an empty list if git diff outputs nothing', async () => {
            const referenceCommit = faker.random.uuid();
            when(MockShellWrapper.exec(`git diff --name-only ${referenceCommit} HEAD`)).thenResolve('\n\n');

            const actualFiles = await gitWrapper.findChangedFiles(referenceCommit);
            assert.isEmpty(actualFiles);

            verify(MockShellWrapper.exec(`git diff --name-only ${referenceCommit} HEAD`)).once();
        });
    });

    describe('#findAllFilesWithExtension', () => {
        it('should call find and return a list of all matched files', async () => {
            const expectedFiles = randomFilePaths(false);
            const findOutput = expectedFiles.join('\n');

            when(MockShellWrapper.exec('find . -name "*.md"')).thenResolve(findOutput);

            const actualFiles = await gitWrapper.findAllFilesWithExtension('.md');
            assert.deepEqual(actualFiles, expectedFiles);

            verify(MockShellWrapper.exec('find . -name "*.md"')).once();
        });

        it('should return an empty list if find outputs nothing', async () => {
            when(MockShellWrapper.exec('find . -name "*.md"')).thenResolve('\n\n');

            const actualFiles = await gitWrapper.findAllFilesWithExtension('.md');
            assert.isEmpty(actualFiles);

            verify(MockShellWrapper.exec('find . -name "*.md"')).once();
        });
    });

    describe('#getAllDevelopers', () => {
        it('should call git log and collect all commit authors', async () => {
            const expectedDevelopers = repeat(10, () => ({
                name: `${faker.name.firstName()} ${faker.name.lastName()}`,
                email: faker.internet.email()
            }));
            const gitLogOutput = expectedDevelopers.map(d => `${d.name} <${d.email}>`).join('\n  \n  ');

            when(MockShellWrapper.exec('git log --pretty="%an <%ae>%n" | sort | uniq')).thenResolve(gitLogOutput);

            const actualDevelopers = await gitWrapper.getAllDevelopers();
            assert.deepEqual(actualDevelopers, expectedDevelopers);

            verify(MockShellWrapper.exec('git log --pretty="%an <%ae>%n" | sort | uniq')).once();
        });

        it('uses the reference commit when provided', async () => {
            const referenceCommit = faker.random.uuid();
            await gitWrapper.getAllDevelopers(referenceCommit);
            verify(
                MockShellWrapper.exec(`git log --pretty="%an <%ae>%n" ${referenceCommit}..HEAD | sort | uniq`)
            ).once();
        });
    });
});
