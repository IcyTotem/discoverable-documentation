import * as path from 'path';
import * as fs from 'fs';
import * as faker from 'faker';
import { assert } from 'chai';
import { mock, verify, when, anything, instance, deepEqual, strictEqual } from 'ts-mockito';
import { describe, it, beforeEach } from 'mocha';
import { IngestionApi, ProjectManifest, CommitCreationMeta } from '../src/IngestionApi';
import { GitWrapper, ChangedFile } from '../src/GitWrapper';
import { Zipper } from '../src/Zipper';
import { Agent } from '../src/Agent';
import { repeat, randomFile } from './tools/utils';

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const generateChangedFiles = (wantsDeleted: boolean) => {
    const randomMarkdown = (path: string): string => (Math.random() < 0.5 ? `${path}.md` : path);

    const correctDocumentPaths = repeat(5, () => `${faker.internet.url()}.md`);
    const otherFilePaths = repeat(15, () => faker.internet.url()).map(randomMarkdown);

    const correctChangedFiles: ChangedFile[] = correctDocumentPaths.map(path => ({ path, deleted: wantsDeleted }));
    const otherChangedFiles: ChangedFile[] = otherFilePaths.map(path => ({ path, deleted: !wantsDeleted }));

    return {
        correctDocumentPaths,
        otherFilePaths,
        otherDocumentPaths: otherFilePaths.filter(path => path.endsWith('.md')),
        correctChangedFiles,
        otherChangedFiles,
        allFilePaths: [...correctDocumentPaths, ...otherFilePaths],
        allChangedFiles: [...correctChangedFiles, ...otherChangedFiles]
    };
};

describe('Agent', () => {
    let MockIngestionApi: IngestionApi;
    let MockGitWrapper: GitWrapper;
    let MockZipper: Zipper;
    let agent: Agent;

    const currentBranchName = faker.random.word();
    const masterBranch = faker.random.word();
    const currentCommitHash = faker.random.uuid();
    const authorName = `${faker.name.firstName()} ${faker.name.lastName()}`;
    const authorEmail = faker.internet.email();
    const creationTime = faker.date.past();

    beforeEach(() => {
        MockIngestionApi = mock(IngestionApi);
        MockGitWrapper = mock(GitWrapper);
        MockZipper = mock(Zipper);

        when(MockGitWrapper.currentBranchName()).thenResolve(currentBranchName);
        when(MockGitWrapper.currentCommitHash()).thenResolve(currentCommitHash);
        when(MockGitWrapper.show('%an')).thenResolve(authorName);
        when(MockGitWrapper.show('%ae')).thenResolve(authorEmail);
        when(MockGitWrapper.show('%aI')).thenResolve(creationTime.toISOString());

        agent = new Agent(instance(MockIngestionApi), instance(MockGitWrapper), instance(MockZipper));
    });

    it('should upload ALL documentation files when running on master for the first time', async () => {
        const allDocumentationFiles = repeat(10, () => faker.internet.url());
        const allDevelopers = repeat(10, () => ({ email: faker.internet.email(), name: faker.name.firstName() }));
        const projectManifest: ProjectManifest = {
            id: faker.random.uuid(),
            latestCommit: null,
            masterBranch
        };

        when(MockGitWrapper.currentBranchName()).thenResolve(masterBranch);
        when(MockGitWrapper.findAllFilesWithExtension('.md')).thenResolve(allDocumentationFiles);
        when(MockGitWrapper.getAllDevelopers(null)).thenResolve(allDevelopers);
        when(MockIngestionApi.fetchProjectManifest()).thenResolve(projectManifest);

        await agent.run();

        const expectedArchivePath = path.resolve(`${currentCommitHash}.zip`);
        const expectedCreatedCommit: CommitCreationMeta = {
            hash: currentCommitHash,
            authorName,
            authorEmail,
            creationTime,
            branchName: masterBranch,
            triggersReminder: false,
            containsDocumentation: true
        };

        verify(MockIngestionApi.registerDevelopers(deepEqual(allDevelopers))).once();
        verify(MockIngestionApi.createCommit(deepEqual(expectedCreatedCommit))).once();
        verify(MockZipper.zip(deepEqual(allDocumentationFiles), expectedArchivePath)).once();
        verify(MockIngestionApi.uploadDocumentsArchive(currentCommitHash, expectedArchivePath)).once();
    });

    it('should only upload changed documents and marking deleted documents when running on master after setup', async () => {
        const diffDevelopers = repeat(2, () => ({ email: faker.internet.email(), name: faker.name.firstName() }));
        const projectManifest: ProjectManifest = {
            id: faker.random.uuid(),
            latestCommit: faker.random.uuid(),
            masterBranch
        };

        const { allChangedFiles, correctDocumentPaths, otherDocumentPaths } = generateChangedFiles(false);

        when(MockGitWrapper.currentBranchName()).thenResolve(masterBranch);
        when(MockGitWrapper.findChangedFiles(projectManifest.latestCommit)).thenResolve(allChangedFiles);
        when(MockGitWrapper.getAllDevelopers(projectManifest.latestCommit)).thenResolve(diffDevelopers);
        when(MockIngestionApi.fetchProjectManifest()).thenResolve(projectManifest);

        // Fake the creation of a file, so that we can check there is proper cleanup code
        when(MockZipper.zip(anything(), anything())).thenCall(async (files, archivePath) => randomFile(archivePath));

        // The actual implementation does not throw an exception when the file is missing.
        // The correct approach would be to verify that the file deletion method is called only
        // after the upload. However at the moment we are not able to mock native modules,
        // and it feels like useless pollution to create a fs service to mock. Therefore
        // we have to fall back to making the method fail if the file does not exist.
        when(MockIngestionApi.uploadDocumentsArchive(anything(), anything())).thenCall(
            async (commitHash, archivePath) => {
                if (!fs.existsSync(archivePath)) {
                    throw new Error('File does not exist');
                }
            }
        );

        await agent.run();

        const expectedArchivePath = path.resolve(`${currentCommitHash}.zip`);
        const expectedCreatedCommit: CommitCreationMeta = {
            hash: currentCommitHash,
            authorName,
            authorEmail,
            creationTime,
            branchName: masterBranch,
            triggersReminder: false,
            containsDocumentation: true
        };

        verify(MockIngestionApi.registerDevelopers(deepEqual(diffDevelopers))).once();
        verify(MockIngestionApi.createCommit(deepEqual(expectedCreatedCommit))).once();
        verify(MockZipper.zip(deepEqual(correctDocumentPaths), strictEqual(expectedArchivePath))).once();
        verify(MockIngestionApi.uploadDocumentsArchive(currentCommitHash, expectedArchivePath)).once();
        verify(MockIngestionApi.markDocumentsAsDeleted(currentCommitHash, deepEqual(otherDocumentPaths))).once();

        // Check that the archive has been correctly deleted after upload
        assert.isFalse(fs.existsSync(expectedArchivePath));
    });

    it('should do nothing when finding some documents on a feature branch', async () => {
        const { allChangedFiles } = generateChangedFiles(false);
        const projectManifest: ProjectManifest = {
            id: faker.random.uuid(),
            latestCommit: faker.random.uuid(),
            masterBranch
        };

        when(MockIngestionApi.fetchProjectManifest()).thenResolve(projectManifest);
        when(MockGitWrapper.findChangedFiles(masterBranch)).thenResolve(allChangedFiles);

        await agent.run();

        verify(MockIngestionApi.createCommit(anything())).never();
        verify(MockIngestionApi.uploadDocumentsArchive(anything(), anything())).never();
    });

    it('should do nothing when finding no documents on a feature branch but the latest commit contains the ignore phrase', async () => {
        const { correctChangedFiles } = generateChangedFiles(true);
        const projectManifest: ProjectManifest = {
            id: faker.random.uuid(),
            latestCommit: faker.random.uuid(),
            ignoreTriggerPhrase: faker.random.words(),
            masterBranch
        };

        when(MockIngestionApi.fetchProjectManifest()).thenResolve(projectManifest);
        when(MockGitWrapper.findChangedFiles(masterBranch)).thenResolve(correctChangedFiles);
        when(MockGitWrapper.show('%s %b')).thenResolve(`something ${projectManifest.ignoreTriggerPhrase} something`);

        await agent.run();

        verify(MockGitWrapper.show('%s %b')).once();
        verify(MockIngestionApi.createCommit(anything())).never();
        verify(MockIngestionApi.uploadDocumentsArchive(anything(), anything())).never();
    });

    it('should send a request to create a reminder commit when finding no documents on a feature branch', async () => {
        const { correctChangedFiles } = generateChangedFiles(true);
        const projectManifest: ProjectManifest = {
            id: faker.random.uuid(),
            latestCommit: faker.random.uuid(),
            ignoreTriggerPhrase: faker.random.words(),
            masterBranch
        };

        when(MockIngestionApi.fetchProjectManifest()).thenResolve(projectManifest);
        when(MockGitWrapper.findChangedFiles(masterBranch)).thenResolve(correctChangedFiles);
        when(MockGitWrapper.show('%s %b')).thenResolve(faker.lorem.sentence());

        try {
            await agent.run();
            assert.fail();
        } catch (e) {
            assert.equal(e.message, 'No updated documentation found on the current branch');
        }

        const expectedCreatedCommit: CommitCreationMeta = {
            hash: currentCommitHash,
            authorName,
            authorEmail,
            creationTime,
            branchName: masterBranch,
            triggersReminder: true,
            containsDocumentation: false
        };

        verify(MockGitWrapper.show('%s %b')).once();
        verify(MockIngestionApi.uploadDocumentsArchive(anything(), anything())).never();
        verify(MockIngestionApi.createCommit(deepEqual(expectedCreatedCommit))).once();
    });
});
