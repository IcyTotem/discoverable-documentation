import * as fs from 'fs';
import * as path from 'path';
import * as faker from 'faker';
import * as unzipper from 'unzipper';
import { assert } from 'chai';
import { it, describe, before, after } from 'mocha';
import { Zipper } from '../src/Zipper';
import { repeat, randomFile } from './tools/utils';

async function extract(archivePath: string, destinationDirectory: string): Promise<void> {
    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    return new Promise((resolve, reject) => {
        fs.createReadStream(archivePath)
            .pipe(unzipper.Extract({ path: destinationDirectory }))
            .on('error', reject)
            .on('close', resolve);
    });
}

function deleteDirectory(directoryPath: string): void {
    fs.readdirSync(directoryPath).forEach(file => fs.unlinkSync(path.join(directoryPath, file)));
    fs.rmdirSync(directoryPath);
}

/**
 * Create a directory object that maps a file name to its original path.
 * @param files List of relative or absolute file paths.
 */
function filesToMap(files: string[]): { [key: string]: string } {
    const result = {};
    files.forEach(file => (result[path.basename(file)] = file));
    return result;
}

describe('Zipper', () => {
    let testDirectory: string;

    before(() => {
        testDirectory = path.resolve(faker.random.uuid());
        fs.mkdirSync(testDirectory);
    });

    after(() => deleteDirectory(testDirectory));

    it('creates a zip archive out of the provided files', async () => {
        // Create random files with random content
        const n = faker.random.number({ min: 3, max: 10 });
        const files = repeat(n, () => randomFile(path.join(testDirectory, faker.random.uuid())));

        // Zip all the files into a single archive
        const zipper = new Zipper();
        const archivePath = path.join(testDirectory, faker.random.uuid());
        await zipper.zip(files, archivePath);

        // Extract the zip (using a different library) and check the contents are the same
        const destinationDirectory = path.join(testDirectory, faker.random.uuid());
        fs.mkdirSync(destinationDirectory);

        await extract(archivePath, destinationDirectory);

        const originalFilesMap = filesToMap(files);
        const extractedFilesMap = filesToMap(
            fs.readdirSync(destinationDirectory).map(file => path.join(destinationDirectory, file))
        );

        // Extracted files have the same name as original files, and there is no
        // extra file in the input nor in the output
        assert.hasAllKeys(extractedFilesMap, Object.keys(originalFilesMap));

        // Files with the same name have the same content before and after compression
        for (const baseName in originalFilesMap) {
            assert.equal(
                fs.readFileSync(originalFilesMap[baseName], { encoding: 'utf-8' }),
                fs.readFileSync(extractedFilesMap[baseName], { encoding: 'utf-8' })
            );
        }

        deleteDirectory(destinationDirectory);
    });
});
