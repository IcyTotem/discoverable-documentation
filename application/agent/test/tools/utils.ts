import * as path from 'path';
import * as fs from 'fs';
import * as faker from 'faker';

export function randomFilePath(absolute: boolean): string {
    const n = faker.random.number({ min: 2, max: 8 });
    const chunks: string[] = [];

    for (let i = 0; i < n; ++i) {
        chunks.push(faker.random.word());
    }

    const joinedChunks = path.join(...chunks);
    return absolute ? path.resolve('/', joinedChunks) : joinedChunks;
}

export function randomFilePaths(absolute?: boolean): string[] {
    const n = faker.random.number({ min: 1, max: 12 });
    const results: string[] = [];

    for (let i = 0; i < n; ++i) {
        const actualIsAbsolute = typeof absolute === 'boolean' ? absolute : faker.random.boolean();
        results.push(randomFilePath(actualIsAbsolute));
    }

    return results;
}

export function randomFile(givenFilePath?: string): string {
    const usedFilePath = givenFilePath || path.resolve(faker.random.uuid());
    fs.writeFileSync(usedFilePath, faker.lorem.paragraphs(5));
    return usedFilePath;
}

export function repeat<T>(n: number, f: () => T): T[] {
    const result: T[] = [];
    for (let i = 0; i < n; ++i) {
        result.push(f());
    }
    return result;
}

export function randomItem<T>(array: T[]): T {
    return array[faker.random.number({ min: 0, max: array.length - 1 })];
}
