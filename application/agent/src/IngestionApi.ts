import { resolve as resolveUrl } from 'url';
import { createReadStream, existsSync } from 'fs';
import * as request from 'request-promise-native';

const API_KEY_HEADER = 'X-Api-Key';

export interface ProjectManifest {
    /**
     * Id of the project.
     */
    id: string;

    /**
     * Name of the branch designated as master (usually just "master").
     */
    masterBranch: string;

    /**
     * Hash of the latest commit registered on the master branch
     * as far as the core database is concerned. It can be null if the agent is being executed
     * for the first time on a new project/repository.
     */
    latestCommit?: string;

    /**
     * An arbitrary user-defined text that, whenever present in the
     * latest commit message, will ignore any trigger of missing documentation that would be otherwise raised.
     * The empty string and null will be ignored.
     */
    ignoreTriggerPhrase?: string;
}

export interface CommitCreationMeta {
    hash: string;
    authorName: string;
    authorEmail: string;
    branchName: string;
    triggersReminder: boolean;
    containsDocumentation: boolean;
    creationTime: Date;
}

export interface DeveloperMeta {
    email: string;
    name: string;
}

export class IngestionApi {
    constructor(private readonly remoteUrl: string, private readonly apiKey: string) {}

    /**
     * Request information about the current project.
     * @returns The result is used to decide what strategy to adopt in the agent and what to
     *     return to the caling terminal.
     */
    public async fetchProjectManifest(): Promise<ProjectManifest> {
        return request({
            uri: resolveUrl(this.remoteUrl, '/in/project'),
            headers: { [API_KEY_HEADER]: this.apiKey },
            json: true
        });
    }

    /**
     * Request the creation of a new commit with the given properties. The commit entity is
     * used to keep track of various points in time at which documentation was changed, and
     * it functions as red flag for reminders.
     * @param meta The commit being created should refer to the current commit in the analyzed repository.
     */
    public async createCommit(meta: CommitCreationMeta): Promise<void> {
        return request({
            method: 'POST',
            uri: resolveUrl(this.remoteUrl, '/in/commit'),
            headers: { [API_KEY_HEADER]: this.apiKey },
            json: true,
            body: meta
        });
    }

    /**
     * Request the server to append any new developers in the provided array to the list of people
     * working on the current project. The list is used to determine whom to notify when documentation
     * is updated.
     * @param metas Name and e-mail are both required, although the most important is e-mail, which works
     *     as a unique developer identifier.
     */
    public async registerDevelopers(metas: DeveloperMeta[]): Promise<void> {
        if (metas && metas.length > 0) {
            return request({
                method: 'POST',
                uri: resolveUrl(this.remoteUrl, '/in/developers'),
                headers: { [API_KEY_HEADER]: this.apiKey },
                json: true,
                body: { developers: metas }
            });
        }
    }

    /**
     * Upload a zip archive containing all changed documents, arranged according to the same tree structure
     * in which they appear in the current repository.
     * @param commitHash Hash of the commit to which the changes refer to. Needed to build the correct request url.
     * @param archivePath A (preferably absolute) path to a zip file containing all changed documents. This method
     *     does not check the extension or contents of the archive; however, it will check whether the file exists
     *     and simply refrain from issuing any request if not so.
     */
    public async uploadDocumentsArchive(commitHash: string, archivePath: string): Promise<void> {
        if (commitHash && existsSync(archivePath)) {
            return request({
                method: 'PUT',
                uri: resolveUrl(this.remoteUrl, `/in/commit/${commitHash}/docs`),
                headers: { [API_KEY_HEADER]: this.apiKey },
                formData: { data: createReadStream(archivePath) }
            });
        }
    }

    /**
     * Notify the server that some documents that previously existed have been deleted during the given commit.
     * The server should take care to update the representation of currently available files and only provide
     * access to the deleted ones when searchin through the repository history.
     * @param commitHash Hash of the commit to which the changes refer to. Needed to build the correct request url.
     * @param deletedDocumentsPaths List of relative paths (to the repository root) of documents that have been deleted.
     */
    public async markDocumentsAsDeleted(commitHash: string, deletedDocumentsPaths: string[]): Promise<void> {
        if (commitHash && deletedDocumentsPaths.length > 0) {
            return request({
                method: 'POST',
                uri: resolveUrl(this.remoteUrl, `/in/commit/${commitHash}/docs/deleted`),
                headers: { [API_KEY_HEADER]: this.apiKey },
                json: true,
                body: { paths: deletedDocumentsPaths }
            });
        }
    }
}
