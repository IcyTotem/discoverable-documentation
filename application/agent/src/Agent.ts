import * as path from 'path';
import * as fs from 'fs';
import { IngestionApi, ProjectManifest, CommitCreationMeta } from './IngestionApi';
import { GitWrapper, ChangedFile } from './GitWrapper';
import { Zipper } from './Zipper';

const DOCUMENT_EXTENSION = '.md';

export class Agent {
    private projectManifest: ProjectManifest;

    public constructor(
        private readonly ingestionApi: IngestionApi,
        private readonly gitWrapper: GitWrapper,
        private readonly zipper: Zipper
    ) {}

    /**
     * Silently run the agent. See the design document to know its exact behaviour.
     * @throws The promise is rejected and an Error is thrown if and only if we are currently
     *     on a feature branch without changed documentation and without ignore phrase in the
     *     latest commit message. This should be caught to make the process return a non-zero
     *     exit code.
     */
    public async run(): Promise<void> {
        const currentBranchName = await this.gitWrapper.currentBranchName();
        this.projectManifest = await this.ingestionApi.fetchProjectManifest();

        if (this.projectManifest.masterBranch === currentBranchName) {
            return this.runMasterMode();
        } else {
            return this.runFeatureBranchMode();
        }
    }

    private async runMasterMode(): Promise<void> {
        // The list of all developers is updated every time. The core server should properly
        // handle (ignore) any duplicate. Notice that if latestCommit is missing, all developers
        // will be returned, and this is intended
        const developers = await this.gitWrapper.getAllDevelopers(this.projectManifest.latestCommit);
        await this.ingestionApi.registerDevelopers(developers);

        const currentCommitHash = await this.gitWrapper.currentCommitHash();
        await this.sendCreateCommitRequest(false, true);

        const documentationFiles = await this.findDocumentsOnMaster();
        const archivePath = path.resolve(`${currentCommitHash}.zip`);

        const uploadableFilePaths = documentationFiles
            .filter(changedFile => !changedFile.deleted)
            .map(changedFile => changedFile.path);

        await this.zipper.zip(uploadableFilePaths, archivePath);
        await this.ingestionApi.uploadDocumentsArchive(currentCommitHash, archivePath);

        const deletedFilePaths = documentationFiles
            .filter(changedFile => changedFile.deleted)
            .map(changedFile => changedFile.path);

        await this.ingestionApi.markDocumentsAsDeleted(currentCommitHash, deletedFilePaths);

        // Dispose of the temporary archive after uploading
        if (fs.existsSync(archivePath)) {
            fs.unlinkSync(archivePath);
        }
    }

    private async runFeatureBranchMode(): Promise<void> {
        const documentationFiles = await this.findDocumentsOnFeatureBranch();
        const relevantChangedFiles = documentationFiles.filter(changedFile => !changedFile.deleted);

        if (relevantChangedFiles.length > 0) {
            // If there are documents present, the feature branch is considered good
            // and we don't have anything to notify
            return;
        }

        // The project has a magic phrase that ignores notifications, thus we should check for it
        if (this.projectManifest.ignoreTriggerPhrase) {
            const latestCommitMessage = await this.gitWrapper.show('%s %b');
            if (latestCommitMessage.includes(this.projectManifest.ignoreTriggerPhrase)) {
                // The commit contains the magic phrase, so notifications are disabled.
                // Regardless of the contents of this branch, we end here
                return;
            }
        }

        // No documentation found! Create a commit entry that will trigger a reminder
        await this.sendCreateCommitRequest(true, false);

        // In this case, the agent is expected to fail. The process should return a non-zero
        // value to signal a CI that the step has failed
        throw new Error('No updated documentation found on the current branch');
    }

    private async findDocumentsOnMaster(): Promise<ChangedFile[]> {
        if (this.projectManifest.latestCommit) {
            const changedFiles = await this.gitWrapper.findChangedFiles(this.projectManifest.latestCommit);
            return changedFiles.filter(changedFile => changedFile.path.endsWith(DOCUMENT_EXTENSION));
        } else {
            const allFilePaths = await this.gitWrapper.findAllFilesWithExtension(DOCUMENT_EXTENSION);
            return allFilePaths.map(path => ({ path, deleted: false }));
        }
    }

    private async findDocumentsOnFeatureBranch(): Promise<ChangedFile[]> {
        const changedFiles = await this.gitWrapper.findChangedFiles(this.projectManifest.masterBranch);
        return changedFiles.filter(changedFile => changedFile.path.endsWith(DOCUMENT_EXTENSION));
    }

    private async sendCreateCommitRequest(triggersReminder: boolean, containsDocumentation: boolean): Promise<void> {
        const currentCommitHash = await this.gitWrapper.currentCommitHash();
        const currentCommit: CommitCreationMeta = {
            hash: currentCommitHash,
            authorName: await this.gitWrapper.show('%an'),
            authorEmail: await this.gitWrapper.show('%ae'),
            creationTime: new Date(await this.gitWrapper.show('%aI')),
            branchName: this.projectManifest.masterBranch,
            triggersReminder,
            containsDocumentation
        };

        return this.ingestionApi.createCommit(currentCommit);
    }
}
