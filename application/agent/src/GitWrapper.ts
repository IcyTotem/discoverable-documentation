import { exec } from 'child_process';

export interface ChangedFile {
    path: string;

    /**
     * True if the file was deleted as a result of a change. Otherwise, it's assumed
     * the file pointed by path has been modified in some way (creation also counts as modification).
     */
    deleted: boolean;
}

export interface Developer {
    email: string;
    name: string;
}

export interface ShellWrapper {
    exec: (command: string) => Promise<string>;
}

/**
 * Execute the given shell command and return its output.
 * @returns The function produces the whole output of the command emitted on stdout, as a string.
 * @throws On rejection, the only parameter will contain err (the original error object)
 *     and stderr (the whole content of stderr at the time the program exited).
 */
const execPromise = async (command: string): Promise<string> => {
    // No idea of the internal promise function signature, not worth it anyway
    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    return new Promise((resolve, reject) => {
        exec(command, (err, stdout, stderr) => {
            if (err) {
                reject({ err, stderr });
            } else {
                resolve(stdout.trim());
            }
        });
    });
};

/**
 * Parse the output of: git log --pretty="%an <%ae>%n"
 * @returns A list of developers, as name and email pairs.
 */
const parseDevelopersFromGitLog = (gitLogOutput: string): Developer[] => {
    const lineRegex = /(.+?)\s+<(.+?@.+?\..+?)>/gi;
    const developers: Developer[] = [];
    let match: RegExpExecArray;

    do {
        match = lineRegex.exec(gitLogOutput);
        if (match) {
            developers.push({ name: match[1].trim(), email: match[2].trim() });
        }
    } while (match);

    return developers;
};

/**
 * Build one or more ChangedFile object(s) from the elements found on a single line of: git diff --name-only
 * @param operationFlag One of: A (add), M (modify), C (copy), R (rename), D (delete).
 * @param path The original file path.
 * @param newPath The new file path. Only present for copy and rename operations.
 */
const buildChangedFileEntries = function*(operationFlag: string, path: string, newPath: string): Iterable<ChangedFile> {
    switch (operationFlag) {
        case 'A':
        case 'M':
            yield { path, deleted: false };
            break;
        case 'C':
            yield { path: newPath, deleted: false };
            break;
        case 'D':
            yield { path, deleted: true };
            break;
        case 'R':
            yield { path, deleted: true };
            yield { path: newPath, deleted: false };
            break;
    }
};

/**
 * Parse the output of: git diff --name-only
 * @returns A list of changed files. The function automatically expands renames into a pair
 *     of delete and modify operations. Copy operations are converted into modify operations on
 *     the target file. Finally, adding new files is considered the same as modify. Hence
 *     the resulting list of changes only comprises modify and delete operations.
 */
const parseChangedFilesFromGitDiff = (gitLogOutput: string): ChangedFile[] => {
    // The git output will be:
    //  - a letter from the set ACDMR, representing the type of change;
    //  - optionally, a two or three-digit number indicating the percentage similary of a rename or copy;
    //  - the full path to changed file;
    //  - if it's a rename or copy change, the full path to the newly renamed file follows.
    // Notice: [^\S\r\n] matches all whitespaces except newlines.
    // Notice: \.\w+ is used to loosely indicate the extension of a file, but of course this is a very
    //   generic case and may produce false positives. It helps understand where the first path ends
    //   and where the second begins. Just an heuristic. Good enough for the moment being.
    const lineRegex = /^\s*([ACDMR])(\d{2,3})?[^\S\r\n]+(.+?\.\w+)([^\S\r\n]{2,}.+\.\w+)?[^\S\r\n]*$/gim;
    const changedFiles: ChangedFile[] = [];
    let match: RegExpExecArray;

    do {
        match = lineRegex.exec(gitLogOutput);
        if (match) {
            const operation = match[1];
            const path = match[3];
            const newPath = match[4] ? match[4].trim() : undefined;
            for (const entry of buildChangedFileEntries(operation, path, newPath)) {
                changedFiles.push(entry);
            }
        }
    } while (match);

    return changedFiles;
};

export class GitWrapper {
    private readonly shell: ShellWrapper;

    /**
     * Build a git wrapper on top of a shell command executor.
     * @param shell This is a handy parameter to easily test the class with mocks, but can be
     *     used to make the class behaviour more flexible too. If not specified, GitWrapper will
     *     create its own ShellWrapper that uses the native module child_process to launch
     *     shell commands.
     */
    public constructor(shell?: ShellWrapper) {
        this.shell = shell || { exec: execPromise };
    }

    public async currentBranchName(): Promise<string> {
        return this.shell.exec('git rev-parse --abbrev-ref HEAD');
    }

    public async currentCommitHash(): Promise<string> {
        return this.shell.exec('git rev-parse HEAD');
    }

    /**
     * Show formatted information about the current HEAD.
     * @see https://git-scm.com/docs/pretty-formats
     */
    public async show(format: string): Promise<string> {
        return this.shell.exec(`git show -s --format='${format}' HEAD`);
    }

    /**
     * Return a list of all files that, from the current HEAD, have been changed
     * in some way with respect to the given reference commit.
     * @param referenceCommit Hash, branch name or tag of the reference commit.
     * @returns The names contained in the returned list are relative paths to changed files,
     *     and they are relative to the directory in which the script is executed.
     *     The operation associated to each file indicates how that file differs from the reference commit.
     */
    public async findChangedFiles(referenceCommit: string): Promise<ChangedFile[]> {
        const listing = await this.shell.exec(`git diff --name-only ${referenceCommit} HEAD`);
        return parseChangedFilesFromGitDiff(listing);
    }

    public async findAllFilesWithExtension(extension: string): Promise<string[]> {
        const listing = await this.shell.exec(`find . -name "*${extension}"`);
        return listing
            .split('\n')
            .filter(file => file.trim().length > 0)
            .map(file => file.trim());
    }

    /**
     * Return a list of all developers that have authored at least one commit from the
     * reference commit to the current HEAD.
     * @param referenceCommit Hash, branch name of tag of the reference commit. If null or
     *     undefined, it refers to the very beginning of the commit history (i.e. the initial commit).
     * @returns When the reference commit is not given, the result contains all developers
     *     that have worked on the master tree chain since the beginning.
     */
    public async getAllDevelopers(referenceCommit?: string): Promise<Developer[]> {
        const listing = referenceCommit
            ? await this.shell.exec(`git log --pretty="%an <%ae>%n" ${referenceCommit}..HEAD | sort | uniq`)
            : await this.shell.exec('git log --pretty="%an <%ae>%n" | sort | uniq');

        return parseDevelopersFromGitLog(listing);
    }
}
