import * as fs from 'fs';
import * as path from 'path';
import * as archiver from 'archiver';

export class Zipper {
    /**
     * Compress all the given files into a single zip archive.
     * @param files List of relative or absolute paths to files being archived.
     * @param archivePath Relative or absolute path to the new archive file to be created.
     */
    public async zip(files: string[], archivePath: string): Promise<void> {
        // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
        return new Promise((resolve, reject) => {
            const output = fs.createWriteStream(archivePath);
            const archive = archiver('zip', { zlib: { level: 9 } });

            output.on('close', () => resolve());

            archive.on('warning', err => console.warn(err));
            archive.on('error', err => reject(err));
            archive.pipe(output);

            files.forEach(file => archive.file(path.resolve(file), { name: path.basename(file) }));

            archive.finalize();
        });
    }
}
